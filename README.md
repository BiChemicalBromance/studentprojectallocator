# SPRAL - *S*tudent *Pr*oject *Al*locator #
SPRAL is a tool designed to assign students to projects in higher-education (and similar) environments. Using a tool like this can both reduce the cognitive load on the members of staff running these courses, and increase the transparency of the overall allocation process by providing concrete algorithms. A user interface is provided to make it easier to manage the data associated with the process. 

## Input Data Format ##
Currently the program reads in 4 separate csv files (comma separated, UTF-8): students, projects, lecturers, courses. Each of these four files has a different set of required and optional fields which contribute different information to the allocation process. 

### Students ### 

### Lecturers ###

### Projects ###

### Courses ###

## Output Data Format ##

## Session Serialisation ## 

## Allocators ## 
Allocators are scripts which take a list of students, projects and lecturers and produces a list of student-project allocations according to a set of constraints. The allocation process consists of a few main steps which are common to all algorithms;
- data validation
- allocation of students using the algorithm
- allocation of students who could not be allocated using the algorithm

## Extending SPRAL ##
