
#include "GuiComponents/mainWindow.h"
#include <unordered_map>


int main(int argc, char **argv)
{
  QApplication app(argc, argv);
  MainWindow mainWindow{};
  mainWindow.resize(1920, 1080);
  mainWindow.show();
  return app.exec();
}
