//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_ALLOCATOR_H
#define STUDENTPROJECTALLOCATOR_ALLOCATOR_H

#include <QtWidgets/QTextEdit>
#include "Data Structures/Allocation.h"
#include "Data Structures/Lecturer.h"
#include "Data Structures/Course.h"
#include "Algorithms/AllocatorOptions.h"
#include <vector>
#include <memory>
#include <regex>


namespace SPRAL {
  struct AllocationResults
  {
	std::string algorithmName;
	std::string dateTime;
	std::string runBy;
	std::vector<Allocation> allocations;
	std::vector<Student> unassignedStudents;
	std::vector<Project> freeProjects;
  };

  enum class LoggingLevel {
	SILENT,
	EVERYTHING,
	NORMAL,
	WARN,
	FATAL
  };

  struct LoggingSettings
  {
	bool logToFile{ false };
	LoggingLevel verbosity{ LoggingLevel::NORMAL };
	std::string logFilPath;
  };

  enum class ErrorSource {
	STUDENTS,
	PROJECTS,
	LECTURERS,
	COURSES,
	UNKNOWN
  };

  enum class ErrorType {
	EMPTY_VECTOR,
	ZERO_CAPACITY,
	MISMATCH,
	INSUFFICIENT_CAPACITY
  };

  struct ValidationError
  {
	ErrorSource errorSource;
	ErrorType errorType;
	std::string helpMessage;
  };


  class Allocator
  {
  public:
	explicit Allocator(std::vector<Student> s, std::vector<Project> p, std::vector<Lecturer> l, std::vector<Course> c);
	virtual ~Allocator() noexcept = default;
	Allocator(const Allocator &) = delete;
	virtual bool Validate() = 0;
	virtual void operator()(QTextEdit *outputBox) = 0;
	[[nodiscard]] std::optional<ValidationError> validateData();
	std::unordered_map<std::string, std::unique_ptr<AllocatorOption>> algorithmOptions;
	std::vector<std::string> algorithmOptionDrawOrder;
	LoggingSettings logSettings;
	template<class... Args>
	Student &addStudent(Args &&... args) { return students.emplace_back(std::forward<Args>(args)...); }
	template<class... Args>
	Lecturer &addLecturer(Args &&... args) { return lecturers.emplace_back(std::forward<Args>(args)...); }
	template<class... Args>
	Project &addProject(Args &&... args) { return projects.emplace_back(std::forward<Args>(args)...); }
	void SetLogSettings(LoggingSettings loggingSettings);
	bool IDsNeedConverting{ false };
	virtual void convertIDFormat() = 0;
	virtual AllocationResults getResults() = 0;

  private:
	void assignStudent(Student student, Project project);

  protected:
	std::vector<Student> students;
	std::vector<Project> projects;
	std::vector<Lecturer> lecturers;
	std::vector<Course> courses;
	std::vector<Allocation> studentAllocations;
  };
}// namespace SPRAL
#endif// STUDENTPROJECTALLOCATOR_ALLOCATOR_H
