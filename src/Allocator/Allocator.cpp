//
// Created by tom on 05/10/2020.
//


#include "Allocator.h"
#include <utility>
#include <utility>

namespace SPRAL {

  Allocator::Allocator(std::vector<Student> s, std::vector<Project> p, std::vector<Lecturer> l, std::vector<Course> c) : students(std::move(s)), projects(std::move(p)), lecturers(std::move(l)), courses(std::move(c))
  {}

  void Allocator::assignStudent(Student student, Project project)
  {
	studentAllocations.emplace_back(student, project);
  }
  void Allocator::SetLogSettings(LoggingSettings loggingSettings)
  {
	logSettings = std::move(loggingSettings);
  }
  std::optional<ValidationError> Allocator::validateData()
  {
	// check all projects have > 0 capacity and that all have a valid lecturer
	int totalProjectCapacity{ 0 };
	for (const auto &project : projects) {
	  totalProjectCapacity += project.capacity;
	  if (project.capacity <= 0) {
		return ValidationError{ .errorSource = ErrorSource::PROJECTS, .errorType = ErrorType::ZERO_CAPACITY, .helpMessage = fmt::format("A zero or negative capacity was entered for {}", project.ID) };
	  }
	  if (std::find_if(lecturers.begin(), lecturers.end(), [&](const Lecturer &l) { return l.ID == project.lecturer; }) == lecturers.end()) {
		return ValidationError{ .errorSource = ErrorSource::PROJECTS, .errorType = ErrorType::MISMATCH, .helpMessage = fmt::format("Project {0} has listed lecturer {1} as the supervisor, but {1} is not in the list of known lecturers", project.ID, project.lecturer) };
	  }
	}
	if (static_cast<unsigned long>(totalProjectCapacity) < students.size()) {
	  return ValidationError{ .errorSource = ErrorSource::PROJECTS, .errorType = ErrorType::INSUFFICIENT_CAPACITY, .helpMessage = fmt::format("There is not enough capacity among the projects to accomodate all students ({} places vs {} students", totalProjectCapacity, students.size()) };
	}
	// check all lecturers have > 0 capacity
	int totalLecturerCapacity{ 0 };
	for (const auto &lecturer : lecturers) {
	  totalLecturerCapacity += lecturer.capacity;
	  if (lecturer.capacity <= 0) {
		return ValidationError{ .errorSource = ErrorSource::LECTURERS, .errorType = ErrorType::ZERO_CAPACITY, .helpMessage = fmt::format("A zero or negative capacity was entered for {}", lecturer.ID) };
	  }
	}
	if (static_cast<unsigned long>(totalLecturerCapacity) < students.size()) {
	  return ValidationError{ .errorSource = ErrorSource::LECTURERS, .errorType = ErrorType::INSUFFICIENT_CAPACITY, .helpMessage = fmt::format("There is not enough capacity among the lecturers to accomodate all students ({} places vs {} students", totalLecturerCapacity, students.size()) };
	}

	// check all students have a non-empty preference list, that their course is valid as are all their ranked preferences
	for (const auto &student : students) {
	  if (student.projectPreferences[0].empty() && student.projectPreferences[1].empty()) {
		return ValidationError{ .errorSource = ErrorSource::STUDENTS, .errorType = ErrorType::EMPTY_VECTOR, .helpMessage = fmt::format("Student {} has submitted a zero-length preference list.", student.ID) };
	  }
	  if (IDsNeedConverting) {
		for (const auto &project : student.projectPreferences) {
		  if (std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.IDasInStudentPreferences == project; }) == projects.end()) {
			return ValidationError{ .errorSource = ErrorSource::STUDENTS, .errorType = ErrorType::MISMATCH, .helpMessage = fmt::format("Student {0} has listed project {1} in their preference list, but {1} is not in the list of known projects, even after ID conversion", student.ID, project) };
		  }
		}
	  } else {
		for (const auto &project : student.projectPreferences) {
		  if (project.empty()) {
			continue;
		  }
		  if (std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.ID == project; }) == projects.end()) {
			return ValidationError{ .errorSource = ErrorSource::STUDENTS, .errorType = ErrorType::MISMATCH, .helpMessage = fmt::format("Student {0} has listed project {1} in their preference list, but {1} is not in the list of known projects", student.ID, project) };
		  }
		}
	  }
	  if (std::find_if(courses.begin(), courses.end(), [&](const Course &c) { return c.ID == student.course; }) == courses.end()) {
		return ValidationError{ .errorSource = ErrorSource::STUDENTS, .errorType = ErrorType::MISMATCH, .helpMessage = fmt::format("Student {0} has listed course {1}, but {1} is not in the list of known courses", student.ID, student.course) };
	  }
	}
	return {};
  }

}// namespace SPRAL