//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_ALLOCATION_H
#define STUDENTPROJECTALLOCATOR_ALLOCATION_H
#include <utility>

#include "Student.h"
#include "Project.h"
namespace SPRAL {
  struct Allocation
  {
	explicit Allocation(Student s, Project p) : student(s), project(p) {}
	Student student;
	Project project;
  };
}// namespace SPRAL
#endif//STUDENTPROJECTALLOCATOR_ALLOCATION_H

