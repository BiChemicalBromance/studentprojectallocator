//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_COURSE_H
#define STUDENTPROJECTALLOCATOR_COURSE_H

#include <optional>
#include <string>
namespace SPRAL {
  struct Course
  {
	explicit Course(std::string ID, int16_t priority);
	std::string ID;
	int16_t priority;
	uint32_t enrolledStudents{ 0 };
  };

}// namespace SPRAL
#endif// STUDENTPROJECTALLOCATOR_COURSE_H
