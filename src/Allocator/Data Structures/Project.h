//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_PROJECT_H
#define STUDENTPROJECTALLOCATOR_PROJECT_H

#include <cstdint>
#include <optional>
#include <string>
#include <vector>

namespace SPRAL {
  struct Project
  {
	Project(std::string a_ID, std::string a_lect, uint8_t a_capacity, std::vector<std::string> a_Courses, std::string a_title = "", std::vector<std::string> a_topics = {});
	std::string ID;
	std::string lecturer;
	uint8_t capacity;
	std::vector<std::string> suitableCourses;
	std::optional<std::string> title;
	std::optional<std::vector<std::string>> projectTopics;

	std::string IDasInStudentPreferences;
	std::vector<std::string> assignedStudents;
  };

} // namespace SPRAL
#endif // STUDENTPROJECTALLOCATOR_PROJECT_H
