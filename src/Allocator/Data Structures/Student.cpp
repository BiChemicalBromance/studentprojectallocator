//
// Created by tom on 06/10/2020.
//
#include "Student.h"

namespace SPRAL {
  Student::Student(std::string a_ID, std::vector<std::string> prefs, std::string courseID) : ID(std::move(a_ID)), projectPreferences(std::move(prefs)), course(std::move(courseID))
  {
	name = std::nullopt;
	topicPreferences = std::nullopt;
  }

  Student::Student(std::string a_ID, std::vector<std::string> prefs, std::string courseID, std::vector<std::string> a_topicPreferences) : ID(std::move(a_ID)), projectPreferences(std::move(prefs)), course(std::move(courseID)), topicPreferences(a_topicPreferences)
  {
	name = std::nullopt;
  }
}// namespace SPRAL