//
// Created by tom on 08/10/2020.
//
#include "Lecturer.h"

#include <utility>

SPRAL::Lecturer::Lecturer(std::string a_ID, uint16_t a_capacity, const std::string &a_name, std::vector<std::string> a_topics, std::vector<std::string> a_courses) : ID(std::move(a_ID)), capacity(a_capacity)
{
  if (a_name.empty()) {
	name = std::nullopt;
  } else {
	name = a_name;
  }
  if (a_topics.empty()) {
	topics = std::nullopt;
  } else {
	topics = a_topics;
  }
  if (a_courses.empty()) {
	associatedCourses = std::nullopt;
  } else {
	associatedCourses = a_courses;
  }
}
