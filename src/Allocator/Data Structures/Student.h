//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_STUDENT_H
#define STUDENTPROJECTALLOCATOR_STUDENT_H
#include <cstdint>
#include <optional>
#include <string>
#include <fmt/core.h>
#include <utility>
#include <vector>
#include <variant>
#include <yaml-cpp/yaml.h>

namespace SPRAL {
  struct Student
  {
	//  required properties
	Student(std::string ID, std::vector<std::string> prefs, std::string courseID);
	Student(std::string ID, std::vector<std::string> prefs, std::string courseID, std::vector<std::string> topicPreferences);
	std::string ID;
	std::vector<std::string> projectPreferences;
	std::string course;
	//  optional properties depending on data collection method
	[[maybe_unused]] std::optional<std::string> name;
	[[maybe_unused]] std::optional<std::vector<std::string>> topicPreferences;
	bool isAssigned{ false };
	std::string assignedProject;
	int assignedProjectRank{ -1 };
  };

}// namespace SPRAL

template<>
struct fmt::formatter<SPRAL::Student>
{
  template<typename ParseContext>
  constexpr auto parse(ParseContext &ctx)
  {
	return ctx.begin();
  }

  template<typename FormatContext>
  auto format(SPRAL::Student const &student, FormatContext &ctx)
  {
	return fmt::format_to(ctx.out(), "Student: {} on course {}, with {} preferences", student.ID, student.course, student.projectPreferences.size());
  }
};


#endif// STUDENTPROJECTALLOCATOR_STUDENT_H
