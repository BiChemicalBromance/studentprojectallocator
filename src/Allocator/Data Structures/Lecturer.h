//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_LECTURER_H
#define STUDENTPROJECTALLOCATOR_LECTURER_H
#include <optional>
#include <string>
#include <vector>
namespace SPRAL {
  struct Lecturer
  {
	explicit Lecturer(std::string a_ID, uint16_t a_capacity, const std::string &a_name = "", std::vector<std::string> a_topics = {}, std::vector<std::string> a_courses = {});
	std::string ID;
	uint16_t capacity;
	std::optional<std::string> name;
	std::optional<std::vector<std::string>> topics;
	std::optional<std::vector<std::string>> associatedCourses;

	std::vector<std::string> assignedStudents;
	std::vector<std::string> preferences;
  };
}// namespace SPRAL
#endif// STUDENTPROJECTALLOCATOR_LECTURER_H
