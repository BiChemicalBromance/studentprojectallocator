//
// Created by tom on 07/10/2020.
//

#include "Project.h"

#include <utility>

namespace SPRAL {
  Project::Project(std::string a_ID, std::string a_lect, uint8_t a_capacity, std::vector<std::string> a_Courses, std::string a_title, std::vector<std::string> a_topics) : ID(std::move(a_ID)), lecturer(std::move(a_lect)), capacity(a_capacity), suitableCourses(std::move(a_Courses))
  {
	if (a_title.empty()) {
	  title = std::nullopt;
	} else {
	  title = std::move(a_title);
	}
	if (a_topics.empty()) {
	  projectTopics = std::nullopt;
	} else {
	  projectTopics = std::move(a_topics);
	}
  }
}// namespace SPRAL