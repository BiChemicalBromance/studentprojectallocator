//
// Created by tom on 10/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_ABRAHAMALLOCATOR_H
#define STUDENTPROJECTALLOCATOR_ABRAHAMALLOCATOR_H

#include "../Allocator.h"

namespace SPRAL {
  class AbrahamAllocator : public Allocator
  {
  public:
	explicit AbrahamAllocator(std::vector<Student> s, std::vector<Project> p, std::vector<Lecturer> l, std::vector<Course> c) : Allocator(s, p, l, c)
	{
	  algorithmOptions.emplace("randomAssign", std::make_unique<BoolOption>("randomAssignUnassigned", "Randomly Assign Unassigned Students", false));
	  algorithmOptions.emplace("shuffleList", std::make_unique<BoolOption>("shuffleInitialList", "Shuffle initial unassigned students list", true));
	  algorithmOptions.emplace("iterLimit", std::make_unique<IntOption>("iterationLimit", "Iteration Limit", -1, -1, 10000));

	  algorithmOptionDrawOrder = { "randomAssign", "shuffleList", "iterLimit" };
	}

	void deleteSuccessorPrefs(const std::string &student, const Project &p, std::vector<std::string> &projectedPreferences, std::unordered_map<std::string, std::vector<std::string>> &studPrefs)
	{
	  auto studentInProjectedProjectPrefs = std::find(projectedPreferences.begin(), projectedPreferences.end(), student);
	  if (studentInProjectedProjectPrefs == projectedPreferences.end() || *studentInProjectedProjectPrefs == projectedPreferences.back()) {
		return;
	  }
	  std::vector<std::string> studentsToRemove{ studentInProjectedProjectPrefs + 1, projectedPreferences.end() };
	  for (const auto &studentToRemove : studentsToRemove) {
		while (std::find(studPrefs[studentToRemove].begin(), studPrefs[studentToRemove].end(), p.ID) != studPrefs[studentToRemove].end()) {
		  studPrefs[studentToRemove].erase(std::find(studPrefs[studentToRemove].begin(), studPrefs[studentToRemove].end(), p.ID));
		}
	  }
	}
	void convertIDFormat() override
	{
	}

	void deleteSuccessorPrefsAll(std::string &student, const Lecturer &lect, std::unordered_map<std::string, std::vector<std::string>> &projectedPrefs, std::unordered_map<std::string, std::vector<std::string>> studentPreferences)
	{
	  std::vector<std::string> lectProjects = [&, this]() -> std::vector<std::string> {
		std::vector<std::string> result;
		std::for_each(projects.begin(), projects.end(), [&](const Project &p) {if(p.lecturer == lect.ID){result.push_back(p.ID);} });
		return result;
	  }();
	  auto studentInLecturerProjectPrefs = std::find(lect.preferences.begin(), lect.preferences.end(), student);
	  if (studentInLecturerProjectPrefs == lect.preferences.end() || *studentInLecturerProjectPrefs == lect.preferences.back()) {
		return;
	  }
	  std::vector<std::string> studentsToRemove{ studentInLecturerProjectPrefs + 1, lect.preferences.end() };

	  for (const auto &studentToRemove : studentsToRemove) {
		for (const auto &project : lectProjects) {
		  while (std::find(projectedPrefs[project].begin(), projectedPrefs[project].end(), studentToRemove) != projectedPrefs[project].end()) {
			projectedPrefs[project].erase(std::find(projectedPrefs[project].begin(), projectedPrefs[project].end(), studentToRemove));
		  }
		  while (std::find(studentPreferences[studentToRemove].begin(), studentPreferences[studentToRemove].end(), project) != studentPreferences[studentToRemove].end()) {
			studentPreferences[studentToRemove].erase(std::find(studentPreferences[studentToRemove].begin(), studentPreferences[studentToRemove].end(), project));
		  }
		}
	  }
	}
	bool Validate() override
	{
	  return true;
	}
	void operator()(QTextEdit *outputBox) override
	{
	  const int iterLimit = std::any_cast<int>(algorithmOptions.at("iterLimit")->optionValue);
	  std::vector<Student> unassignedStudents{ students };
	  std::unordered_map<std::string, std::vector<std::string>> studentPreferencesMap;
	  std::for_each(students.begin(), students.end(), [&](const Student &s) { studentPreferencesMap[s.ID] = s.projectPreferences; });
	  std::unordered_map<std::string, std::vector<std::string>> projectedPreferences;
	  if (!Validate()) {
		QMessageBox msgBox;
		msgBox.setText("A validation error occurred");
		msgBox.setDetailedText("Some detailed text about where the validation error occurred and suggestions on how to fix it");
		msgBox.exec();
		return;
	  }
	  for (const auto &[student, prefsVec] : studentPreferencesMap) {
		const std::string studentID = student;
		std::for_each(prefsVec.begin(), prefsVec.end(), [&, this](const std::string &projectCode) {
		  Project currentProject = *std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.ID == projectCode; });
		  Lecturer currentLecturer = *std::find_if(lecturers.begin(), lecturers.end(), [&](const Lecturer &l) { return l.ID == currentProject.lecturer; });
		  const auto studentInLecturerPrefs = std::find(currentLecturer.preferences.begin(), currentLecturer.preferences.end(), studentID);
		  if (studentInLecturerPrefs == currentLecturer.preferences.end()) {
			currentLecturer.preferences.push_back(studentID);
		  }
		});
	  }

	  for (const auto &project : projects) {
		auto currentLecturer = *std::find_if(lecturers.begin(), lecturers.end(), [&](const Lecturer &l) { return l.ID == project.lecturer; });
		for (const auto &student : currentLecturer.preferences) {
		  const auto projectInStudentPrefs = std::find(studentPreferencesMap[student].begin(), studentPreferencesMap[student].end(), project.ID);
		  if (projectInStudentPrefs != studentPreferencesMap[student].end()) {
			projectedPreferences[project.ID].push_back(student);
		  }
		}
	  }
	  size_t numProjectsInPrefList = 0;
	  bool done{ false };
	  int iterations{ 0 };
	  Student *currentStudent;
	  while (!done && (iterations <= iterLimit || iterLimit == -1)) {
		iterations++;
		if (!unassignedStudents.empty()) {
		  for (auto &student : unassignedStudents) {
			numProjectsInPrefList = studentPreferencesMap[student.ID].size();
			currentStudent = &student;
		  }
		  if (numProjectsInPrefList > 0) {
			auto currentProject = *std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.ID == currentStudent->projectPreferences[0]; });
			auto currentLecturer = *std::find_if(lecturers.begin(), lecturers.end(), [&](const Lecturer &l) { return l.ID == currentProject.lecturer; });
			studentAllocations.emplace_back(*currentStudent, currentProject);
			unassignedStudents.erase(std::remove_if(unassignedStudents.begin(), unassignedStudents.end(), [&](const Student &s) { return s.ID == currentStudent->ID; }), unassignedStudents.end());
			currentLecturer.assignedStudents.emplace_back(currentStudent->ID);
			currentProject.assignedStudents.emplace_back(currentStudent->ID);
			outputBox->append(QString::fromStdString(fmt::format("Student {} has been assigned to project {}", currentStudent->ID, currentProject.ID)));

			if (currentProject.assignedStudents.size() > currentProject.capacity) {
			  std::string worstStudent;
			  int maxIndex;
			  std::for_each(currentProject.assignedStudents.begin(), currentProject.assignedStudents.end(), [&](const std::string &s) {
				auto studentIdxInProjectedPrefs = std::find(projectedPreferences[currentProject.ID].begin(), projectedPreferences[currentProject.ID].end(), s);
				if (studentIdxInProjectedPrefs == projectedPreferences[currentProject.ID].end()) {
				  maxIndex = -1;
				  worstStudent = s;
				}
				if (int idx = std::distance(projectedPreferences[currentProject.ID].begin(), studentIdxInProjectedPrefs) > maxIndex) {
				  maxIndex = idx;
				  worstStudent = s;
				}
			  });

			  outputBox->append(QString::fromStdString(fmt::format("Project {} is overloaded - removing {}", currentProject.ID, worstStudent)));

			  currentLecturer.assignedStudents.erase(std::remove(currentLecturer.assignedStudents.begin(), currentLecturer.assignedStudents.end(), worstStudent), currentLecturer.assignedStudents.end());
			  ;
			  studentAllocations.pop_back();
			  auto worstStudentObject = std::find_if(students.begin(), students.end(), [&](const Student &s) { return s.ID == worstStudent; });
			  if (worstStudentObject != students.end()) {
				(*worstStudentObject).isAssigned = false;
			  }
			  unassignedStudents.push_back(*worstStudentObject);
			}
			if (currentLecturer.assignedStudents.size() > currentLecturer.capacity) {
			  std::string worstStudent;
			  int maxIndex;
			  std::for_each(currentLecturer.assignedStudents.begin(), currentLecturer.assignedStudents.end(), [&](const std::string &s) {
				auto studentIdxInLectPrefs = std::find(currentLecturer.preferences.begin(), currentLecturer.preferences.end(), s);
				if (studentIdxInLectPrefs == currentLecturer.preferences.end()) {
				  maxIndex = -1;
				  worstStudent = s;
				}
				if (int idx = std::distance(currentLecturer.preferences.begin(), studentIdxInLectPrefs) > maxIndex) {
				  maxIndex = idx;
				  worstStudent = s;
				}
			  });

			  outputBox->append(QString::fromStdString(fmt::format("Lecturer {} is overloaded - removing {}", currentLecturer.ID, worstStudent)));

			  currentLecturer.assignedStudents.erase(std::remove(currentLecturer.assignedStudents.begin(), currentLecturer.assignedStudents.end(), worstStudent), currentLecturer.assignedStudents.end());
			  studentAllocations.pop_back();
			  auto worstStudentObject = std::find_if(students.begin(), students.end(), [&](const Student &s) { return s.ID == worstStudent; });
			  if (worstStudentObject != students.end()) {
				(*worstStudentObject).isAssigned = false;
			  }
			  unassignedStudents.push_back(*worstStudentObject);
			}
			if (currentProject.assignedStudents.size() == currentProject.capacity) {
			  std::string worstStudent;
			  int maxIndex;
			  std::for_each(currentProject.assignedStudents.begin(), currentProject.assignedStudents.end(), [&](const std::string &s) {
				auto studentIdxInProjectedPrefs = std::find(projectedPreferences[currentProject.ID].begin(), projectedPreferences[currentProject.ID].end(), s);
				if (studentIdxInProjectedPrefs == projectedPreferences[currentProject.ID].end()) {
				  maxIndex = -1;
				  worstStudent = s;
				}
				if (int idx = std::distance(projectedPreferences[currentProject.ID].begin(), studentIdxInProjectedPrefs) > maxIndex) {
				  maxIndex = idx;
				  worstStudent = s;
				}
			  });

			  outputBox->append(QString::fromStdString(fmt::format("Project {} is full - removing successors to {}", currentProject.ID, worstStudent)));

			  deleteSuccessorPrefs(worstStudent, currentProject, projectedPreferences[currentProject.ID], studentPreferencesMap);
			}
			if (currentLecturer.assignedStudents.size() == currentLecturer.capacity) {
			  std::string worstStudent;
			  int maxIndex;
			  std::for_each(currentLecturer.assignedStudents.begin(), currentLecturer.assignedStudents.end(), [&](const std::string &s) {
				auto studentIdxInLectPrefs = std::find(currentLecturer.preferences.begin(), currentLecturer.preferences.end(), s);
				if (studentIdxInLectPrefs == currentLecturer.preferences.end()) {
				  maxIndex = -1;
				  worstStudent = s;
				}
				if (int idx = std::distance(currentLecturer.preferences.begin(), studentIdxInLectPrefs) > maxIndex) {
				  maxIndex = idx;
				  worstStudent = s;
				}
			  });

			  outputBox->append(QString::fromStdString(fmt::format("Lecturer {} is full - removing successors to {}", currentLecturer.ID, worstStudent)));
			  deleteSuccessorPrefsAll(worstStudent, currentLecturer, projectedPreferences, studentPreferencesMap);
			}
			outputBox->append("Iteration complete");
		  } else {
			done = true;
		  }
		} else {
		  done = true;
		}
	  }
	}
	AllocationResults getResults() override;
  };

  AllocationResults AbrahamAllocator::getResults()
  {
	return AllocationResults();
  }

}// namespace SPRAL

#endif//STUDENTPROJECTALLOCATOR_ABRAHAMALLOCATOR_H
