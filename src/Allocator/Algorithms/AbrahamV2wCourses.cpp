//
// Created by tom on 28/10/2020.

#include "AbrahamV2wCourses.h"
#include <fstream>
#include <functional>
#include <random>
#include <utility>
#include <date/date.h>

namespace SPRAL {

  AbrahamV2::AbrahamV2(const std::vector<Student> &s, const std::vector<Project> &p, const std::vector<Lecturer> &l, const std::vector<Course> &c) : Allocator(s, p, l, c)
  {

	algorithmOptions.emplace("randomAssign", std::make_unique<BoolOption>("randomAssignUnassigned", "Randomly Assign Unassigned Students", false));
	algorithmOptions.emplace("shuffleList", std::make_unique<BoolOption>("shuffleInitialList", "Shuffle initial unassigned students list", true));
	algorithmOptions.emplace("line1", std::make_unique<HLineOption>());
	algorithmOptions.emplace("iterLimit", std::make_unique<IntOption>("iterationLimit", "Iteration Limit", -1, -1, 10000));
	algorithmOptions.emplace("line2", std::make_unique<HLineOption>());
	algorithmOptions.emplace("followCoursePriority", std::make_unique<BoolOption>("followCoursePriority", "Follow Course Priority", true));

	algorithmOptionDrawOrder = { "randomAssign", "shuffleList", "line1", "iterLimit", "line2", "followCoursePriority" };
  }
  bool AbrahamV2::Validate()
  {
	// check to see that each student has listed projects that are suitable for their course
	for (const auto &student : students) {
	  auto studentCourse = student.course;
	  for (const auto &project : student.projectPreferences) {
		if (auto it = std::find_if(projects.begin(), projects.end(), [&project](const Project &p) { return p.ID == project; }) != projects.end()) {
		  auto applicableProjects = projects.at(it).suitableCourses;
		  if (std::find(applicableProjects.begin(), applicableProjects.end(), studentCourse) == applicableProjects.end()) {
			return false;
		  }
		}
	  }
	  studentPreferencesMap[student.ID] = student.projectPreferences;
	}
	return true;
  }

  void AbrahamV2::convertIDFormat()
  {
  }

  AllocationResults AbrahamV2::getResults()
  {
	std::vector<Project> localFreeProjects;
	for (auto &student : students) {
	  if (student.isAssigned) {
		Project assignedProject = *std::find_if(projects.begin(), projects.end(), [&student](const Project &p) { return p.ID == student.assignedProject; });
		auto projectRank = std::find(student.projectPreferences.begin(), student.projectPreferences.end(), student.assignedProject);
		if (projectRank != student.projectPreferences.end()) {
		  student.assignedProjectRank = std::distance(student.projectPreferences.begin(), projectRank);
		} else {
		}
		studentAllocations.emplace_back(student, assignedProject);
	  }
	}
	for (const auto &project : projects) {
	  if (project.assignedStudents.size() < project.capacity) {
		localFreeProjects.push_back(project);
	  }
	}

	std::string dateRun = date::format("%F %T", std::chrono::system_clock::now());
	QString name = qgetenv("USER");
	if (name.isEmpty())
	  name = qgetenv("USERNAME");
	std::string currentUser = name.toStdString();

	return AllocationResults{
	  .algorithmName = "AbrahamV2",
	  .dateTime = dateRun,
	  .runBy = currentUser,
	  .allocations = studentAllocations,
	  .unassignedStudents = localUnassignedStudents,
	  .freeProjects = localFreeProjects
	};
  }

  void AbrahamV2::operator()(QTextEdit *outputBox)
  {
	outputWidget = outputBox;
	const int iterationLimit = std::any_cast<int>(algorithmOptions["iterLimit"]->optionValue);
	std::vector<Project> freeProjects;
	for (const auto &project : projects) {
	  if (project.capacity > project.assignedStudents.size()) {
		freeProjects.push_back(project);
	  }
	}
	//	if (!Validate()) {
	//	  outputBox->append("An error was raised by the specific allocation function. Exiting.");
	//	  return;
	//	}

	std::map<int, std::vector<std::string>> courseHierarchy;
	for (const auto &course : courses) {
	  courseHierarchy[course.priority].push_back(course.ID);
	}
	std::vector<Lecturer> lecturersLastCourseRun{ lecturers.begin(), lecturers.end() };
	std::vector<Lecturer> lecturersCurrentCourseRun{ lecturers.begin(), lecturers.end() };
	for (auto &[priority, courseList] : courseHierarchy) {
	  for (const auto &project : projects) {
		if (project.capacity > project.assignedStudents.size()) {
		  freeProjects.push_back(project);
		}
	  }

	  for (size_t idx = 0; idx < lecturersLastCourseRun.size(); idx++) {
		lecturersCurrentCourseRun.at(idx).capacity -= lecturersLastCourseRun.at(idx).assignedStudents.size();
		lecturersCurrentCourseRun.at(idx).assignedStudents.clear();
	  }

	  // Construct a preferences map only for the students who are in the courses currently being allocated - will be reset after each priority group

	  std::vector<Student> unassignedStudentsFromCourses;
	  std::map<std::string, std::vector<std::string>> studentPrefsForCurrentCourses;
	  for (const auto &course : courseList) {
		for (const auto &student : students) {
		  if (student.course == course) {
			studentPrefsForCurrentCourses[student.ID] = student.projectPreferences;
			unassignedStudentsFromCourses.push_back(student);
		  }
		}
	  }


	  logMessage(fmt::format("Allocating courses with priority {}, totalling {} students", priority, studentPrefsForCurrentCourses.size()), outputBox);

	  // remove any projects which have already been assigned to higher-priority courses amd then push students to the unassignedStudents list
	  for (auto &pair : studentPrefsForCurrentCourses) {
		auto studentID = pair.first;
		pair.second.erase(std::remove_if(pair.second.begin(), pair.second.end(), [&](const std::string &s) {
		  auto findIt = std::find_if(freeProjects.begin(), freeProjects.end(), [&](const Project &p) { return s == p.ID; });
		  return findIt == freeProjects.end();
		}),
		  pair.second.end());
		pair.second.erase(std::remove_if(pair.second.begin(), pair.second.end(), [&](const std::string &project) {
		  auto projectInFreeProjectsList = std::find_if(freeProjects.begin(), freeProjects.end(), [&](const Project &p) { return project == p.ID; });
		  auto lecturerOfProject = *std::find_if(lecturersCurrentCourseRun.begin(), lecturersCurrentCourseRun.end(), [&](const Lecturer &l) {
			return l.ID == (*projectInFreeProjectsList).lecturer;
		  });
		  return lecturerOfProject.capacity == 0;
		}),
		  pair.second.end());
	  }


	  // construct the lecturerPreferences map by adding students to the lecturer's preferences who are not already in the list
	  std::map<std::string, std::vector<std::string>> lecturerPreferences;
	  // ensure all lecturers exist in the map, lest we get .at() exceptions
	  for (const auto &lecturer : lecturers) {
		if (lecturerPreferences.find(lecturer.ID) == lecturerPreferences.end()) {
		  lecturerPreferences.insert({ lecturer.ID, {} });
		}
	  }
	  for (const auto &entry : studentPrefsForCurrentCourses) {
		auto studentID = entry.first;
		for (auto &project : entry.second) {
		  auto it = std::find_if(projects.begin(), projects.end(), [&project](const Project &p) { return p.ID == project; });
		  if (it != projects.end()) {
			const std::string projectLecturer = (*it).lecturer;
			auto it2 = std::find_if(lecturerPreferences.at(projectLecturer).begin(), lecturerPreferences.at(projectLecturer).end(), [&studentID](const std::string &s) {
			  return studentID == s;
			});
			if (it2 == lecturerPreferences.at(projectLecturer).end()) {
			  lecturerPreferences[projectLecturer].push_back(studentID);
			}
		  }
		}
	  }

	  // Create a map of projects and the students who have applied to that project
	  std::map<std::string, std::vector<std::string>> lecturerProjects;
	  std::map<std::string, std::vector<std::string>> projectedPrefsForCurrentCourses;
	  for (const auto &project : projects) {
		if (lecturerProjects.find(project.lecturer) == lecturerProjects.end()) {
		  lecturerProjects.insert({ project.lecturer, {} });
		}
		lecturerProjects[project.lecturer].push_back(project.ID);
		if (projectedPrefsForCurrentCourses.find(project.ID) == projectedPrefsForCurrentCourses.end()) {
		  projectedPrefsForCurrentCourses.insert({ project.ID, {} });
		}
		for (const auto &student : lecturerPreferences.at(project.lecturer)) {
		  auto it = std::find(studentPrefsForCurrentCourses[student].begin(), studentPrefsForCurrentCourses[student].end(), project.ID);
		  if (it != studentPrefsForCurrentCourses[student].end()) {
			projectedPrefsForCurrentCourses[project.ID].push_back(student);
		  }
		}
	  }

	  bool done{ false };
	  ssize_t iterationsCompleted{ 0 };
	  int projectsRemainingInPreferenceList;
	  std::string currentStudent;
	  while (!done) {
		iterationsCompleted++;
		if (iterationLimit != -1 && iterationsCompleted < iterationLimit) {
		  break;
		}
		if (!unassignedStudentsFromCourses.empty()) {
		  for (auto &student : unassignedStudentsFromCourses) {
			currentStudent = student.ID;
			if (priority == 2 && (student.course == "BSc Medicinal Chemistry" || student.course == "MChem Medicinal Chemistry")) {
			  break;
			}
			if (!studentPrefsForCurrentCourses.at(currentStudent).empty()) {
			  projectsRemainingInPreferenceList = studentPrefsForCurrentCourses.at(currentStudent).size();
			  break;
			} else {
			  projectsRemainingInPreferenceList = 0;
			}
		  }
		  if (projectsRemainingInPreferenceList > 0) {// here we go - the main part of the algorithm
			// Find the current project and lecturer from their string identifiers
			Project &currentProject = projects.at(std::distance(projects.begin(),
			  std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.ID == studentPrefsForCurrentCourses.at(currentStudent).at(0); })));
			Lecturer &currentLecturer = lecturersCurrentCourseRun.at(std::distance(lecturersCurrentCourseRun.begin(),
			  std::find_if(lecturersCurrentCourseRun.begin(), lecturersCurrentCourseRun.end(), [&](const Lecturer &l) { return l.ID == currentProject.lecturer; })));

			// provisionally assign the student to the project
			currentProject.assignedStudents.push_back(currentStudent);
			currentLecturer.assignedStudents.push_back(currentStudent);
			Student &currentStudentObject = students.at(std::distance(students.begin(),
			  std::find_if(students.begin(), students.end(), [&](const Student &s) { return s.ID == currentStudent; })));
			currentStudentObject.isAssigned = true;
			currentStudentObject.assignedProject = currentProject.ID;
			unassignedStudentsFromCourses.erase(std::remove_if(unassignedStudentsFromCourses.begin(), unassignedStudentsFromCourses.end(), [&](const Student &s) { return s.ID == currentStudent; }), unassignedStudentsFromCourses.end());
			logMessage("---------------------------------------------------------------", outputBox, LoggingLevel::EVERYTHING);
			logMessage(fmt::format("Assigned student {} to project {}", currentStudent, currentProject.ID), outputBox, LoggingLevel::EVERYTHING);
			// current project is overloaded
			if (currentProject.assignedStudents.size() > currentProject.capacity) {
			  std::string worstStudentAssignedToProject = findWorstStudent(projectedPrefsForCurrentCourses.at(currentProject.ID), currentProject.assignedStudents);
			  logMessage(fmt::format("Project {} is overloaded, removing {}", currentProject.ID, worstStudentAssignedToProject), outputBox);
			  breakAssociation(worstStudentAssignedToProject, currentProject, currentLecturer, unassignedStudentsFromCourses);
			}
			if (currentLecturer.assignedStudents.size() > currentLecturer.capacity) {
			  std::string worstStudentAssignedToProject = findWorstStudent(lecturerPreferences.at(currentLecturer.ID), currentLecturer.assignedStudents);
			  logMessage(fmt::format("Lecturer {} is overloaded, removing {}", currentLecturer.ID, worstStudentAssignedToProject), outputBox);
			  breakAssociation(worstStudentAssignedToProject, currentProject, currentLecturer, unassignedStudentsFromCourses);
			}
			if (currentProject.assignedStudents.size() == currentProject.capacity) {
			  std::string worstStudentAssignedToProject = findWorstStudent(projectedPrefsForCurrentCourses.at(currentProject.ID), currentProject.assignedStudents);
			  logMessage(fmt::format("Project {} is full, removing successors to {}", currentProject.ID, worstStudentAssignedToProject), outputBox);
			  deleteSuccessorPreferences(worstStudentAssignedToProject, currentProject, projectedPrefsForCurrentCourses.at(currentProject.ID), studentPrefsForCurrentCourses);
			}
			if (currentLecturer.assignedStudents.size() == currentLecturer.capacity) {
			  std::string worstStudentAssignedToProject = findWorstStudent(lecturerPreferences.at(currentLecturer.ID), currentLecturer.assignedStudents);
			  logMessage(fmt::format("Lecturer {} is full, removing successors to {}", currentLecturer.ID, worstStudentAssignedToProject), outputBox);
			  deleteAllSuccessorPreferences(worstStudentAssignedToProject, lecturerPreferences.at(currentLecturer.ID), projectedPrefsForCurrentCourses, lecturerProjects.at(currentLecturer.ID), studentPrefsForCurrentCourses);
			}


		  } else {
			done = true;
		  }
		} else {
		  done = true;
		}
	  }
	  // Students who couldn't be allocated by the main algorithm are allocated here instead using their topic preferences
	  std::vector<Student> studentsToBeAssignedByTopics = { unassignedStudentsFromCourses.begin(), unassignedStudentsFromCourses.end() };
	  for (auto &student : studentsToBeAssignedByTopics) {
		for (const auto &project : student.projectPreferences) {
		  Lecturer &lecturerOfHighestRankedProject = lecturersCurrentCourseRun.at(std::distance(lecturersCurrentCourseRun.begin(), std::find_if(lecturersCurrentCourseRun.begin(), lecturersCurrentCourseRun.end(), [&](const Lecturer &l) {
	  			auto projectObject = *std::find_if(projects.begin(), projects.end(), [&project](const Project& p){return p.ID==project;});
	  			return projectObject.lecturer == l.ID; })));
		  if (lecturerOfHighestRankedProject.assignedStudents.size() < lecturerOfHighestRankedProject.capacity) {
			for (auto &p : projects) {
			  if (p.assignedStudents.size() < p.capacity && p.lecturer == lecturerOfHighestRankedProject.ID) {
				logMessage(fmt::format("Student {} assigned to project {} using alternative method", student.ID, p.ID), outputBox);
				Student &studentInMainList = students.at(std::distance(students.begin(), std::find_if(students.begin(), students.end(), [&](const Student &s) { return student.ID == s.ID; })));
				studentInMainList.assignedProject = p.ID;
				studentInMainList.isAssigned = true;
				studentInMainList.assignedProjectRank = 10;
				lecturerOfHighestRankedProject.assignedStudents.push_back(student.ID);
				p.assignedStudents.push_back(student.ID);
				unassignedStudentsFromCourses.erase(std::remove_if(unassignedStudentsFromCourses.begin(), unassignedStudentsFromCourses.end(), [&](const Student &s) { return s.ID == student.ID; }), unassignedStudentsFromCourses.end());
				break;
			  }
			}
			break;
		  }
		}
	  }

	  // some students still cannot be assigned, so we use the topics associated with their top ranked projects to try and match them with a similar free projects
	  studentsToBeAssignedByTopics = { unassignedStudentsFromCourses.begin(), unassignedStudentsFromCourses.end() };
	  std::vector<Project> freeProjectsForTopicBasedAllocation;
	  for (const auto &project : projects) {
		if (project.assignedStudents.size() < project.capacity) {
		  const Lecturer &projectLecturer = *std::find_if(lecturersCurrentCourseRun.begin(), lecturersCurrentCourseRun.end(), [&](const Lecturer &l) {
				 auto projectObject = *std::find_if(projects.begin(), projects.end(), [&project](const Project& p){return p.ID==project.ID;});
				 return projectObject.lecturer == l.ID; });
		  if (projectLecturer.assignedStudents.size() < projectLecturer.assignedStudents.capacity()) {
			freeProjectsForTopicBasedAllocation.push_back(project);
		  }
		}
	  }
	  std::shuffle(freeProjectsForTopicBasedAllocation.begin(), freeProjectsForTopicBasedAllocation.end(), std::mt19937(std::random_device()()));
	  if (!studentsToBeAssignedByTopics.empty()) {
		for (auto &student : studentsToBeAssignedByTopics) {
		  std::vector<std::string> topRankedProjectTopics = *std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.ID == student.projectPreferences.at(0); })->projectTopics;
		  int counter = 0;
		  for (auto projectInStudentPreferences : student.projectPreferences) {
			topRankedProjectTopics = *std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return p.ID == projectInStudentPreferences; })->projectTopics;
			for (const auto &projectTopic : topRankedProjectTopics) {
			  for (auto &project : freeProjectsForTopicBasedAllocation) {
				if (std::find(project.suitableCourses.begin(), project.suitableCourses.end(), student.course) != project.suitableCourses.end()) {
				  if (std::find(project.projectTopics->begin(), project.projectTopics->end(), projectTopic) != project.projectTopics->end() && project.assignedStudents.size() < project.capacity) {
					Lecturer &projectLecturer = lecturersCurrentCourseRun.at(std::distance(lecturersCurrentCourseRun.begin(), std::find_if(lecturersCurrentCourseRun.begin(), lecturersCurrentCourseRun.end(), [&](const Lecturer &l) {
						   auto projectObject = *std::find_if(projects.begin(), projects.end(), [&project](const Project& p){return p.ID==project.ID;});
						   return projectObject.lecturer == l.ID; })));
					if (projectLecturer.assignedStudents.size() < projectLecturer.capacity) {
					  Project &projectInMainList = projects.at(std::distance(projects.begin(), std::find_if(projects.begin(), projects.end(), [&](const Project &p) { return project.ID == p.ID; })));
					  projectInMainList.assignedStudents.push_back(student.ID);
					  project.assignedStudents.push_back(student.ID);
					  logMessage(fmt::format("Student {} assigned to project {} ({}) using topics", student.ID, project.ID, projectLecturer.ID), outputBox);
					  Student &studentInMainList = students.at(std::distance(students.begin(), std::find_if(students.begin(), students.end(), [&](const Student &s) { return student.ID == s.ID; })));
					  studentInMainList.isAssigned = true;
					  studentInMainList.assignedProjectRank = 11 + counter;
					  counter++;
					  studentInMainList.assignedProject = project.ID;
					  unassignedStudentsFromCourses.erase(std::remove_if(unassignedStudentsFromCourses.begin(), unassignedStudentsFromCourses.end(), [&](const Student &s) { return s.ID == student.ID; }), unassignedStudentsFromCourses.end());
					  projectLecturer.assignedStudents.push_back(student.ID);
					  goto jmpPoint;
					}
				  }
				}
			  }
			}
		  }
		jmpPoint:
		  continue;
		}
	  }

	  for (auto &student : unassignedStudentsFromCourses) {
		localUnassignedStudents.push_back(student);
	  }
	  freeProjects.clear();
	  lecturerPreferences.clear();
	  unassignedStudentsFromCourses.clear();
	  studentPrefsForCurrentCourses.clear();

	  for (size_t idx = 0; idx < lecturersCurrentCourseRun.size(); idx++) {
		lecturers.at(idx).capacity = lecturersCurrentCourseRun.at(idx).capacity;
		for (const auto &student : lecturersCurrentCourseRun.at(idx).assignedStudents) {
		  lecturers.at(idx).assignedStudents.push_back(student);
		}
	  }
	  lecturersLastCourseRun = lecturersCurrentCourseRun;
	}
	logMessage(fmt::format("Algorithm finished, {} students remain unassigned", localUnassignedStudents.size()), outputBox);
  }

  void AbrahamV2::logMessage(const std::string &message, QTextEdit *outputBox, LoggingLevel level)
  {
	if (logSettings.logToFile && logSettings.verbosity >= level) {
	  std::ofstream output;
	  output.open(logSettings.logFilPath.c_str());
	  if (output.is_open()) {
		output << message << "\n";
	  }
	  output.close();
	}
	outputBox->append(QString::fromStdString(message));
  }

  [[maybe_unused]] void AbrahamV2::prettyPrintMap(const std::map<std::string, std::vector<std::string>> &map)
  {
	auto generateStringFromVector = [](const std::vector<std::string> &vec) -> std::string {
	  std::string out = "[";
	  for (const auto &e : vec) {
		out += fmt::format(" {},", e);
	  }
	  if (out.size() > 2) {
		out.erase(out.end() - 1);
	  }
	  out += "]";
	  return out;
	};
	fmt::print("Key          Value \n");
	fmt::print("------------|---------------------------------------------------\n");
	for (const auto &pair : map) {
	  fmt::print("{}: {} \n", pair.first, generateStringFromVector(pair.second));
	}
	fmt::print("------------|---------------------------------------------------\n");
  }

  std::string AbrahamV2::findWorstStudent(const std::vector<std::string> &preferences, const std::vector<std::string> &assignedStudents)
  {
	int maxIndex{ -1 };
	std::string worstStudentAssignedToProject;
	for (const auto &student : assignedStudents) {
	  auto it = std::find(preferences.begin(), preferences.end(), student);
	  if (it == preferences.end()) {
		return student;
	  }
	  if (std::distance(preferences.begin(), it) > maxIndex) {
		maxIndex = std::distance(preferences.begin(), it);
		worstStudentAssignedToProject = student;
	  }
	}
	return worstStudentAssignedToProject;
  }

  void AbrahamV2::breakAssociation(const std::string &student, Project &currentProject, Lecturer &currentLecturer, std::vector<Student> &unassignedStudents)
  {
	currentProject.assignedStudents.erase(std::remove(currentProject.assignedStudents.begin(), currentProject.assignedStudents.end(), student), currentProject.assignedStudents.end());
	currentLecturer.assignedStudents.erase(std::remove(currentLecturer.assignedStudents.begin(), currentLecturer.assignedStudents.end(), student), currentLecturer.assignedStudents.end());
	auto studentObject = std::find_if(students.begin(), students.end(), [&](const Student &s) {
	  return s.ID == student;
	});
	(*studentObject).isAssigned = false;
	(*studentObject).assignedProject = "";
	unassignedStudents.push_back(*studentObject);
  }

  void AbrahamV2::deleteSuccessorPreferences(const std::string &student, Project &currentProject, std::vector<std::string> &projectedPreferences, std::map<std::string, std::vector<std::string>> &studentPreferences)
  {
	auto it = std::find(projectedPreferences.begin(), projectedPreferences.end(), student);
	if (!(it == projectedPreferences.end() || static_cast<unsigned long>(std::distance(projectedPreferences.begin(), it)) == projectedPreferences.size() - 1)) {
	  std::vector<std::string> v{ projectedPreferences.begin() + std::distance(projectedPreferences.begin(), it) + 1, projectedPreferences.end() };
	  for (auto &s : v) {
		outputWidget->append(QString::fromStdString(fmt::format("Removing {}", s)));
		studentPreferences.at(s).erase(std::remove(studentPreferences.at(s).begin(), studentPreferences.at(s).end(), currentProject.ID), studentPreferences.at(s).end());
	  }
	}
  }

  void AbrahamV2::deleteAllSuccessorPreferences(const std::string &student, std::vector<std::string> &lecturerPreferences, std::map<std::string, std::vector<std::string>> &projectedPreferences, std::vector<std::string> &lecturerProjects, std::map<std::string, std::vector<std::string>> &studentPreferences)
  {
	auto it = std::find(lecturerPreferences.begin(), lecturerPreferences.end(), student);
	if (!(it == lecturerPreferences.end() || static_cast<unsigned long>(std::distance(lecturerPreferences.begin(), it)) == lecturerPreferences.size() - 1)) {
	  std::vector<std::string> studentsToRemove{ it + 1, lecturerPreferences.end() };
	  for (auto &sToRemove : studentsToRemove) {
		for (const auto &project : lecturerProjects) {
		  outputWidget->append(QString::fromStdString(fmt::format("Removing {} @ {}", project, sToRemove)));
		  projectedPreferences.at(project).erase(std::remove(projectedPreferences.at(project).begin(), projectedPreferences.at(project).end(), sToRemove), projectedPreferences.at(project).end());
		  studentPreferences.at(sToRemove).erase(std::remove(studentPreferences.at(sToRemove).begin(), studentPreferences.at(sToRemove).end(), project), studentPreferences.at(sToRemove).end());
		}
	  }
	}
  }

}// namespace SPRAL
//
