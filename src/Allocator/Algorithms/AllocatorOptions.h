//
// Created by tom on 09/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_ALLOCATOROPTIONS_H
#define STUDENTPROJECTALLOCATOR_ALLOCATOROPTIONS_H

#include <QtWidgets/QFormLayout>
#include <QtWidgets/QCheckBox>
#include <string>
#include <unordered_map>
#include <memory>
#include <variant>
#include <QtWidgets/QLabel>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QLineEdit>
#include <fmt/core.h>
#include <QtWidgets/QSpinBox>
#include <any>

namespace SPRAL {
  class AllocatorOption : public QObject
  {
	Q_OBJECT
  public:
	AllocatorOption() = default;
	AllocatorOption(std::string a_shortName, std::string a_displayName) : shortName(a_shortName), displayName(a_displayName){};
	virtual ~AllocatorOption() = default;
	std::string shortName{};
	std::string displayName{};
	virtual void draw(QFormLayout *widget) = 0;
	bool hasBeenDrawn{ false };
	std::any optionValue;
  };

  class BoolOption : public AllocatorOption
  {
  public:
	explicit BoolOption(std::string a_shortName, std::string a_displayName, bool initialValue) : AllocatorOption(std::move(a_shortName), std::move(a_displayName))
	{
	  this->optionValue = initialValue;
	  checkbox = new QCheckBox();
	  connect(checkbox, &QCheckBox::stateChanged, [this](int newState) {
		optionValue = static_cast<bool>(newState);
	  });
	}

	QCheckBox *checkbox{ nullptr };
	void draw(QFormLayout *layout) override
	{
	  if (!hasBeenDrawn) {
		const int currentRow = layout->rowCount();
		checkbox->setParent(layout->parentWidget());
		checkbox->setObjectName(QString::fromStdString(shortName));
		checkbox->setCheckState(std::any_cast<bool>(optionValue) ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);

		auto label = new QLabel(layout->parentWidget());
		label->setText(QCoreApplication::translate("MainWindow", displayName.c_str(), nullptr));
		label->setEnabled(true);
		layout->setWidget(currentRow, QFormLayout::LabelRole, label);
		layout->setWidget(currentRow, QFormLayout::FieldRole, checkbox);
	  }
	  hasBeenDrawn = true;
	}
  };

  class TextOption : public AllocatorOption
  {
  public:
	explicit TextOption(std::string a_shortName, std::string a_displayName) : AllocatorOption(std::move(a_shortName), std::move(a_displayName))
	{
	  lineEdit = new QLineEdit();
	  lineEdit->setText(QString::fromStdString(std::any_cast<std::string>(optionValue)));
	  connect(lineEdit, &QLineEdit::textEdited, [this](QString newString) {
		optionValue = newString.toStdString();
	  });
	}
	void draw(QFormLayout *layout) override
	{
	  if (!hasBeenDrawn) {
		const int currentRow = layout->rowCount();
		lineEdit->setParent(layout->parentWidget());
		lineEdit->setObjectName(QString::fromStdString(shortName));
		auto label = new QLabel(layout->parentWidget());
		label->setText(QCoreApplication::translate("MainWindow", displayName.c_str(), nullptr));
		label->setEnabled(true);
		layout->setWidget(currentRow, QFormLayout::LabelRole, label);
		layout->setWidget(currentRow, QFormLayout::FieldRole, lineEdit);
	  }
	  hasBeenDrawn = true;
	}

  private:
	QLineEdit *lineEdit{ nullptr };
  };

  class IntOption : public AllocatorOption
  {
  public:
	explicit IntOption(std::string a_shortName, std::string a_displayName, int initialValue, int min, int max) : AllocatorOption(std::move(a_shortName), std::move(a_displayName))
	{
	  this->optionValue = initialValue;
	  spinBox = new QSpinBox();
	  spinBox->setRange(min, max);
	  connect(spinBox, QOverload<int>::of(&QSpinBox::valueChanged), [this](int newValue) {
		optionValue = newValue;
	  });
	}

	void draw(QFormLayout *layout) override
	{
	  if (!hasBeenDrawn) {
		const int currentRow = layout->rowCount();
		spinBox->setParent(layout->parentWidget());
		spinBox->setObjectName(QString::fromStdString(shortName));
		spinBox->setValue(std::any_cast<int>(optionValue));

		auto label = new QLabel(layout->parentWidget());
		label->setText(QCoreApplication::translate("MainWindow", displayName.c_str(), nullptr));
		label->setEnabled(true);
		layout->setWidget(currentRow, QFormLayout::LabelRole, label);
		layout->setWidget(currentRow, QFormLayout::FieldRole, spinBox);
	  }
	  hasBeenDrawn = true;
	}

  private:
	QSpinBox *spinBox{ nullptr };
  };

  class DoubleOption : public AllocatorOption
  {
  public:
	explicit DoubleOption(std::string a_shortName, std::string a_displayName, double initialValue, double min, double max) : AllocatorOption(std::move(a_shortName), std::move(a_displayName))
	{
	  this->optionValue = initialValue;
	  spinBox = new QDoubleSpinBox();
	  spinBox->setRange(min, max);
	  connect(spinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](int newValue) {
		optionValue = newValue;
	  });
	}
	void draw(QFormLayout *layout) override
	{
	  if (!hasBeenDrawn) {
		const int currentRow = layout->rowCount();
		spinBox->setParent(layout->parentWidget());
		spinBox->setObjectName(QString::fromStdString(shortName));
		spinBox->setValue(std::any_cast<double>(optionValue));

		auto label = new QLabel(layout->parentWidget());
		label->setText(QCoreApplication::translate("MainWindow", displayName.c_str(), nullptr));
		label->setEnabled(true);
		layout->setWidget(currentRow, QFormLayout::LabelRole, label);
		layout->setWidget(currentRow, QFormLayout::FieldRole, spinBox);
	  }
	  hasBeenDrawn = true;
	}

  private:
	QDoubleSpinBox *spinBox{ nullptr };
  };

  class HLineOption : public AllocatorOption
  {
  public:
	explicit HLineOption() : AllocatorOption("", "")
	{}
	void draw(QFormLayout *layout) override
	{
	  if (!hasBeenDrawn) {
		const int currentRow = layout->rowCount();
		labelLine = new QFrame(layout->parentWidget());
		labelLine->setObjectName(QString::fromUtf8("labelLine"));
		labelLine->setFrameShape(QFrame::HLine);
		labelLine->setFrameShadow(QFrame::Sunken);
		linePolicy->setHeightForWidth(labelLine->sizePolicy().hasHeightForWidth());
		labelLine->setSizePolicy(*linePolicy);
		labelLine->setLineWidth(1);
		layout->setWidget(currentRow, QFormLayout::FieldRole, labelLine);
		hasBeenDrawn = true;
	  }
	}

  private:
	QFrame *labelLine{ nullptr };
	QFrame *fieldLine{ nullptr };
	QSizePolicy *linePolicy = new QSizePolicy{ QSizePolicy::Expanding, QSizePolicy::Fixed };
  };

}// namespace SPRAL
#endif//STUDENTPROJECTALLOCATOR_ALLOCATOROPTIONS_H
