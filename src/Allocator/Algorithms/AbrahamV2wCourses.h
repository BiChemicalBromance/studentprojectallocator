//
// Created by tom on 26/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_ABRAHAMV2WCOURSES_H
#define STUDENTPROJECTALLOCATOR_ABRAHAMV2WCOURSES_H
#include "../Allocator.h"


namespace SPRAL {
  class AbrahamV2 : public Allocator
  {
  public:
	AbrahamV2(const std::vector<Student> &s, const std::vector<Project> &p, const std::vector<Lecturer> &l, const std::vector<Course> &c);

	bool Validate() override;
	void operator()(QTextEdit *outputBox) override;
	void convertIDFormat() override;
	AllocationResults getResults() override;

  private:
	QTextEdit *outputWidget{ nullptr };
	std::unordered_map<std::string, std::vector<std::string>> studentPreferencesMap;
	std::vector<Student> localUnassignedStudents;
	static std::string findWorstStudent(const std::vector<std::string> &preferences, const std::vector<std::string> &assignedStudents);
	void logMessage(const std::string &message, QTextEdit *outputBox, LoggingLevel level = LoggingLevel::NORMAL);
	void breakAssociation(const std::string &student, Project &currentProject, Lecturer &currentLecturer, std::vector<Student> &unassignedStudents);
	void deleteSuccessorPreferences(const std::string &student, Project &currentProject, std::vector<std::string> &projectedPreferences, std::map<std::string, std::vector<std::string>> &studentPreferences);
	void deleteAllSuccessorPreferences(const std::string &student, std::vector<std::string> &lecturerPreferences, std::map<std::string, std::vector<std::string>> &projectedPreferences, std::vector<std::string> &lecturerProjects, std::map<std::string, std::vector<std::string>> &studentPreferences);
	[[maybe_unused]] static void prettyPrintMap(const std::map<std::string, std::vector<std::string>> &map);
  };


}// namespace SPRAL
#endif//STUDENTPROJECTALLOCATOR_ABRAHAMV2WCOURSES_H
