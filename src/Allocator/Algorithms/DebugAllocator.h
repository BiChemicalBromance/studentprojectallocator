//
// Created by tom on 08/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_DEBUGALLOCATOR_H
#define STUDENTPROJECTALLOCATOR_DEBUGALLOCATOR_H
#include "../Allocator.h"
#include <fmt/core.h>

namespace SPRAL {
  // Quick debug allocator to dump all imported data to the output window for debugging purposes
  class DebugAllocator : public Allocator
  {
  public:
	explicit DebugAllocator(std::vector<Student> s, std::vector<Project> p, std::vector<Lecturer> l, std::vector<Course> c) : Allocator(s, p, l, c)
	{
	  algorithmOptions.emplace("printToScreen", std::make_unique<BoolOption>("printToScreenCheckbox", "Print Output To Screen1", true));
	  algorithmOptions.emplace("line1", std::make_unique<HLineOption>());
	  algorithmOptions.emplace("redValue", std::make_unique<IntOption>("redValue", "Red Value", 100, 0, 255));
	  algorithmOptions.emplace("blueValue", std::make_unique<IntOption>("blueValue", "Blue Value", 100, 0, 255));
	  algorithmOptions.emplace("greenValue", std::make_unique<IntOption>("greenValue", "Green Value", 100, 0, 255));

	  algorithmOptionDrawOrder = { "printToScreen", "line1", "redValue", "blueValue", "greenValue" };
	}
	bool Validate() override
	{
	  return true;
	}

	void operator()(QTextEdit *outputBox) override
	{
	  const int red = std::any_cast<int>(algorithmOptions["redValue"]->optionValue);
	  const int green = std::any_cast<int>(algorithmOptions["greenValue"]->optionValue);
	  const int blue = std::any_cast<int>(algorithmOptions["blueValue"]->optionValue);
	  outputBox->setTextColor(QColor(red, green, blue));
	  if (!Validate()) {
		return;
	  }
	  std::map<int, std::vector<std::string>> courseHierarchy;
	  for (const auto &course : courses) {
		courseHierarchy[course.priority].push_back(course.ID);
	  }
	  for (const auto &courseList : courseHierarchy) {
		for (const auto &course : courseList.second) {
		  for (const auto &student : students) {
			if (student.course == course) {
			  if (std::any_cast<bool>(algorithmOptions["printToScreen"]->optionValue)) {
				std::string line = fmt::format("{}", student);
				outputBox->append(QString::fromStdString(line));
			  }
			}
		  }
		}
	  }
	  return;
	}
	void convertIDFormat() override
	{}

	AllocationResults getResults() override
	{
	  return AllocationResults();
	}
  };
}// namespace SPRAL
#endif//STUDENTPROJECTALLOCATOR_DEBUGALLOCATOR_H
