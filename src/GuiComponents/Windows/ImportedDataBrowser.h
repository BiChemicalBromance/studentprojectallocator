//
// Created by tom on 05/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_IMPORTEDDATABROWSER_H
#define STUDENTPROJECTALLOCATOR_IMPORTEDDATABROWSER_H

#include "ui_ImportedDataBrowser.h"
#include <rapidcsv.h>


//TODO: add JSON-like previewer which is live and reactive to the columns selected in the comboboxes

class ImportedDataBrowser : public QDialog
  , public Ui::DataBrowser
{
  Q_OBJECT

public:
  enum class DataTypes {
	BLANK,
	STUDENTS,
	PROJECTS,
	LECTURERS,
	COURSES
  };
  Q_ENUM(DataTypes)
  explicit ImportedDataBrowser(QWidget *parent = nullptr);
  void setCurrentSpreadsheet(rapidcsv::Document *sheet);

private:
  std::unordered_map<std::string, std::string> currentPageSettings;
  rapidcsv::Document *currentSpreadsheet{ nullptr };
private slots:
  void changeInfoWidgetPage(const QString &text);

public:
  void accept() override;
};


#endif//STUDENTPROJECTALLOCATOR_IMPORTEDDATABROWSER_H
