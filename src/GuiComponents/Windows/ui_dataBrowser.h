/********************************************************************************
** Form generated from reading UI file 'dataBrowserXjztXh.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef DATABROWSERXJZTXH_H
#define DATABROWSERXJZTXH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DataViewer
{
public:
    QVBoxLayout *verticalLayout_4;
    QTabWidget *tabWidget;
    QWidget *Students;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *studentTree;
    QWidget *projects;
    QVBoxLayout *verticalLayout_3;
    QTreeWidget *projectsTree;
    QWidget *lecturers;
    QVBoxLayout *verticalLayout;
    QTreeWidget *lecturerTree;
    QWidget *courses;
    QVBoxLayout *verticalLayout_5;
    QTreeWidget *coursesTree;

    void setupUi(QDialog *DataViewer)
    {
        if (DataViewer->objectName().isEmpty())
            DataViewer->setObjectName(QString::fromUtf8("DataViewer"));
        DataViewer->resize(732, 803);
        verticalLayout_4 = new QVBoxLayout(DataViewer);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        tabWidget = new QTabWidget(DataViewer);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setAutoFillBackground(false);
        tabWidget->setTabShape(QTabWidget::Rounded);
        Students = new QWidget();
        Students->setObjectName(QString::fromUtf8("Students"));
        verticalLayout_2 = new QVBoxLayout(Students);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        studentTree = new QTreeWidget(Students);
        studentTree->setObjectName(QString::fromUtf8("studentTree"));
        studentTree->header()->setDefaultSectionSize(200);

        verticalLayout_2->addWidget(studentTree);

        tabWidget->addTab(Students, QString());
        projects = new QWidget();
        projects->setObjectName(QString::fromUtf8("projects"));
        verticalLayout_3 = new QVBoxLayout(projects);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        projectsTree = new QTreeWidget(projects);
        projectsTree->setObjectName(QString::fromUtf8("projectsTree"));

        verticalLayout_3->addWidget(projectsTree);

        tabWidget->addTab(projects, QString());
        lecturers = new QWidget();
        lecturers->setObjectName(QString::fromUtf8("lecturers"));
        verticalLayout = new QVBoxLayout(lecturers);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lecturerTree = new QTreeWidget(lecturers);
        lecturerTree->setObjectName(QString::fromUtf8("lecturerTree"));

        verticalLayout->addWidget(lecturerTree);

        tabWidget->addTab(lecturers, QString());
        courses = new QWidget();
        courses->setObjectName(QString::fromUtf8("courses"));
        verticalLayout_5 = new QVBoxLayout(courses);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        coursesTree = new QTreeWidget(courses);
        coursesTree->setObjectName(QString::fromUtf8("coursesTree"));

        verticalLayout_5->addWidget(coursesTree);

        tabWidget->addTab(courses, QString());

        verticalLayout_4->addWidget(tabWidget);


        retranslateUi(DataViewer);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(DataViewer);
    } // setupUi

    void retranslateUi(QDialog *DataViewer)
    {
        DataViewer->setWindowTitle(QCoreApplication::translate("DataViewer", "Dialog", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = studentTree->headerItem();
        ___qtreewidgetitem->setText(1, QCoreApplication::translate("DataViewer", "Value", nullptr));
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("DataViewer", "Key", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Students), QCoreApplication::translate("DataViewer", "Students", nullptr));
        QTreeWidgetItem *___qtreewidgetitem1 = projectsTree->headerItem();
        ___qtreewidgetitem1->setText(1, QCoreApplication::translate("DataViewer", "Value", nullptr));
        ___qtreewidgetitem1->setText(0, QCoreApplication::translate("DataViewer", "Key", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(projects), QCoreApplication::translate("DataViewer", "Projects", nullptr));
        QTreeWidgetItem *___qtreewidgetitem2 = lecturerTree->headerItem();
        ___qtreewidgetitem2->setText(1, QCoreApplication::translate("DataViewer", "Value", nullptr));
        ___qtreewidgetitem2->setText(0, QCoreApplication::translate("DataViewer", "Key", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(lecturers), QCoreApplication::translate("DataViewer", "Lecturers", nullptr));
        QTreeWidgetItem *___qtreewidgetitem3 = coursesTree->headerItem();
        ___qtreewidgetitem3->setText(1, QCoreApplication::translate("DataViewer", "Value", nullptr));
        ___qtreewidgetitem3->setText(0, QCoreApplication::translate("DataViewer", "Key", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(courses), QCoreApplication::translate("DataViewer", "Courses", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DataViewer: public Ui_DataViewer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // DATABROWSERXJZTXH_H
