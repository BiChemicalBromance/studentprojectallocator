//
// Created by tom on 05/10/2020.
//
#include <fmt/core.h>
#include "ImportedDataBrowser.h"
#include <QDebug>
#include <QtCore/QMetaEnum>
#include <QtWidgets/QMessageBox>
#include <algorithm>
#include <unordered_set>
ImportedDataBrowser::ImportedDataBrowser(QWidget *parent) : QDialog(parent)
{
  currentSpreadsheet = nullptr;
  setupUi(this);
  stackedWidget->setCurrentIndex(0);
  setWindowTitle("Import Data");
  connect(DataTypeCombo, &QComboBox::currentTextChanged, this, &ImportedDataBrowser::changeInfoWidgetPage);
  const QMetaObject &metaObject = ImportedDataBrowser::staticMetaObject;
  QMetaEnum tempMetaEnum = metaObject.enumerator(ImportedDataBrowser::staticMetaObject.indexOfEnumerator("DataTypes"));
  for (int i = 0; i < tempMetaEnum.keyCount(); i++) {
	std::string thisItem = tempMetaEnum.key(i);
	std::string recasedView;
	size_t counter{ 0 };
	if (thisItem == "BLANK") {
	  DataTypeCombo->addItem("");
	} else {
	  for (auto &character : thisItem) {
		if (counter == 0) {
		  recasedView.push_back(character);
		} else {
		  recasedView.push_back(static_cast<char>(tolower(character)));
		}
		counter++;
	  }
	  DataTypeCombo->addItem(QString::fromStdString(recasedView));
	}
  }
}

void ImportedDataBrowser::setCurrentSpreadsheet(rapidcsv::Document *sheet)
{
  currentSpreadsheet = sheet;
}

void ImportedDataBrowser::changeInfoWidgetPage(const QString &text)
{
  QMetaEnum tempMetaEnum = ImportedDataBrowser::staticMetaObject.enumerator(ImportedDataBrowser::staticMetaObject.indexOfEnumerator("DataTypes"));
  if (DataTypeCombo->count() < tempMetaEnum.keyCount()) {
	return;
  }
  // TODO Disable accept button until required fields are filled in
  // TODO Check for repeats
  const auto columnHeaders = currentSpreadsheet->GetColumnNames();
  QStringList headerQStringList;
  std::for_each(columnHeaders.begin(), columnHeaders.end(), [&](std::string header) {
	headerQStringList.push_back(QString::fromStdString(header));
  });
  switch (tempMetaEnum.keyToValue(text.toUpper().toLatin1())) {
	case static_cast<int>(DataTypes::STUDENTS): {
	  stackedWidget->setCurrentIndex(4);
	  auto currentComboBoxes = stackedWidget->currentWidget()->findChildren<QComboBox *>();
	  foreach (QComboBox *child, currentComboBoxes) {
		child->addItem("");
		child->addItems(headerQStringList);
	  }
	  break;
	}
	case static_cast<int>(DataTypes::PROJECTS): {
	  stackedWidget->setCurrentIndex(1);
	  auto currentComboBoxes = stackedWidget->currentWidget()->findChildren<QComboBox *>();
	  foreach (QComboBox *child, currentComboBoxes) {
		child->addItem("");
		child->addItems(headerQStringList);
	  }
	  break;
	}
	case static_cast<int>(DataTypes::LECTURERS): {
	  stackedWidget->setCurrentIndex(2);
	  auto currentComboBoxes = stackedWidget->currentWidget()->findChildren<QComboBox *>();
	  foreach (QComboBox *child, currentComboBoxes) {
		child->addItem("");
		child->addItems(headerQStringList);
	  }
	  break;
	}
	case static_cast<int>(DataTypes::COURSES): {
	  stackedWidget->setCurrentIndex(3);
	  auto currentComboBoxes = stackedWidget->currentWidget()->findChildren<QComboBox *>();
	  foreach (QComboBox *child, currentComboBoxes) {
		child->addItem("");
		child->addItems(headerQStringList);
	  }
	  break;
	}
	default: {
	  stackedWidget->setCurrentIndex(0);
	  break;
	}
  }
}
void ImportedDataBrowser::accept()
{
  //Checking non-optional boxes have been filled in
  std::unordered_map<std::string, bool> nonOptionalCombosFilled{ false };
  if (DataTypeCombo->currentIndex() == 0) {
	QMessageBox msgBox;
	msgBox.setText("No data type selected");
	msgBox.exec();
	return;
  }

  const auto comboboxesOnPage = stackedWidget->currentWidget()->findChildren<QComboBox *>();
  for (const auto &combobox : comboboxesOnPage) {
	if (combobox->currentIndex() == 0) {
	  nonOptionalCombosFilled.emplace(combobox->objectName().toStdString(), false);
	}
  }


  bool isFieldUnset{ false };

  // TODO Extend this into a customisable list of optional fields, linking in with a wider parsed-input approach and external functions
  for (const auto &it : nonOptionalCombosFilled) {
	if (!it.second) {
	  if (DataTypeCombo->currentText() == "Students") {
		if (!(it.first == "FirstTopicPrefCombo" || it.first == "LastTopicPrefCombo_3")) {
		  isFieldUnset = true;
		  break;
		}
	  }

	  if (DataTypeCombo->currentText() == "Lecturers") {
		if (!(it.first == "NameCombo" || it.first == "Courses_Combo" || it.first == "TopicsCombo_2")) {
		  isFieldUnset = true;
		  break;
		}
	  }
	  if (DataTypeCombo->currentText() == "Projects") {
		if (it.first != "TitleCombo" && it.first != "TopicsCombo") {
		  isFieldUnset = true;
		  break;
		}
	  }
	  if (DataTypeCombo->currentText() == "Courses") {
		isFieldUnset = true;
		break;
	  }
	}
  }
  if (isFieldUnset) {
	QMessageBox mbox;
	mbox.setText("Some non-optional field has been left blank - please correct before moving on");
	mbox.exec();
	return;
  }
  // Checking entered data is valid (i.e. not all the same)
  // Any other validation can happen here
  std::vector<std::string> options;
  for (const auto combobox : comboboxesOnPage) {
	if (combobox->currentText() != "") {
	  options.push_back(combobox->currentText().toStdString());
	}
  }
  std::unordered_set<std::string> optionsSet(options.begin(), options.end());
  // TODO make this more elegant and able to display which element is repeatd
  if (optionsSet.size() != options.size()) {
	QMessageBox msgBox;
	msgBox.setText("Options are not unique - check options");
	msgBox.exec();
	return;
  }
  QDialog::accept();
}