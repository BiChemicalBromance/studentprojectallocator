//
// Created by tom on 26/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_DATAVIEWER_H
#define STUDENTPROJECTALLOCATOR_DATAVIEWER_H

#include "ui_dataBrowser.h"

class DataViewer : public QDialog
  , public Ui::DataViewer
{

public:
  DataViewer(QWidget *parent = nullptr) : QDialog(parent)
  {
	setupUi(this);
	tabWidget->setCurrentIndex(0);
	tabWidget->setTabEnabled(0, false);
	tabWidget->setTabEnabled(1, false);
	tabWidget->setTabEnabled(2, false);
	tabWidget->setTabEnabled(3, false);
	studentTree->header()->setDefaultSectionSize(200);
	projectsTree->header()->setDefaultSectionSize(200);
	coursesTree->header()->setDefaultSectionSize(200);
	lecturerTree->header()->setDefaultSectionSize(200);
	setWindowTitle("Data Viewer");
  }
};
#endif//STUDENTPROJECTALLOCATOR_DATAVIEWER_H
