/********************************************************************************
** Form generated from reading UI file 'MainWindowcDlmcw.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAINWINDOWCDLMCW_H
#define MAINWINDOWCDLMCW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QtCharts"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionImport_Data;
    QAction *actionResults;
    QAction *actionSession;
    QAction *actionSession_2;
    QAction *actionExit;
    QAction *actionView_Help;
    QAction *actionAbout;
    QAction *actionEdit;
    QAction *actionView_All;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QWidget *algoSettings;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *formLayout;
    QLabel *logFileNameLabel;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *logFilePath;
    QPushButton *filerFinder;
    QLabel *loggingTFLabel;
    QCheckBox *logToFile;
    QComboBox *verbosityOptionsCombo;
    QLabel *loggingLevelLabel;
    QFrame *line;
    QFormLayout *formLayout_3;
    QLabel *label_7;
    QComboBox *algoChoiceCombo;
    QStackedWidget *algoSpecificSettings;
    QWidget *page;
    QVBoxLayout *verticalLayout_5;
    QFormLayout *formLayout_4;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_6;
    QFormLayout *optionsFormLayout_2;
    QWidget *page_3;
    QVBoxLayout *verticalLayout_7;
    QFormLayout *optionsFormLayout_3;
    QWidget *page_4;
    QVBoxLayout *verticalLayout_10;
    QFormLayout *formLayout_5;
    QPushButton *runButton;
    QWidget *main;
    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    QWidget *StatsDisplay;
    QVBoxLayout *verticalLayout_3;
    QTabWidget *tabWidget;
    QWidget *importedDtaStats;
    QVBoxLayout *verticalLayout_4;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_3;
    QFormLayout *formLayout_2;
	QLabel *label;
	QLabel *label_2;
	QLabel *label_3;
	QLabel *label_4;
	QLabel *label_5;
	QLabel *label_6;
	QLineEdit *NumberOfStudents;
	QLineEdit *NumberOfProjects;
	QLineEdit *NumberOfLectuerers;
	QLineEdit *AverageProjectsPerLecturer;
	QLineEdit *AverageProjectPopularity;
	QLineEdit *NumberIgnoredProjects;
	QVBoxLayout *verticalLayout_9;
	QLabel *label_9;
	QComboBox *projectStatisticCombo;
	QHBoxLayout *horizontalLayout_4;
	QLabel *label_10;
	QLineEdit *capacityDisplay;
	QLabel *label_11;
	QLineEdit *lecturerDisplay;
	QHBoxLayout *horizontalLayout_6;
	QLabel *label_30;
	QLineEdit *lineEdit;
	QChartView *projectStatisticChart;
	QVBoxLayout *verticalLayout_8;
	QLabel *label_8;
	QComboBox *studentStatisticCombo;
	QChartView *studentStatsChart;
	QWidget *outputDataStats;
	QHBoxLayout *horizontalLayout_5;
	QVBoxLayout *verticalLayout_13;
	QComboBox *allocationResultsChoser;
	QFormLayout *formLayout_7;
	QLabel *label_14;
	QLabel *avgRank;
	QLabel *label_16;
	QLabel *numberUnassignedStudents;
	QLabel *label_18;
	QLabel *randomDistribute;
	QLabel *label_20;
	QLabel *topicsDistribute;
	QLabel *label_22;
	QLabel *label_23;
	QLabel *label_24;
	QLabel *label_25;
	QLabel *numberFreeProjects;
	QLabel *numberFreeLecturerSlots;
	QLabel *numberOversubscribedLecturers;
	QSpacerItem *verticalSpacer;
	QSpacerItem *verticalSpacer_2;
	QSpacerItem *verticalSpacer_3;
	QLabel *numverOversubsrcibedProjects;
	QLabel *label_15;
	QLabel *label_17;
	QLabel *allocationAlgorithmName;
	QLabel *allocationRunDatetime;
	QVBoxLayout *verticalLayout_11;
	QLabel *label_12;
	QListWidget *freeProjectsList;
	QWidget *widget_3;
	QVBoxLayout *verticalLayout_16;
	QFormLayout *formLayout_8;
	QLabel *label_28;
	QLabel *freeProjectID;
	QLabel *label_31;
	QLabel *label_32;
	QLabel *label_33;
	QLabel *label_34;
	QLabel *label_35;
	QLabel *freeProjectTitle;
	QLabel *freeProjectRemainingCapacity;
	QLabel *freeProjectRemainingLecturerCapacity;
	QTextBrowser *freeProjectTopics;
	QTextBrowser *freeProjectCourses;
	QLabel *label_29;
	QLabel *freeProjectLecturer;
	QVBoxLayout *verticalLayout_12;
	QVBoxLayout *verticalLayout_14;
	QLabel *label_13;
	QListWidget *unassignedStudentsList;
	QWidget *widget_2;
	QVBoxLayout *verticalLayout_15;
	QFormLayout *formLayout_6;
	QLabel *label_19;
	QLabel *label_21;
	QLabel *label_26;
	QLabel *label_27;
	QLabel *unassignedStudentID;
	QLabel *unassignedStudentCourse;
	QTextBrowser *unassignedStudentTopicPreferences;
	QTextBrowser *unassignedStudentProjectPreferences;
	QWidget *outputGraphs;
	QHBoxLayout *horizontalLayout_7;
	QChartView *outputStats;
	QChartView *outputStatsCum;
	QTextEdit *outputBox;
	QMenuBar *menubar;
	QMenu *menuFile;
	QMenu *menuSave;
	QMenu *menuOpen;
	QMenu *menuData;
	QMenu *menuHelp;
	QStatusBar *statusbar;

	void setupUi(QMainWindow *MainWindow)
	{
	  if (MainWindow->objectName().isEmpty())
		MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
	  MainWindow->resize(1600, 863);
	  MainWindow->setUnifiedTitleAndToolBarOnMac(false);
        actionImport_Data = new QAction(MainWindow);
        actionImport_Data->setObjectName(QString::fromUtf8("actionImport_Data"));
        actionResults = new QAction(MainWindow);
        actionResults->setObjectName(QString::fromUtf8("actionResults"));
        actionSession = new QAction(MainWindow);
        actionSession->setObjectName(QString::fromUtf8("actionSession"));
        actionSession_2 = new QAction(MainWindow);
        actionSession_2->setObjectName(QString::fromUtf8("actionSession_2"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionView_Help = new QAction(MainWindow);
        actionView_Help->setObjectName(QString::fromUtf8("actionView_Help"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionEdit = new QAction(MainWindow);
        actionEdit->setObjectName(QString::fromUtf8("actionEdit"));
        actionView_All = new QAction(MainWindow);
        actionView_All->setObjectName(QString::fromUtf8("actionView_All"));
		centralwidget = new QWidget(MainWindow);
		centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
		horizontalLayout = new QHBoxLayout(centralwidget);
		horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
		algoSettings = new QWidget(centralwidget);
		algoSettings->setObjectName(QString::fromUtf8("algoSettings"));
		QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
		sizePolicy.setHorizontalStretch(0);
		sizePolicy.setVerticalStretch(0);
		sizePolicy.setHeightForWidth(algoSettings->sizePolicy().hasHeightForWidth());
		algoSettings->setSizePolicy(sizePolicy);
		algoSettings->setMinimumSize(QSize(400, 0));
		verticalLayout_2 = new QVBoxLayout(algoSettings);
		verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
		formLayout = new QFormLayout();
		formLayout->setObjectName(QString::fromUtf8("formLayout"));
		formLayout->setContentsMargins(-1, 15, -1, -1);
		logFileNameLabel = new QLabel(algoSettings);
		logFileNameLabel->setObjectName(QString::fromUtf8("logFileNameLabel"));
		logFileNameLabel->setEnabled(true);
		QFont font;
		font.setBold(true);
		font.setWeight(75);
		logFileNameLabel->setFont(font);

		formLayout->setWidget(2, QFormLayout::LabelRole, logFileNameLabel);

		horizontalLayout_2 = new QHBoxLayout();
		horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
		horizontalLayout_2->setContentsMargins(0, -1, -1, -1);
		logFilePath = new QLineEdit(algoSettings);
		logFilePath->setObjectName(QString::fromUtf8("logFilePath"));

		horizontalLayout_2->addWidget(logFilePath);

		filerFinder = new QPushButton(algoSettings);
		filerFinder->setObjectName(QString::fromUtf8("filerFinder"));

		horizontalLayout_2->addWidget(filerFinder);


		formLayout->setLayout(2, QFormLayout::FieldRole, horizontalLayout_2);

		loggingTFLabel = new QLabel(algoSettings);
		loggingTFLabel->setObjectName(QString::fromUtf8("loggingTFLabel"));
		loggingTFLabel->setFont(font);

		formLayout->setWidget(0, QFormLayout::LabelRole, loggingTFLabel);

		logToFile = new QCheckBox(algoSettings);
		logToFile->setObjectName(QString::fromUtf8("logToFile"));

		formLayout->setWidget(0, QFormLayout::FieldRole, logToFile);

		verbosityOptionsCombo = new QComboBox(algoSettings);
		verbosityOptionsCombo->setObjectName(QString::fromUtf8("verbosityOptionsCombo"));
		QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
		sizePolicy1.setHorizontalStretch(0);
		sizePolicy1.setVerticalStretch(0);
		sizePolicy1.setHeightForWidth(verbosityOptionsCombo->sizePolicy().hasHeightForWidth());
		verbosityOptionsCombo->setSizePolicy(sizePolicy1);

		formLayout->setWidget(1, QFormLayout::FieldRole, verbosityOptionsCombo);

		loggingLevelLabel = new QLabel(algoSettings);
		loggingLevelLabel->setObjectName(QString::fromUtf8("loggingLevelLabel"));
		loggingLevelLabel->setFont(font);

		formLayout->setWidget(1, QFormLayout::LabelRole, loggingLevelLabel);


		verticalLayout_2->addLayout(formLayout);

		line = new QFrame(algoSettings);
		line->setObjectName(QString::fromUtf8("line"));
		line->setFrameShape(QFrame::HLine);
		line->setFrameShadow(QFrame::Sunken);

		verticalLayout_2->addWidget(line);

		formLayout_3 = new QFormLayout();
		formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
		formLayout_3->setContentsMargins(-1, 0, -1, -1);
		label_7 = new QLabel(algoSettings);
		label_7->setObjectName(QString::fromUtf8("label_7"));
		label_7->setFont(font);

		formLayout_3->setWidget(0, QFormLayout::LabelRole, label_7);

		algoChoiceCombo = new QComboBox(algoSettings);
		algoChoiceCombo->setObjectName(QString::fromUtf8("algoChoiceCombo"));
		QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
		sizePolicy2.setHorizontalStretch(0);
		sizePolicy2.setVerticalStretch(0);
		sizePolicy2.setHeightForWidth(algoChoiceCombo->sizePolicy().hasHeightForWidth());
		algoChoiceCombo->setSizePolicy(sizePolicy2);

		formLayout_3->setWidget(0, QFormLayout::FieldRole, algoChoiceCombo);


        verticalLayout_2->addLayout(formLayout_3);

        algoSpecificSettings = new QStackedWidget(algoSettings);
        algoSpecificSettings->setObjectName(QString::fromUtf8("algoSpecificSettings"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout_5 = new QVBoxLayout(page);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));

        verticalLayout_5->addLayout(formLayout_4);

        algoSpecificSettings->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        verticalLayout_6 = new QVBoxLayout(page_2);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        optionsFormLayout_2 = new QFormLayout();
        optionsFormLayout_2->setObjectName(QString::fromUtf8("optionsFormLayout_2"));

        verticalLayout_6->addLayout(optionsFormLayout_2);

        algoSpecificSettings->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        verticalLayout_7 = new QVBoxLayout(page_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        optionsFormLayout_3 = new QFormLayout();
        optionsFormLayout_3->setObjectName(QString::fromUtf8("optionsFormLayout_3"));

        verticalLayout_7->addLayout(optionsFormLayout_3);

        algoSpecificSettings->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        verticalLayout_10 = new QVBoxLayout(page_4);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));

        verticalLayout_10->addLayout(formLayout_5);

        algoSpecificSettings->addWidget(page_4);

        verticalLayout_2->addWidget(algoSpecificSettings);

        runButton = new QPushButton(algoSettings);
        runButton->setObjectName(QString::fromUtf8("runButton"));

        verticalLayout_2->addWidget(runButton);


        horizontalLayout->addWidget(algoSettings);

        main = new QWidget(centralwidget);
        main->setObjectName(QString::fromUtf8("main"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(1);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(main->sizePolicy().hasHeightForWidth());
        main->setSizePolicy(sizePolicy3);
        verticalLayout = new QVBoxLayout(main);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        splitter = new QSplitter(main);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        StatsDisplay = new QWidget(splitter);
        StatsDisplay->setObjectName(QString::fromUtf8("StatsDisplay"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(StatsDisplay->sizePolicy().hasHeightForWidth());
        StatsDisplay->setSizePolicy(sizePolicy4);
        verticalLayout_3 = new QVBoxLayout(StatsDisplay);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, -1, -1, -1);
        tabWidget = new QTabWidget(StatsDisplay);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        sizePolicy3.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy3);
        tabWidget->setMinimumSize(QSize(0, 0));
		importedDtaStats = new QWidget();
		importedDtaStats->setObjectName(QString::fromUtf8("importedDtaStats"));
		QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Expanding);
		sizePolicy5.setHorizontalStretch(0);
		sizePolicy5.setVerticalStretch(0);
		sizePolicy5.setHeightForWidth(importedDtaStats->sizePolicy().hasHeightForWidth());
		importedDtaStats->setSizePolicy(sizePolicy5);
		importedDtaStats->setAutoFillBackground(false);
		verticalLayout_4 = new QVBoxLayout(importedDtaStats);
		verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
		widget = new QWidget(importedDtaStats);
		widget->setObjectName(QString::fromUtf8("widget"));
		sizePolicy5.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
		widget->setSizePolicy(sizePolicy5);
		horizontalLayout_3 = new QHBoxLayout(widget);
		horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
		formLayout_2 = new QFormLayout();
		formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
		label = new QLabel(widget);
		label->setObjectName(QString::fromUtf8("label"));
		QFont font1;
		font1.setBold(true);
		font1.setWeight(75);
		font1.setKerning(true);
		label->setFont(font1);

		formLayout_2->setWidget(0, QFormLayout::LabelRole, label);

		label_2 = new QLabel(widget);
		label_2->setObjectName(QString::fromUtf8("label_2"));
		label_2->setFont(font1);

		formLayout_2->setWidget(1, QFormLayout::LabelRole, label_2);

		label_3 = new QLabel(widget);
		label_3->setObjectName(QString::fromUtf8("label_3"));
		label_3->setFont(font1);

		formLayout_2->setWidget(2, QFormLayout::LabelRole, label_3);

		label_4 = new QLabel(widget);
		label_4->setObjectName(QString::fromUtf8("label_4"));
		label_4->setFont(font1);

		formLayout_2->setWidget(3, QFormLayout::LabelRole, label_4);

		label_5 = new QLabel(widget);
		label_5->setObjectName(QString::fromUtf8("label_5"));
		label_5->setFont(font1);

		formLayout_2->setWidget(4, QFormLayout::LabelRole, label_5);

		label_6 = new QLabel(widget);
		label_6->setObjectName(QString::fromUtf8("label_6"));
		label_6->setFont(font1);

		formLayout_2->setWidget(5, QFormLayout::LabelRole, label_6);

		NumberOfStudents = new QLineEdit(widget);
		NumberOfStudents->setObjectName(QString::fromUtf8("NumberOfStudents"));
		NumberOfStudents->setReadOnly(true);

		formLayout_2->setWidget(0, QFormLayout::FieldRole, NumberOfStudents);

		NumberOfProjects = new QLineEdit(widget);
		NumberOfProjects->setObjectName(QString::fromUtf8("NumberOfProjects"));
        NumberOfProjects->setReadOnly(true);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, NumberOfProjects);

        NumberOfLectuerers = new QLineEdit(widget);
        NumberOfLectuerers->setObjectName(QString::fromUtf8("NumberOfLectuerers"));
        NumberOfLectuerers->setReadOnly(true);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, NumberOfLectuerers);

        AverageProjectsPerLecturer = new QLineEdit(widget);
        AverageProjectsPerLecturer->setObjectName(QString::fromUtf8("AverageProjectsPerLecturer"));
        AverageProjectsPerLecturer->setReadOnly(true);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, AverageProjectsPerLecturer);

        AverageProjectPopularity = new QLineEdit(widget);
        AverageProjectPopularity->setObjectName(QString::fromUtf8("AverageProjectPopularity"));
        AverageProjectPopularity->setReadOnly(true);

        formLayout_2->setWidget(4, QFormLayout::FieldRole, AverageProjectPopularity);

		NumberIgnoredProjects = new QLineEdit(widget);
		NumberIgnoredProjects->setObjectName(QString::fromUtf8("NumberIgnoredProjects"));
		NumberIgnoredProjects->setReadOnly(true);

		formLayout_2->setWidget(5, QFormLayout::FieldRole, NumberIgnoredProjects);


		horizontalLayout_3->addLayout(formLayout_2);

		verticalLayout_9 = new QVBoxLayout();
		verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
		verticalLayout_9->setContentsMargins(0, -1, -1, -1);
		label_9 = new QLabel(widget);
		label_9->setObjectName(QString::fromUtf8("label_9"));
		QFont font2;
		font2.setPointSize(11);
		font2.setBold(true);
		font2.setWeight(75);
		label_9->setFont(font2);
		label_9->setAlignment(Qt::AlignCenter);

		verticalLayout_9->addWidget(label_9);

		projectStatisticCombo = new QComboBox(widget);
		projectStatisticCombo->setObjectName(QString::fromUtf8("projectStatisticCombo"));

		verticalLayout_9->addWidget(projectStatisticCombo);

		horizontalLayout_4 = new QHBoxLayout();
		horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
		horizontalLayout_4->setContentsMargins(-1, 10, -1, -1);
		label_10 = new QLabel(widget);
		label_10->setObjectName(QString::fromUtf8("label_10"));
		label_10->setFont(font);

		horizontalLayout_4->addWidget(label_10);

		capacityDisplay = new QLineEdit(widget);
		capacityDisplay->setObjectName(QString::fromUtf8("capacityDisplay"));
		capacityDisplay->setReadOnly(true);

		horizontalLayout_4->addWidget(capacityDisplay);

		label_11 = new QLabel(widget);
		label_11->setObjectName(QString::fromUtf8("label_11"));
		label_11->setFont(font);

		horizontalLayout_4->addWidget(label_11);

		lecturerDisplay = new QLineEdit(widget);
		lecturerDisplay->setObjectName(QString::fromUtf8("lecturerDisplay"));
		lecturerDisplay->setReadOnly(true);

		horizontalLayout_4->addWidget(lecturerDisplay);


		verticalLayout_9->addLayout(horizontalLayout_4);

		horizontalLayout_6 = new QHBoxLayout();
		horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
		horizontalLayout_6->setContentsMargins(-1, 10, -1, -1);
		label_30 = new QLabel(widget);
		label_30->setObjectName(QString::fromUtf8("label_30"));
		label_30->setFont(font);

		horizontalLayout_6->addWidget(label_30);

		lineEdit = new QLineEdit(widget);
		lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

		horizontalLayout_6->addWidget(lineEdit);


		verticalLayout_9->addLayout(horizontalLayout_6);

		projectStatisticChart = new QChartView(widget);
		projectStatisticChart->setObjectName(QString::fromUtf8("projectStatisticChart"));
		sizePolicy3.setHeightForWidth(projectStatisticChart->sizePolicy().hasHeightForWidth());
		projectStatisticChart->setSizePolicy(sizePolicy3);

		verticalLayout_9->addWidget(projectStatisticChart);


		horizontalLayout_3->addLayout(verticalLayout_9);

		verticalLayout_8 = new QVBoxLayout();
		verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
		verticalLayout_8->setContentsMargins(0, -1, -1, -1);
		label_8 = new QLabel(widget);
		label_8->setObjectName(QString::fromUtf8("label_8"));
		label_8->setFont(font2);
		label_8->setAlignment(Qt::AlignCenter);

		verticalLayout_8->addWidget(label_8);

		studentStatisticCombo = new QComboBox(widget);
		studentStatisticCombo->addItem(QString());
		studentStatisticCombo->setObjectName(QString::fromUtf8("studentStatisticCombo"));

		verticalLayout_8->addWidget(studentStatisticCombo);

		studentStatsChart = new QChartView(widget);
		studentStatsChart->setObjectName(QString::fromUtf8("studentStatsChart"));
		sizePolicy3.setHeightForWidth(studentStatsChart->sizePolicy().hasHeightForWidth());
		studentStatsChart->setSizePolicy(sizePolicy3);

		verticalLayout_8->addWidget(studentStatsChart);


		horizontalLayout_3->addLayout(verticalLayout_8);


		verticalLayout_4->addWidget(widget);

		tabWidget->addTab(importedDtaStats, QString());
		outputDataStats = new QWidget();
		outputDataStats->setObjectName(QString::fromUtf8("outputDataStats"));
		horizontalLayout_5 = new QHBoxLayout(outputDataStats);
		horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
		verticalLayout_13 = new QVBoxLayout();
		verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
		verticalLayout_13->setContentsMargins(10, -1, -1, -1);
		allocationResultsChoser = new QComboBox(outputDataStats);
		allocationResultsChoser->setObjectName(QString::fromUtf8("allocationResultsChoser"));
		QSizePolicy sizePolicy6(QSizePolicy::Preferred, QSizePolicy::Fixed);
		sizePolicy6.setHorizontalStretch(1);
		sizePolicy6.setVerticalStretch(0);
		sizePolicy6.setHeightForWidth(allocationResultsChoser->sizePolicy().hasHeightForWidth());
		allocationResultsChoser->setSizePolicy(sizePolicy6);

		verticalLayout_13->addWidget(allocationResultsChoser);

		formLayout_7 = new QFormLayout();
		formLayout_7->setObjectName(QString::fromUtf8("formLayout_7"));
		label_14 = new QLabel(outputDataStats);
		label_14->setObjectName(QString::fromUtf8("label_14"));
		label_14->setFont(font);
		label_14->setWordWrap(true);

		formLayout_7->setWidget(2, QFormLayout::LabelRole, label_14);

		avgRank = new QLabel(outputDataStats);
		avgRank->setObjectName(QString::fromUtf8("avgRank"));

		formLayout_7->setWidget(2, QFormLayout::FieldRole, avgRank);

		label_16 = new QLabel(outputDataStats);
		label_16->setObjectName(QString::fromUtf8("label_16"));
		label_16->setFont(font);
		label_16->setWordWrap(true);

		formLayout_7->setWidget(4, QFormLayout::LabelRole, label_16);

		numberUnassignedStudents = new QLabel(outputDataStats);
		numberUnassignedStudents->setObjectName(QString::fromUtf8("numberUnassignedStudents"));

		formLayout_7->setWidget(4, QFormLayout::FieldRole, numberUnassignedStudents);

		label_18 = new QLabel(outputDataStats);
		label_18->setObjectName(QString::fromUtf8("label_18"));
		label_18->setFont(font);
		label_18->setWordWrap(true);

		formLayout_7->setWidget(5, QFormLayout::LabelRole, label_18);

		randomDistribute = new QLabel(outputDataStats);
		randomDistribute->setObjectName(QString::fromUtf8("randomDistribute"));

		formLayout_7->setWidget(5, QFormLayout::FieldRole, randomDistribute);

		label_20 = new QLabel(outputDataStats);
		label_20->setObjectName(QString::fromUtf8("label_20"));
		label_20->setFont(font);
		label_20->setWordWrap(true);

		formLayout_7->setWidget(6, QFormLayout::LabelRole, label_20);

		topicsDistribute = new QLabel(outputDataStats);
		topicsDistribute->setObjectName(QString::fromUtf8("topicsDistribute"));

		formLayout_7->setWidget(6, QFormLayout::FieldRole, topicsDistribute);

		label_22 = new QLabel(outputDataStats);
		label_22->setObjectName(QString::fromUtf8("label_22"));
		label_22->setFont(font);
		label_22->setWordWrap(true);

		formLayout_7->setWidget(8, QFormLayout::LabelRole, label_22);

		label_23 = new QLabel(outputDataStats);
		label_23->setObjectName(QString::fromUtf8("label_23"));
		label_23->setFont(font);
		label_23->setWordWrap(true);

		formLayout_7->setWidget(11, QFormLayout::LabelRole, label_23);

		label_24 = new QLabel(outputDataStats);
		label_24->setObjectName(QString::fromUtf8("label_24"));
		label_24->setFont(font);
		label_24->setWordWrap(true);

		formLayout_7->setWidget(10, QFormLayout::LabelRole, label_24);

		label_25 = new QLabel(outputDataStats);
		label_25->setObjectName(QString::fromUtf8("label_25"));
		label_25->setFont(font);
		label_25->setWordWrap(true);

		formLayout_7->setWidget(9, QFormLayout::LabelRole, label_25);

		numberFreeProjects = new QLabel(outputDataStats);
		numberFreeProjects->setObjectName(QString::fromUtf8("numberFreeProjects"));

		formLayout_7->setWidget(8, QFormLayout::FieldRole, numberFreeProjects);

		numberFreeLecturerSlots = new QLabel(outputDataStats);
		numberFreeLecturerSlots->setObjectName(QString::fromUtf8("numberFreeLecturerSlots"));

		formLayout_7->setWidget(9, QFormLayout::FieldRole, numberFreeLecturerSlots);

		numberOversubscribedLecturers = new QLabel(outputDataStats);
		numberOversubscribedLecturers->setObjectName(QString::fromUtf8("numberOversubscribedLecturers"));

		formLayout_7->setWidget(10, QFormLayout::FieldRole, numberOversubscribedLecturers);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		formLayout_7->setItem(3, QFormLayout::FieldRole, verticalSpacer);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		formLayout_7->setItem(7, QFormLayout::FieldRole, verticalSpacer_2);

		verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		formLayout_7->setItem(12, QFormLayout::LabelRole, verticalSpacer_3);

		numverOversubsrcibedProjects = new QLabel(outputDataStats);
		numverOversubsrcibedProjects->setObjectName(QString::fromUtf8("numverOversubsrcibedProjects"));

		formLayout_7->setWidget(11, QFormLayout::FieldRole, numverOversubsrcibedProjects);

		label_15 = new QLabel(outputDataStats);
		label_15->setObjectName(QString::fromUtf8("label_15"));
		label_15->setFont(font);

		formLayout_7->setWidget(0, QFormLayout::LabelRole, label_15);

		label_17 = new QLabel(outputDataStats);
		label_17->setObjectName(QString::fromUtf8("label_17"));
		label_17->setFont(font);

		formLayout_7->setWidget(1, QFormLayout::LabelRole, label_17);

		allocationAlgorithmName = new QLabel(outputDataStats);
		allocationAlgorithmName->setObjectName(QString::fromUtf8("allocationAlgorithmName"));

		formLayout_7->setWidget(0, QFormLayout::FieldRole, allocationAlgorithmName);

		allocationRunDatetime = new QLabel(outputDataStats);
		allocationRunDatetime->setObjectName(QString::fromUtf8("allocationRunDatetime"));

		formLayout_7->setWidget(1, QFormLayout::FieldRole, allocationRunDatetime);


		verticalLayout_13->addLayout(formLayout_7);

		verticalLayout_13->setStretch(0, 1);

		horizontalLayout_5->addLayout(verticalLayout_13);

		verticalLayout_11 = new QVBoxLayout();
		verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
		verticalLayout_11->setContentsMargins(0, -1, -1, -1);
		label_12 = new QLabel(outputDataStats);
		label_12->setObjectName(QString::fromUtf8("label_12"));
		label_12->setFont(font2);
		label_12->setAlignment(Qt::AlignCenter);

		verticalLayout_11->addWidget(label_12);

		freeProjectsList = new QListWidget(outputDataStats);
		freeProjectsList->setObjectName(QString::fromUtf8("freeProjectsList"));
		QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Expanding);
		sizePolicy7.setHorizontalStretch(1);
		sizePolicy7.setVerticalStretch(2);
		sizePolicy7.setHeightForWidth(freeProjectsList->sizePolicy().hasHeightForWidth());
		freeProjectsList->setSizePolicy(sizePolicy7);

		verticalLayout_11->addWidget(freeProjectsList);

		widget_3 = new QWidget(outputDataStats);
		widget_3->setObjectName(QString::fromUtf8("widget_3"));
		QSizePolicy sizePolicy8(QSizePolicy::Preferred, QSizePolicy::Preferred);
		sizePolicy8.setHorizontalStretch(0);
		sizePolicy8.setVerticalStretch(1);
		sizePolicy8.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
		widget_3->setSizePolicy(sizePolicy8);
		verticalLayout_16 = new QVBoxLayout(widget_3);
		verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
		formLayout_8 = new QFormLayout();
		formLayout_8->setObjectName(QString::fromUtf8("formLayout_8"));
		formLayout_8->setContentsMargins(-1, 10, -1, -1);
		label_28 = new QLabel(widget_3);
		label_28->setObjectName(QString::fromUtf8("label_28"));
		label_28->setFont(font);

		formLayout_8->setWidget(0, QFormLayout::LabelRole, label_28);

		freeProjectID = new QLabel(widget_3);
		freeProjectID->setObjectName(QString::fromUtf8("freeProjectID"));

		formLayout_8->setWidget(0, QFormLayout::FieldRole, freeProjectID);

		label_31 = new QLabel(widget_3);
		label_31->setObjectName(QString::fromUtf8("label_31"));
		label_31->setFont(font);

		formLayout_8->setWidget(1, QFormLayout::LabelRole, label_31);

		label_32 = new QLabel(widget_3);
		label_32->setObjectName(QString::fromUtf8("label_32"));
		label_32->setFont(font);
		label_32->setAlignment(Qt::AlignRight | Qt::AlignTrailing | Qt::AlignVCenter);

		formLayout_8->setWidget(3, QFormLayout::LabelRole, label_32);

		label_33 = new QLabel(widget_3);
		label_33->setObjectName(QString::fromUtf8("label_33"));
		label_33->setFont(font);

		formLayout_8->setWidget(5, QFormLayout::LabelRole, label_33);

		label_34 = new QLabel(widget_3);
		label_34->setObjectName(QString::fromUtf8("label_34"));
		label_34->setFont(font);
		label_34->setAlignment(Qt::AlignRight | Qt::AlignTrailing | Qt::AlignVCenter);

		formLayout_8->setWidget(4, QFormLayout::LabelRole, label_34);

		label_35 = new QLabel(widget_3);
		label_35->setObjectName(QString::fromUtf8("label_35"));
		label_35->setFont(font);

		formLayout_8->setWidget(6, QFormLayout::LabelRole, label_35);

		freeProjectTitle = new QLabel(widget_3);
		freeProjectTitle->setObjectName(QString::fromUtf8("freeProjectTitle"));

		formLayout_8->setWidget(1, QFormLayout::FieldRole, freeProjectTitle);

		freeProjectRemainingCapacity = new QLabel(widget_3);
		freeProjectRemainingCapacity->setObjectName(QString::fromUtf8("freeProjectRemainingCapacity"));

		formLayout_8->setWidget(3, QFormLayout::FieldRole, freeProjectRemainingCapacity);

		freeProjectRemainingLecturerCapacity = new QLabel(widget_3);
		freeProjectRemainingLecturerCapacity->setObjectName(QString::fromUtf8("freeProjectRemainingLecturerCapacity"));

		formLayout_8->setWidget(4, QFormLayout::FieldRole, freeProjectRemainingLecturerCapacity);

		freeProjectTopics = new QTextBrowser(widget_3);
		freeProjectTopics->setObjectName(QString::fromUtf8("freeProjectTopics"));
		QSizePolicy sizePolicy9(QSizePolicy::Expanding, QSizePolicy::Maximum);
		sizePolicy9.setHorizontalStretch(0);
		sizePolicy9.setVerticalStretch(0);
		sizePolicy9.setHeightForWidth(freeProjectTopics->sizePolicy().hasHeightForWidth());
		freeProjectTopics->setSizePolicy(sizePolicy9);
		freeProjectTopics->setMaximumSize(QSize(16777215, 100));

		formLayout_8->setWidget(5, QFormLayout::FieldRole, freeProjectTopics);

		freeProjectCourses = new QTextBrowser(widget_3);
		freeProjectCourses->setObjectName(QString::fromUtf8("freeProjectCourses"));
		sizePolicy9.setHeightForWidth(freeProjectCourses->sizePolicy().hasHeightForWidth());
		freeProjectCourses->setSizePolicy(sizePolicy9);
		freeProjectCourses->setMaximumSize(QSize(16777215, 100));

		formLayout_8->setWidget(6, QFormLayout::FieldRole, freeProjectCourses);

		label_29 = new QLabel(widget_3);
		label_29->setObjectName(QString::fromUtf8("label_29"));
		label_29->setFont(font);

		formLayout_8->setWidget(2, QFormLayout::LabelRole, label_29);

		freeProjectLecturer = new QLabel(widget_3);
		freeProjectLecturer->setObjectName(QString::fromUtf8("freeProjectLecturer"));

		formLayout_8->setWidget(2, QFormLayout::FieldRole, freeProjectLecturer);


		verticalLayout_16->addLayout(formLayout_8);


		verticalLayout_11->addWidget(widget_3);


		horizontalLayout_5->addLayout(verticalLayout_11);

		verticalLayout_12 = new QVBoxLayout();
		verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
		verticalLayout_12->setContentsMargins(0, -1, -1, -1);
		verticalLayout_14 = new QVBoxLayout();
		verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
		verticalLayout_14->setContentsMargins(-1, 0, -1, -1);
		label_13 = new QLabel(outputDataStats);
		label_13->setObjectName(QString::fromUtf8("label_13"));
		label_13->setFont(font2);
		label_13->setAlignment(Qt::AlignCenter);

		verticalLayout_14->addWidget(label_13);

		unassignedStudentsList = new QListWidget(outputDataStats);
		unassignedStudentsList->setObjectName(QString::fromUtf8("unassignedStudentsList"));
		sizePolicy7.setHeightForWidth(unassignedStudentsList->sizePolicy().hasHeightForWidth());
		unassignedStudentsList->setSizePolicy(sizePolicy7);

		verticalLayout_14->addWidget(unassignedStudentsList);

		widget_2 = new QWidget(outputDataStats);
		widget_2->setObjectName(QString::fromUtf8("widget_2"));
		sizePolicy8.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
		widget_2->setSizePolicy(sizePolicy8);
		verticalLayout_15 = new QVBoxLayout(widget_2);
		verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
		formLayout_6 = new QFormLayout();
		formLayout_6->setObjectName(QString::fromUtf8("formLayout_6"));
		formLayout_6->setSizeConstraint(QLayout::SetMinimumSize);
		formLayout_6->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
		formLayout_6->setRowWrapPolicy(QFormLayout::WrapLongRows);
		label_19 = new QLabel(widget_2);
		label_19->setObjectName(QString::fromUtf8("label_19"));
		label_19->setFont(font);

		formLayout_6->setWidget(0, QFormLayout::LabelRole, label_19);

		label_21 = new QLabel(widget_2);
		label_21->setObjectName(QString::fromUtf8("label_21"));
		label_21->setFont(font);
		label_21->setAlignment(Qt::AlignRight | Qt::AlignTrailing | Qt::AlignVCenter);
		label_21->setWordWrap(true);

		formLayout_6->setWidget(3, QFormLayout::LabelRole, label_21);

		label_26 = new QLabel(widget_2);
		label_26->setObjectName(QString::fromUtf8("label_26"));
		label_26->setFont(font);
		label_26->setAlignment(Qt::AlignRight | Qt::AlignTrailing | Qt::AlignVCenter);
		label_26->setWordWrap(true);

		formLayout_6->setWidget(2, QFormLayout::LabelRole, label_26);

		label_27 = new QLabel(widget_2);
		label_27->setObjectName(QString::fromUtf8("label_27"));
		label_27->setFont(font);

		formLayout_6->setWidget(1, QFormLayout::LabelRole, label_27);

		unassignedStudentID = new QLabel(widget_2);
		unassignedStudentID->setObjectName(QString::fromUtf8("unassignedStudentID"));

		formLayout_6->setWidget(0, QFormLayout::FieldRole, unassignedStudentID);

		unassignedStudentCourse = new QLabel(widget_2);
		unassignedStudentCourse->setObjectName(QString::fromUtf8("unassignedStudentCourse"));

		formLayout_6->setWidget(1, QFormLayout::FieldRole, unassignedStudentCourse);

		unassignedStudentTopicPreferences = new QTextBrowser(widget_2);
		unassignedStudentTopicPreferences->setObjectName(QString::fromUtf8("unassignedStudentTopicPreferences"));
		unassignedStudentTopicPreferences->setEnabled(true);
		QSizePolicy sizePolicy10(QSizePolicy::Minimum, QSizePolicy::Minimum);
		sizePolicy10.setHorizontalStretch(0);
		sizePolicy10.setVerticalStretch(0);
		sizePolicy10.setHeightForWidth(unassignedStudentTopicPreferences->sizePolicy().hasHeightForWidth());
		unassignedStudentTopicPreferences->setSizePolicy(sizePolicy10);
		unassignedStudentTopicPreferences->setMaximumSize(QSize(16777215, 100));

		formLayout_6->setWidget(2, QFormLayout::FieldRole, unassignedStudentTopicPreferences);

		unassignedStudentProjectPreferences = new QTextBrowser(widget_2);
		unassignedStudentProjectPreferences->setObjectName(QString::fromUtf8("unassignedStudentProjectPreferences"));
		QSizePolicy sizePolicy11(QSizePolicy::Expanding, QSizePolicy::Minimum);
		sizePolicy11.setHorizontalStretch(0);
		sizePolicy11.setVerticalStretch(0);
		sizePolicy11.setHeightForWidth(unassignedStudentProjectPreferences->sizePolicy().hasHeightForWidth());
		unassignedStudentProjectPreferences->setSizePolicy(sizePolicy11);
		unassignedStudentProjectPreferences->setMaximumSize(QSize(16777215, 100));

		formLayout_6->setWidget(3, QFormLayout::FieldRole, unassignedStudentProjectPreferences);


		verticalLayout_15->addLayout(formLayout_6);


		verticalLayout_14->addWidget(widget_2);


		verticalLayout_12->addLayout(verticalLayout_14);


		horizontalLayout_5->addLayout(verticalLayout_12);

		tabWidget->addTab(outputDataStats, QString());
		outputGraphs = new QWidget();
		outputGraphs->setObjectName(QString::fromUtf8("outputGraphs"));
		horizontalLayout_7 = new QHBoxLayout(outputGraphs);
		horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
		outputStats = new QChartView(outputGraphs);
		outputStats->setObjectName(QString::fromUtf8("outputStats"));
		QSizePolicy sizePolicy12(QSizePolicy::Expanding, QSizePolicy::Expanding);
		sizePolicy12.setHorizontalStretch(3);
		sizePolicy12.setVerticalStretch(0);
		sizePolicy12.setHeightForWidth(outputStats->sizePolicy().hasHeightForWidth());
		outputStats->setSizePolicy(sizePolicy12);

		horizontalLayout_7->addWidget(outputStats);

		outputStatsCum = new QChartView(outputGraphs);
		outputStatsCum->setObjectName(QString::fromUtf8("outputStatsCum"));
		sizePolicy12.setHeightForWidth(outputStatsCum->sizePolicy().hasHeightForWidth());
		outputStatsCum->setSizePolicy(sizePolicy12);

		horizontalLayout_7->addWidget(outputStatsCum);

		tabWidget->addTab(outputGraphs, QString());

		verticalLayout_3->addWidget(tabWidget);

		splitter->addWidget(StatsDisplay);
		outputBox = new QTextEdit(splitter);
		outputBox->setObjectName(QString::fromUtf8("outputBox"));
		QSizePolicy sizePolicy13(QSizePolicy::Preferred, QSizePolicy::Preferred);
		sizePolicy13.setHorizontalStretch(0);
		sizePolicy13.setVerticalStretch(0);
		sizePolicy13.setHeightForWidth(outputBox->sizePolicy().hasHeightForWidth());
		outputBox->setSizePolicy(sizePolicy13);
		outputBox->setMinimumSize(QSize(0, 200));
		outputBox->setReadOnly(true);
		splitter->addWidget(outputBox);

		verticalLayout->addWidget(splitter);


		horizontalLayout->addWidget(main);

		MainWindow->setCentralWidget(centralwidget);
		menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1600, 30));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuSave = new QMenu(menuFile);
        menuSave->setObjectName(QString::fromUtf8("menuSave"));
        menuOpen = new QMenu(menuFile);
        menuOpen->setObjectName(QString::fromUtf8("menuOpen"));
        menuData = new QMenu(menubar);
        menuData->setObjectName(QString::fromUtf8("menuData"));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuData->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionImport_Data);
        menuFile->addAction(menuOpen->menuAction());
        menuFile->addAction(menuSave->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
		menuSave->addAction(actionResults);
		menuSave->addAction(actionSession);
		menuOpen->addAction(actionSession_2);
		menuData->addAction(actionEdit);
		menuData->addAction(actionView_All);
		menuHelp->addAction(actionView_Help);
		menuHelp->addAction(actionAbout);

		retranslateUi(MainWindow);

		algoSpecificSettings->setCurrentIndex(3);
		tabWidget->setCurrentIndex(2);


		QMetaObject::connectSlotsByName(MainWindow);
	}// setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "SPrAl", nullptr));
        actionImport_Data->setText(QCoreApplication::translate("MainWindow", "Import Data", nullptr));
        actionResults->setText(QCoreApplication::translate("MainWindow", "Results", nullptr));
        actionSession->setText(QCoreApplication::translate("MainWindow", "Session", nullptr));
        actionSession_2->setText(QCoreApplication::translate("MainWindow", "Session", nullptr));
        actionExit->setText(QCoreApplication::translate("MainWindow", "Exit", nullptr));
        actionView_Help->setText(QCoreApplication::translate("MainWindow", "View Help", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About", nullptr));
        actionEdit->setText(QCoreApplication::translate("MainWindow", "Edit Imported", nullptr));
        actionView_All->setText(QCoreApplication::translate("MainWindow", "View All", nullptr));
        logFileNameLabel->setText(QCoreApplication::translate("MainWindow", "Logfile Location", nullptr));
        filerFinder->setText(QCoreApplication::translate("MainWindow", "...", nullptr));
        loggingTFLabel->setText(QCoreApplication::translate("MainWindow", "Log to file?", nullptr));
		logToFile->setText(QString());
		loggingLevelLabel->setText(QCoreApplication::translate("MainWindow", "logging level", nullptr));
		label_7->setText(QCoreApplication::translate("MainWindow", "Allocation Algorithm", nullptr));
		runButton->setText(QCoreApplication::translate("MainWindow", "Run Allocations", nullptr));
		label->setText(QCoreApplication::translate("MainWindow", "Num. Students", nullptr));
		label_2->setText(QCoreApplication::translate("MainWindow", "Num. Projects", nullptr));
		label_3->setText(QCoreApplication::translate("MainWindow", "Num. Lecturers", nullptr));
		label_4->setText(QCoreApplication::translate("MainWindow", "Avg. Proj/Lect", nullptr));
		label_5->setText(QCoreApplication::translate("MainWindow", "Avg Proj Pop.", nullptr));
		label_6->setText(QCoreApplication::translate("MainWindow", "Num. Ignored Projects.", nullptr));
		label_9->setText(QCoreApplication::translate("MainWindow", "Project Info", nullptr));
		projectStatisticCombo->setPlaceholderText(QCoreApplication::translate("MainWindow", "Select a project...", nullptr));
		label_10->setText(QCoreApplication::translate("MainWindow", "Capacity", nullptr));
		label_11->setText(QCoreApplication::translate("MainWindow", "Lecturer", nullptr));
		label_30->setText(QCoreApplication::translate("MainWindow", "Total Ranks", nullptr));
		label_8->setText(QCoreApplication::translate("MainWindow", "Student Statistics", nullptr));
		studentStatisticCombo->setItemText(0, QCoreApplication::translate("MainWindow", "Course Distribution", nullptr));

		studentStatisticCombo->setPlaceholderText(QCoreApplication::translate("MainWindow", "Select a metric...", nullptr));
		tabWidget->setTabText(tabWidget->indexOf(importedDtaStats), QCoreApplication::translate("MainWindow", "Imported Data Stats", nullptr));
		allocationResultsChoser->setPlaceholderText(QCoreApplication::translate("MainWindow", "Choose a set of allocation results...", nullptr));
		label_14->setText(QCoreApplication::translate("MainWindow", "Avg Rank", nullptr));
		avgRank->setText(QString());
		label_16->setText(QCoreApplication::translate("MainWindow", "No. Unassigned", nullptr));
		numberUnassignedStudents->setText(QString());
		label_18->setText(QCoreApplication::translate("MainWindow", "Random Dist.", nullptr));
		randomDistribute->setText(QString());
		label_20->setText(QCoreApplication::translate("MainWindow", "Topic Dist.", nullptr));
		topicsDistribute->setText(QString());
		label_22->setText(QCoreApplication::translate("MainWindow", "No. Free Projects", nullptr));
		label_23->setText(QCoreApplication::translate("MainWindow", "Over Subscr. Projects", nullptr));
		label_24->setText(QCoreApplication::translate("MainWindow", "Over Subscr. Lects", nullptr));
		label_25->setText(QCoreApplication::translate("MainWindow", "Free Lecturer Spots", nullptr));
		numberFreeProjects->setText(QString());
		numberFreeLecturerSlots->setText(QString());
		numberOversubscribedLecturers->setText(QString());
		numverOversubsrcibedProjects->setText(QString());
		label_15->setText(QCoreApplication::translate("MainWindow", "Allocation Algorithm", nullptr));
		label_17->setText(QCoreApplication::translate("MainWindow", "Run datetime", nullptr));
		allocationAlgorithmName->setText(QString());
		allocationRunDatetime->setText(QString());
		label_12->setText(QCoreApplication::translate("MainWindow", "Free Projects", nullptr));
		label_28->setText(QCoreApplication::translate("MainWindow", "ID", nullptr));
		freeProjectID->setText(QString());
		label_31->setText(QCoreApplication::translate("MainWindow", "Title", nullptr));
		label_32->setText(QCoreApplication::translate("MainWindow",
		  "Remaining\n"
		  " Capacity",
		  nullptr));
		label_33->setText(QCoreApplication::translate("MainWindow", "Topics", nullptr));
		label_34->setText(QCoreApplication::translate("MainWindow",
		  "Remaining\n"
		  " Lecturer\n"
		  " Capacity",
		  nullptr));
		label_35->setText(QCoreApplication::translate("MainWindow", "Courses", nullptr));
		freeProjectTitle->setText(QString());
		freeProjectRemainingCapacity->setText(QString());
		freeProjectRemainingLecturerCapacity->setText(QString());
		label_29->setText(QCoreApplication::translate("MainWindow", "Lecturer", nullptr));
		freeProjectLecturer->setText(QString());
		label_13->setText(QCoreApplication::translate("MainWindow", "Unassignable Students", nullptr));
		label_19->setText(QCoreApplication::translate("MainWindow", "ID", nullptr));
		label_21->setText(QCoreApplication::translate("MainWindow",
		  "Project \n"
		  " Preferences",
		  nullptr));
		label_26->setText(QCoreApplication::translate("MainWindow",
		  "Topic\n"
		  " Preferences",
		  nullptr));
		label_27->setText(QCoreApplication::translate("MainWindow", "Course", nullptr));
		unassignedStudentID->setText(QString());
		unassignedStudentCourse->setText(QString());
		tabWidget->setTabText(tabWidget->indexOf(outputDataStats), QCoreApplication::translate("MainWindow", "Output Stats", nullptr));
		tabWidget->setTabText(tabWidget->indexOf(outputGraphs), QCoreApplication::translate("MainWindow", "Output Graphs", nullptr));
		menuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
		menuSave->setTitle(QCoreApplication::translate("MainWindow", "Save", nullptr));
		menuOpen->setTitle(QCoreApplication::translate("MainWindow", "Open", nullptr));
		menuData->setTitle(QCoreApplication::translate("MainWindow", "Data", nullptr));
		menuHelp->setTitle(QCoreApplication::translate("MainWindow", "Help", nullptr));
	}// retranslateUi
};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif// MAINWINDOWCDLMCW_H
