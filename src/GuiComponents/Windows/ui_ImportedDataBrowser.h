/********************************************************************************
** Form generated from reading UI file 'ImportedDataBrowservZLHmv.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef IMPORTEDDATABROWSERVZLHMV_H
#define IMPORTEDDATABROWSERVZLHMV_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DataBrowser
{
public:
  QVBoxLayout *verticalLayout;
  QWidget *widget;
  QHBoxLayout *horizontalLayout;
  QVBoxLayout *verticalLayout_2;
  QSpacerItem *verticalSpacer;
  QWidget *infoWidget;
  QVBoxLayout *verticalLayout_5;
  QVBoxLayout *verticalLayout_4;
  QFormLayout *DataSelectionLayout;
  QComboBox *DataTypeCombo;
  QLabel *DataTypeLabel;
  QStackedWidget *stackedWidget;
  QWidget *blank;
  QVBoxLayout *verticalLayout_7;
  QVBoxLayout *verticalLayout_6;
  QFormLayout *formLayout;
  QSpacerItem *horizontalSpacer;
  QSpacerItem *verticalSpacer_3;
  QTextEdit *blankPreview;
  QWidget *project;
  QVBoxLayout *verticalLayout_9;
  QVBoxLayout *verticalLayout_8;
  QFormLayout *formLayout_2;
  QSpacerItem *horizontalSpacer_2;
  QSpacerItem *verticalSpacer_4;
  QLabel *label_2;
  QLabel *IDLabel;
  QComboBox *IDColumnCombo;
  QLabel *TitleLabel;
  QComboBox *TitleCombo;
  QLabel *CapacityLabel_2;
  QComboBox *CapacityCombo_2;
  QLabel *LastPrefLabel;
  QComboBox *LecturerCombo;
  QLabel *TopicsLabel;
  QComboBox *TopicsCombo;
  QLabel *SuitableCoursesLabel;
  QComboBox *SuitableCoursesCombo;
  QSpacerItem *verticalSpacer_5;
  QFrame *line;
  QFrame *line_2;
  QFrame *line_7;
  QFrame *line_8;
  QTextEdit *projectPreview;
  QWidget *lecturer;
  QVBoxLayout *verticalLayout_11;
  QVBoxLayout *verticalLayout_10;
  QFormLayout *formLayout_3;
  QSpacerItem *horizontalSpacer_3;
  QSpacerItem *verticalSpacer_6;
  QLabel *label_3;
  QLabel *IDLabel_2;
  QComboBox *IDColumnCombo_2;
  QLabel *CapacityLabel;
  QComboBox *CapacityCombo;
  QFrame *line_3;
  QLabel *nameLabel;
  QComboBox *NameCombo;
  QLabel *TopicsLabel_2;
  QComboBox *TopicsCombo_2;
  QLabel *SuitableCoursesLabel_2;
  QComboBox *Courses_Combo;
  QSpacerItem *verticalSpacer_7;
  QTextEdit *lecturerPreview;
  QWidget *course;
  QVBoxLayout *verticalLayout_13;
  QVBoxLayout *verticalLayout_12;
  QFormLayout *formLayout_4;
  QSpacerItem *horizontalSpacer_4;
  QSpacerItem *verticalSpacer_8;
  QLabel *label_4;
  QLabel *IDLabel_3;
  QComboBox *IDColumnCombo_3;
  QLabel *PriorityLabel;
  QComboBox *PriorityCombo;
  QSpacerItem *verticalSpacer_11;
  QTextEdit *coursePreview;
  QWidget *student;
  QVBoxLayout *verticalLayout_15;
  QVBoxLayout *verticalLayout_14;
  QFormLayout *formLayout_5;
  QSpacerItem *verticalSpacer_9;
  QLabel *label_5;
  QLabel *IDLabel_4;
  QComboBox *IDColumnCombo_4;
  QLabel *FirstPrefLabel;
  QComboBox *PrefsStartCombo;
  QLabel *LastPrefLabel_2;
  QComboBox *PrefsEndCombo;
  QFrame *line_5;
  QLabel *FirstTopicPrefLabe;
  QComboBox *FirstTopicPrefCombo;
  QLabel *LastTopicPrefLabel;
  QComboBox *LastTopicPrefCombo_3;
  QFrame *line_6;
  QLabel *label;
  QComboBox *CourseCombo;
  QSpacerItem *verticalSpacer_10;
  QSpacerItem *horizontalSpacer_5;
  QTextEdit *studentPreview;
  QSpacerItem *verticalSpacer_2;
  QVBoxLayout *verticalLayout_3;
  QTableWidget *tableWidget;
  QDialogButtonBox *buttonBox;

  void setupUi(QDialog *DataBrowser)
  {
	if (DataBrowser->objectName().isEmpty())
	  DataBrowser->setObjectName(QString::fromUtf8("DataBrowser"));
	DataBrowser->resize(1106, 807);
	QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	sizePolicy.setHeightForWidth(DataBrowser->sizePolicy().hasHeightForWidth());
	DataBrowser->setSizePolicy(sizePolicy);
	verticalLayout = new QVBoxLayout(DataBrowser);
	verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
	widget = new QWidget(DataBrowser);
	widget->setObjectName(QString::fromUtf8("widget"));
	QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
	sizePolicy1.setHorizontalStretch(0);
	sizePolicy1.setVerticalStretch(0);
	sizePolicy1.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
	widget->setSizePolicy(sizePolicy1);
	horizontalLayout = new QHBoxLayout(widget);
	horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
	verticalLayout_2 = new QVBoxLayout();
	verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
	verticalLayout_2->setContentsMargins(0, -1, -1, -1);
	verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	verticalLayout_2->addItem(verticalSpacer);

	infoWidget = new QWidget(widget);
	infoWidget->setObjectName(QString::fromUtf8("infoWidget"));
	sizePolicy.setHeightForWidth(infoWidget->sizePolicy().hasHeightForWidth());
	infoWidget->setSizePolicy(sizePolicy);
	verticalLayout_5 = new QVBoxLayout(infoWidget);
	verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
	verticalLayout_4 = new QVBoxLayout();
	verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
	verticalLayout_4->setContentsMargins(-1, 20, -1, -1);
	DataSelectionLayout = new QFormLayout();
	DataSelectionLayout->setObjectName(QString::fromUtf8("DataSelectionLayout"));
	DataSelectionLayout->setContentsMargins(-1, 10, -1, -1);
	DataTypeCombo = new QComboBox(infoWidget);
	DataTypeCombo->setObjectName(QString::fromUtf8("DataTypeCombo"));
	QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
	sizePolicy2.setHorizontalStretch(0);
	sizePolicy2.setVerticalStretch(0);
	sizePolicy2.setHeightForWidth(DataTypeCombo->sizePolicy().hasHeightForWidth());
	DataTypeCombo->setSizePolicy(sizePolicy2);

	DataSelectionLayout->setWidget(0, QFormLayout::FieldRole, DataTypeCombo);

	DataTypeLabel = new QLabel(infoWidget);
	DataTypeLabel->setObjectName(QString::fromUtf8("DataTypeLabel"));

	DataSelectionLayout->setWidget(0, QFormLayout::LabelRole, DataTypeLabel);


	verticalLayout_4->addLayout(DataSelectionLayout);

	stackedWidget = new QStackedWidget(infoWidget);
	stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
	blank = new QWidget();
	blank->setObjectName(QString::fromUtf8("blank"));
	verticalLayout_7 = new QVBoxLayout(blank);
	verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
	verticalLayout_6 = new QVBoxLayout();
	verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
	verticalLayout_6->setContentsMargins(-1, 0, -1, -1);
	formLayout = new QFormLayout();
	formLayout->setObjectName(QString::fromUtf8("formLayout"));
	formLayout->setContentsMargins(-1, -1, 10, -1);
	horizontalSpacer = new QSpacerItem(150, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	formLayout->setItem(0, QFormLayout::LabelRole, horizontalSpacer);

	verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout->setItem(0, QFormLayout::FieldRole, verticalSpacer_3);


	verticalLayout_6->addLayout(formLayout);

	blankPreview = new QTextEdit(blank);
	blankPreview->setObjectName(QString::fromUtf8("blankPreview"));

	verticalLayout_6->addWidget(blankPreview);


	verticalLayout_7->addLayout(verticalLayout_6);

	stackedWidget->addWidget(blank);
	project = new QWidget();
	project->setObjectName(QString::fromUtf8("project"));
	verticalLayout_9 = new QVBoxLayout(project);
	verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
	verticalLayout_8 = new QVBoxLayout();
	verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
	verticalLayout_8->setContentsMargins(-1, 0, -1, -1);
	formLayout_2 = new QFormLayout();
	formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
	formLayout_2->setContentsMargins(-1, -1, 10, -1);
	horizontalSpacer_2 = new QSpacerItem(150, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	formLayout_2->setItem(0, QFormLayout::LabelRole, horizontalSpacer_2);

	verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_2->setItem(0, QFormLayout::FieldRole, verticalSpacer_4);

	label_2 = new QLabel(project);
	label_2->setObjectName(QString::fromUtf8("label_2"));

	formLayout_2->setWidget(1, QFormLayout::FieldRole, label_2);

	IDLabel = new QLabel(project);
	IDLabel->setObjectName(QString::fromUtf8("IDLabel"));

	formLayout_2->setWidget(2, QFormLayout::LabelRole, IDLabel);

	IDColumnCombo = new QComboBox(project);
	IDColumnCombo->setObjectName(QString::fromUtf8("IDColumnCombo"));
	sizePolicy2.setHeightForWidth(IDColumnCombo->sizePolicy().hasHeightForWidth());
	IDColumnCombo->setSizePolicy(sizePolicy2);

	formLayout_2->setWidget(2, QFormLayout::FieldRole, IDColumnCombo);

	TitleLabel = new QLabel(project);
	TitleLabel->setObjectName(QString::fromUtf8("TitleLabel"));

	formLayout_2->setWidget(4, QFormLayout::LabelRole, TitleLabel);

	TitleCombo = new QComboBox(project);
	TitleCombo->setObjectName(QString::fromUtf8("TitleCombo"));
	sizePolicy2.setHeightForWidth(TitleCombo->sizePolicy().hasHeightForWidth());
	TitleCombo->setSizePolicy(sizePolicy2);

	formLayout_2->setWidget(4, QFormLayout::FieldRole, TitleCombo);

	CapacityLabel_2 = new QLabel(project);
	CapacityLabel_2->setObjectName(QString::fromUtf8("CapacityLabel_2"));

	formLayout_2->setWidget(6, QFormLayout::LabelRole, CapacityLabel_2);

	CapacityCombo_2 = new QComboBox(project);
	CapacityCombo_2->setObjectName(QString::fromUtf8("CapacityCombo_2"));
	sizePolicy2.setHeightForWidth(CapacityCombo_2->sizePolicy().hasHeightForWidth());
	CapacityCombo_2->setSizePolicy(sizePolicy2);

	formLayout_2->setWidget(6, QFormLayout::FieldRole, CapacityCombo_2);

	LastPrefLabel = new QLabel(project);
	LastPrefLabel->setObjectName(QString::fromUtf8("LastPrefLabel"));

	formLayout_2->setWidget(7, QFormLayout::LabelRole, LastPrefLabel);

	LecturerCombo = new QComboBox(project);
	LecturerCombo->setObjectName(QString::fromUtf8("LecturerCombo"));
	sizePolicy2.setHeightForWidth(LecturerCombo->sizePolicy().hasHeightForWidth());
	LecturerCombo->setSizePolicy(sizePolicy2);

	formLayout_2->setWidget(7, QFormLayout::FieldRole, LecturerCombo);

	TopicsLabel = new QLabel(project);
	TopicsLabel->setObjectName(QString::fromUtf8("TopicsLabel"));
	QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicy3.setHorizontalStretch(0);
	sizePolicy3.setVerticalStretch(0);
	sizePolicy3.setHeightForWidth(TopicsLabel->sizePolicy().hasHeightForWidth());
	TopicsLabel->setSizePolicy(sizePolicy3);
	TopicsLabel->setScaledContents(false);
	TopicsLabel->setWordWrap(true);

	formLayout_2->setWidget(9, QFormLayout::LabelRole, TopicsLabel);

	TopicsCombo = new QComboBox(project);
	TopicsCombo->setObjectName(QString::fromUtf8("TopicsCombo"));
	sizePolicy2.setHeightForWidth(TopicsCombo->sizePolicy().hasHeightForWidth());
	TopicsCombo->setSizePolicy(sizePolicy2);

	formLayout_2->setWidget(9, QFormLayout::FieldRole, TopicsCombo);

	SuitableCoursesLabel = new QLabel(project);
	SuitableCoursesLabel->setObjectName(QString::fromUtf8("SuitableCoursesLabel"));
	sizePolicy3.setHeightForWidth(SuitableCoursesLabel->sizePolicy().hasHeightForWidth());
	SuitableCoursesLabel->setSizePolicy(sizePolicy3);
	SuitableCoursesLabel->setScaledContents(false);
	SuitableCoursesLabel->setWordWrap(true);

	formLayout_2->setWidget(11, QFormLayout::LabelRole, SuitableCoursesLabel);

	SuitableCoursesCombo = new QComboBox(project);
	SuitableCoursesCombo->setObjectName(QString::fromUtf8("SuitableCoursesCombo"));
	sizePolicy2.setHeightForWidth(SuitableCoursesCombo->sizePolicy().hasHeightForWidth());
	SuitableCoursesCombo->setSizePolicy(sizePolicy2);

	formLayout_2->setWidget(11, QFormLayout::FieldRole, SuitableCoursesCombo);

	verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_2->setItem(13, QFormLayout::FieldRole, verticalSpacer_5);

	line = new QFrame(project);
	line->setObjectName(QString::fromUtf8("line"));
	sizePolicy2.setHeightForWidth(line->sizePolicy().hasHeightForWidth());
	line->setSizePolicy(sizePolicy2);
	line->setLineWidth(1);
	line->setMidLineWidth(1);
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);

	formLayout_2->setWidget(10, QFormLayout::FieldRole, line);

	line_2 = new QFrame(project);
	line_2->setObjectName(QString::fromUtf8("line_2"));
	sizePolicy2.setHeightForWidth(line_2->sizePolicy().hasHeightForWidth());
	line_2->setSizePolicy(sizePolicy2);
	line_2->setFrameShape(QFrame::HLine);
	line_2->setFrameShadow(QFrame::Sunken);

	formLayout_2->setWidget(8, QFormLayout::LabelRole, line_2);

	line_7 = new QFrame(project);
	line_7->setObjectName(QString::fromUtf8("line_7"));
	sizePolicy2.setHeightForWidth(line_7->sizePolicy().hasHeightForWidth());
	line_7->setSizePolicy(sizePolicy2);
	line_7->setFrameShape(QFrame::HLine);
	line_7->setFrameShadow(QFrame::Sunken);

	formLayout_2->setWidget(3, QFormLayout::FieldRole, line_7);

	line_8 = new QFrame(project);
	line_8->setObjectName(QString::fromUtf8("line_8"));
	sizePolicy2.setHeightForWidth(line_8->sizePolicy().hasHeightForWidth());
	line_8->setSizePolicy(sizePolicy2);
	line_8->setFrameShape(QFrame::HLine);
	line_8->setFrameShadow(QFrame::Sunken);

	formLayout_2->setWidget(5, QFormLayout::FieldRole, line_8);


	verticalLayout_8->addLayout(formLayout_2);

	projectPreview = new QTextEdit(project);
	projectPreview->setObjectName(QString::fromUtf8("projectPreview"));

	verticalLayout_8->addWidget(projectPreview);


	verticalLayout_9->addLayout(verticalLayout_8);

	stackedWidget->addWidget(project);
	lecturer = new QWidget();
	lecturer->setObjectName(QString::fromUtf8("lecturer"));
	verticalLayout_11 = new QVBoxLayout(lecturer);
	verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
	verticalLayout_10 = new QVBoxLayout();
	verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
	verticalLayout_10->setContentsMargins(-1, 0, -1, -1);
	formLayout_3 = new QFormLayout();
	formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
	formLayout_3->setContentsMargins(-1, -1, 10, -1);
	horizontalSpacer_3 = new QSpacerItem(150, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	formLayout_3->setItem(0, QFormLayout::LabelRole, horizontalSpacer_3);

	verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_3->setItem(0, QFormLayout::FieldRole, verticalSpacer_6);

	label_3 = new QLabel(lecturer);
	label_3->setObjectName(QString::fromUtf8("label_3"));

	formLayout_3->setWidget(1, QFormLayout::FieldRole, label_3);

	IDLabel_2 = new QLabel(lecturer);
	IDLabel_2->setObjectName(QString::fromUtf8("IDLabel_2"));

	formLayout_3->setWidget(2, QFormLayout::LabelRole, IDLabel_2);

	IDColumnCombo_2 = new QComboBox(lecturer);
	IDColumnCombo_2->setObjectName(QString::fromUtf8("IDColumnCombo_2"));
	sizePolicy2.setHeightForWidth(IDColumnCombo_2->sizePolicy().hasHeightForWidth());
	IDColumnCombo_2->setSizePolicy(sizePolicy2);

	formLayout_3->setWidget(2, QFormLayout::FieldRole, IDColumnCombo_2);

	CapacityLabel = new QLabel(lecturer);
	CapacityLabel->setObjectName(QString::fromUtf8("CapacityLabel"));

	formLayout_3->setWidget(4, QFormLayout::LabelRole, CapacityLabel);

	CapacityCombo = new QComboBox(lecturer);
	CapacityCombo->setObjectName(QString::fromUtf8("CapacityCombo"));
	sizePolicy2.setHeightForWidth(CapacityCombo->sizePolicy().hasHeightForWidth());
	CapacityCombo->setSizePolicy(sizePolicy2);

	formLayout_3->setWidget(4, QFormLayout::FieldRole, CapacityCombo);

	line_3 = new QFrame(lecturer);
	line_3->setObjectName(QString::fromUtf8("line_3"));
	sizePolicy2.setHeightForWidth(line_3->sizePolicy().hasHeightForWidth());
	line_3->setSizePolicy(sizePolicy2);
	line_3->setLineWidth(1);
	line_3->setMidLineWidth(1);
	line_3->setFrameShape(QFrame::HLine);
	line_3->setFrameShadow(QFrame::Sunken);

	formLayout_3->setWidget(5, QFormLayout::FieldRole, line_3);

	nameLabel = new QLabel(lecturer);
	nameLabel->setObjectName(QString::fromUtf8("nameLabel"));

	formLayout_3->setWidget(6, QFormLayout::LabelRole, nameLabel);

	NameCombo = new QComboBox(lecturer);
	NameCombo->setObjectName(QString::fromUtf8("NameCombo"));
	sizePolicy2.setHeightForWidth(NameCombo->sizePolicy().hasHeightForWidth());
	NameCombo->setSizePolicy(sizePolicy2);

	formLayout_3->setWidget(6, QFormLayout::FieldRole, NameCombo);

	TopicsLabel_2 = new QLabel(lecturer);
	TopicsLabel_2->setObjectName(QString::fromUtf8("TopicsLabel_2"));
	sizePolicy3.setHeightForWidth(TopicsLabel_2->sizePolicy().hasHeightForWidth());
	TopicsLabel_2->setSizePolicy(sizePolicy3);
	TopicsLabel_2->setScaledContents(false);
	TopicsLabel_2->setWordWrap(true);

	formLayout_3->setWidget(7, QFormLayout::LabelRole, TopicsLabel_2);

	TopicsCombo_2 = new QComboBox(lecturer);
	TopicsCombo_2->setObjectName(QString::fromUtf8("TopicsCombo_2"));
	sizePolicy2.setHeightForWidth(TopicsCombo_2->sizePolicy().hasHeightForWidth());
	TopicsCombo_2->setSizePolicy(sizePolicy2);

	formLayout_3->setWidget(7, QFormLayout::FieldRole, TopicsCombo_2);

	SuitableCoursesLabel_2 = new QLabel(lecturer);
	SuitableCoursesLabel_2->setObjectName(QString::fromUtf8("SuitableCoursesLabel_2"));
	sizePolicy3.setHeightForWidth(SuitableCoursesLabel_2->sizePolicy().hasHeightForWidth());
	SuitableCoursesLabel_2->setSizePolicy(sizePolicy3);
	SuitableCoursesLabel_2->setScaledContents(false);
	SuitableCoursesLabel_2->setWordWrap(true);

	formLayout_3->setWidget(8, QFormLayout::LabelRole, SuitableCoursesLabel_2);

	Courses_Combo = new QComboBox(lecturer);
	Courses_Combo->setObjectName(QString::fromUtf8("Courses_Combo"));
	sizePolicy2.setHeightForWidth(Courses_Combo->sizePolicy().hasHeightForWidth());
	Courses_Combo->setSizePolicy(sizePolicy2);

	formLayout_3->setWidget(8, QFormLayout::FieldRole, Courses_Combo);

	verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_3->setItem(10, QFormLayout::FieldRole, verticalSpacer_7);


	verticalLayout_10->addLayout(formLayout_3);

	lecturerPreview = new QTextEdit(lecturer);
	lecturerPreview->setObjectName(QString::fromUtf8("lecturerPreview"));

	verticalLayout_10->addWidget(lecturerPreview);


	verticalLayout_11->addLayout(verticalLayout_10);

	stackedWidget->addWidget(lecturer);
	course = new QWidget();
	course->setObjectName(QString::fromUtf8("course"));
	verticalLayout_13 = new QVBoxLayout(course);
	verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
	verticalLayout_12 = new QVBoxLayout();
	verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
	verticalLayout_12->setContentsMargins(-1, 0, -1, -1);
	formLayout_4 = new QFormLayout();
	formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
	formLayout_4->setContentsMargins(-1, -1, 10, -1);
	horizontalSpacer_4 = new QSpacerItem(150, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	formLayout_4->setItem(0, QFormLayout::LabelRole, horizontalSpacer_4);

	verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_4->setItem(0, QFormLayout::FieldRole, verticalSpacer_8);

	label_4 = new QLabel(course);
	label_4->setObjectName(QString::fromUtf8("label_4"));

	formLayout_4->setWidget(1, QFormLayout::FieldRole, label_4);

	IDLabel_3 = new QLabel(course);
	IDLabel_3->setObjectName(QString::fromUtf8("IDLabel_3"));

	formLayout_4->setWidget(2, QFormLayout::LabelRole, IDLabel_3);

	IDColumnCombo_3 = new QComboBox(course);
	IDColumnCombo_3->setObjectName(QString::fromUtf8("IDColumnCombo_3"));
	sizePolicy2.setHeightForWidth(IDColumnCombo_3->sizePolicy().hasHeightForWidth());
	IDColumnCombo_3->setSizePolicy(sizePolicy2);

	formLayout_4->setWidget(2, QFormLayout::FieldRole, IDColumnCombo_3);

	PriorityLabel = new QLabel(course);
	PriorityLabel->setObjectName(QString::fromUtf8("PriorityLabel"));

	formLayout_4->setWidget(3, QFormLayout::LabelRole, PriorityLabel);

	PriorityCombo = new QComboBox(course);
	PriorityCombo->setObjectName(QString::fromUtf8("PriorityCombo"));
	sizePolicy2.setHeightForWidth(PriorityCombo->sizePolicy().hasHeightForWidth());
	PriorityCombo->setSizePolicy(sizePolicy2);

	formLayout_4->setWidget(3, QFormLayout::FieldRole, PriorityCombo);

	verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_4->setItem(4, QFormLayout::FieldRole, verticalSpacer_11);


	verticalLayout_12->addLayout(formLayout_4);

	coursePreview = new QTextEdit(course);
	coursePreview->setObjectName(QString::fromUtf8("coursePreview"));

	verticalLayout_12->addWidget(coursePreview);


	verticalLayout_13->addLayout(verticalLayout_12);

	stackedWidget->addWidget(course);
	student = new QWidget();
	student->setObjectName(QString::fromUtf8("student"));
	verticalLayout_15 = new QVBoxLayout(student);
	verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
	verticalLayout_14 = new QVBoxLayout();
	verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
	verticalLayout_14->setContentsMargins(-1, 0, -1, -1);
	formLayout_5 = new QFormLayout();
	formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
	formLayout_5->setContentsMargins(-1, -1, 10, -1);
	verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_5->setItem(1, QFormLayout::FieldRole, verticalSpacer_9);

	label_5 = new QLabel(student);
	label_5->setObjectName(QString::fromUtf8("label_5"));

	formLayout_5->setWidget(2, QFormLayout::FieldRole, label_5);

	IDLabel_4 = new QLabel(student);
	IDLabel_4->setObjectName(QString::fromUtf8("IDLabel_4"));

	formLayout_5->setWidget(3, QFormLayout::LabelRole, IDLabel_4);

	IDColumnCombo_4 = new QComboBox(student);
	IDColumnCombo_4->setObjectName(QString::fromUtf8("IDColumnCombo_4"));
	sizePolicy2.setHeightForWidth(IDColumnCombo_4->sizePolicy().hasHeightForWidth());
	IDColumnCombo_4->setSizePolicy(sizePolicy2);

	formLayout_5->setWidget(3, QFormLayout::FieldRole, IDColumnCombo_4);

	FirstPrefLabel = new QLabel(student);
	FirstPrefLabel->setObjectName(QString::fromUtf8("FirstPrefLabel"));

	formLayout_5->setWidget(4, QFormLayout::LabelRole, FirstPrefLabel);

	PrefsStartCombo = new QComboBox(student);
	PrefsStartCombo->setObjectName(QString::fromUtf8("PrefsStartCombo"));
	sizePolicy2.setHeightForWidth(PrefsStartCombo->sizePolicy().hasHeightForWidth());
	PrefsStartCombo->setSizePolicy(sizePolicy2);

	formLayout_5->setWidget(4, QFormLayout::FieldRole, PrefsStartCombo);

	LastPrefLabel_2 = new QLabel(student);
	LastPrefLabel_2->setObjectName(QString::fromUtf8("LastPrefLabel_2"));

	formLayout_5->setWidget(5, QFormLayout::LabelRole, LastPrefLabel_2);

	PrefsEndCombo = new QComboBox(student);
	PrefsEndCombo->setObjectName(QString::fromUtf8("PrefsEndCombo"));
	sizePolicy2.setHeightForWidth(PrefsEndCombo->sizePolicy().hasHeightForWidth());
	PrefsEndCombo->setSizePolicy(sizePolicy2);

	formLayout_5->setWidget(5, QFormLayout::FieldRole, PrefsEndCombo);

	line_5 = new QFrame(student);
	line_5->setObjectName(QString::fromUtf8("line_5"));
	sizePolicy2.setHeightForWidth(line_5->sizePolicy().hasHeightForWidth());
	line_5->setSizePolicy(sizePolicy2);
	line_5->setLineWidth(1);
	line_5->setMidLineWidth(1);
	line_5->setFrameShape(QFrame::HLine);
	line_5->setFrameShadow(QFrame::Sunken);

	formLayout_5->setWidget(6, QFormLayout::FieldRole, line_5);

	FirstTopicPrefLabe = new QLabel(student);
	FirstTopicPrefLabe->setObjectName(QString::fromUtf8("FirstTopicPrefLabe"));
	sizePolicy3.setHeightForWidth(FirstTopicPrefLabe->sizePolicy().hasHeightForWidth());
	FirstTopicPrefLabe->setSizePolicy(sizePolicy3);
	FirstTopicPrefLabe->setScaledContents(false);
	FirstTopicPrefLabe->setWordWrap(false);

	formLayout_5->setWidget(7, QFormLayout::LabelRole, FirstTopicPrefLabe);

	FirstTopicPrefCombo = new QComboBox(student);
	FirstTopicPrefCombo->setObjectName(QString::fromUtf8("FirstTopicPrefCombo"));
	sizePolicy2.setHeightForWidth(FirstTopicPrefCombo->sizePolicy().hasHeightForWidth());
	FirstTopicPrefCombo->setSizePolicy(sizePolicy2);

	formLayout_5->setWidget(7, QFormLayout::FieldRole, FirstTopicPrefCombo);

	LastTopicPrefLabel = new QLabel(student);
	LastTopicPrefLabel->setObjectName(QString::fromUtf8("LastTopicPrefLabel"));
	sizePolicy3.setHeightForWidth(LastTopicPrefLabel->sizePolicy().hasHeightForWidth());
	LastTopicPrefLabel->setSizePolicy(sizePolicy3);
	LastTopicPrefLabel->setScaledContents(false);
	LastTopicPrefLabel->setWordWrap(false);

	formLayout_5->setWidget(8, QFormLayout::LabelRole, LastTopicPrefLabel);

	LastTopicPrefCombo_3 = new QComboBox(student);
	LastTopicPrefCombo_3->setObjectName(QString::fromUtf8("LastTopicPrefCombo_3"));
	sizePolicy2.setHeightForWidth(LastTopicPrefCombo_3->sizePolicy().hasHeightForWidth());
	LastTopicPrefCombo_3->setSizePolicy(sizePolicy2);

	formLayout_5->setWidget(8, QFormLayout::FieldRole, LastTopicPrefCombo_3);

	line_6 = new QFrame(student);
	line_6->setObjectName(QString::fromUtf8("line_6"));
	sizePolicy2.setHeightForWidth(line_6->sizePolicy().hasHeightForWidth());
	line_6->setSizePolicy(sizePolicy2);
	line_6->setFrameShape(QFrame::HLine);
	line_6->setFrameShadow(QFrame::Sunken);

	formLayout_5->setWidget(9, QFormLayout::FieldRole, line_6);

	label = new QLabel(student);
	label->setObjectName(QString::fromUtf8("label"));

	formLayout_5->setWidget(10, QFormLayout::LabelRole, label);

	CourseCombo = new QComboBox(student);
	CourseCombo->setObjectName(QString::fromUtf8("CourseCombo"));
	sizePolicy2.setHeightForWidth(CourseCombo->sizePolicy().hasHeightForWidth());
	CourseCombo->setSizePolicy(sizePolicy2);

	formLayout_5->setWidget(10, QFormLayout::FieldRole, CourseCombo);

	verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	formLayout_5->setItem(11, QFormLayout::FieldRole, verticalSpacer_10);

	horizontalSpacer_5 = new QSpacerItem(150, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	formLayout_5->setItem(1, QFormLayout::LabelRole, horizontalSpacer_5);


	verticalLayout_14->addLayout(formLayout_5);

	studentPreview = new QTextEdit(student);
	studentPreview->setObjectName(QString::fromUtf8("studentPreview"));

	verticalLayout_14->addWidget(studentPreview);


	verticalLayout_15->addLayout(verticalLayout_14);

	stackedWidget->addWidget(student);

	verticalLayout_4->addWidget(stackedWidget, 0, Qt::AlignLeft);


	verticalLayout_5->addLayout(verticalLayout_4);


	verticalLayout_2->addWidget(infoWidget, 0, Qt::AlignLeft);

	verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	verticalLayout_2->addItem(verticalSpacer_2);


	horizontalLayout->addLayout(verticalLayout_2);

	verticalLayout_3 = new QVBoxLayout();
	verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
	verticalLayout_3->setSizeConstraint(QLayout::SetNoConstraint);
	verticalLayout_3->setContentsMargins(0, -1, -1, -1);
	tableWidget = new QTableWidget(widget);
	if (tableWidget->columnCount() < 5)
	  tableWidget->setColumnCount(5);
	if (tableWidget->rowCount() < 5)
	  tableWidget->setRowCount(5);
	tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
	QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Preferred);
	sizePolicy4.setHorizontalStretch(0);
	sizePolicy4.setVerticalStretch(0);
	sizePolicy4.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
	tableWidget->setSizePolicy(sizePolicy4);
	tableWidget->setMinimumSize(QSize(0, 0));
	tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tableWidget->setRowCount(5);
	tableWidget->setColumnCount(5);

	verticalLayout_3->addWidget(tableWidget);


	horizontalLayout->addLayout(verticalLayout_3);


	verticalLayout->addWidget(widget);

	buttonBox = new QDialogButtonBox(DataBrowser);
	buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
	buttonBox->setOrientation(Qt::Horizontal);
	buttonBox->setStandardButtons(QDialogButtonBox::Abort | QDialogButtonBox::Yes);
	buttonBox->setCenterButtons(false);

	verticalLayout->addWidget(buttonBox);


	retranslateUi(DataBrowser);
	QObject::connect(buttonBox, SIGNAL(accepted()), DataBrowser, SLOT(accept()));
	QObject::connect(buttonBox, SIGNAL(rejected()), DataBrowser, SLOT(reject()));

	stackedWidget->setCurrentIndex(2);


	QMetaObject::connectSlotsByName(DataBrowser);
  }// setupUi

  void retranslateUi(QDialog *DataBrowser)
  {
	DataBrowser->setWindowTitle(QCoreApplication::translate("DataBrowser", "Dialog", nullptr));
	DataTypeLabel->setText(QCoreApplication::translate("DataBrowser", "Data Type", nullptr));
	blankPreview->setPlaceholderText(QCoreApplication::translate("DataBrowser",
	  "Select a data type and\n"
	  "                                                                                         options will appear here to\n"
	  "                                                                                         configure the import\n"
	  "                                                                                     ",
	  nullptr));
	label_2->setText(QCoreApplication::translate("DataBrowser", "Select matching columns...", nullptr));
	IDLabel->setText(QCoreApplication::translate("DataBrowser", "ID", nullptr));
	TitleLabel->setText(QCoreApplication::translate("DataBrowser", "Title", nullptr));
	CapacityLabel_2->setText(QCoreApplication::translate("DataBrowser", "Capacity", nullptr));
	LastPrefLabel->setText(QCoreApplication::translate("DataBrowser", "Lecturer", nullptr));
#if QT_CONFIG(tooltip)
	TopicsLabel->setToolTip(QCoreApplication::translate("DataBrowser",
	  "The least favoured\n"
	  "                                                                                                 topic offered by the\n"
	  "                                                                                                 studenet (if using,\n"
	  "                                                                                                 otherwise leave blank)\n"
	  "                                                                                             ",
	  nullptr));
#endif// QT_CONFIG(tooltip)
	TopicsLabel->setText(QCoreApplication::translate("DataBrowser", "Topics", nullptr));
#if QT_CONFIG(tooltip)
	SuitableCoursesLabel->setToolTip(QCoreApplication::translate("DataBrowser",
	  "The least favoured\n"
	  "                                                                                                 topic offered by the\n"
	  "                                                                                                 studenet (if using,\n"
	  "                                                                                                 otherwise leave blank)\n"
	  "                                                                                             ",
	  nullptr));
#endif// QT_CONFIG(tooltip)
	SuitableCoursesLabel->setText(QCoreApplication::translate("DataBrowser", "Suitable Courses", nullptr));
	projectPreview->setPlaceholderText(QCoreApplication::translate("DataBrowser",
	  "Select columns and a\n"
	  "                                                                                         preview of a project will\n"
	  "                                                                                         appear here for verification.\n"
	  "                                                                                     ",
	  nullptr));
	label_3->setText(QCoreApplication::translate("DataBrowser", "Select matching columns...", nullptr));
	IDLabel_2->setText(QCoreApplication::translate("DataBrowser", "ID", nullptr));
	CapacityLabel->setText(QCoreApplication::translate("DataBrowser", "Capacity", nullptr));
	nameLabel->setText(QCoreApplication::translate("DataBrowser", "Name", nullptr));
#if QT_CONFIG(tooltip)
	TopicsLabel_2->setToolTip(QCoreApplication::translate("DataBrowser",
	  "The least favoured\n"
	  "                                                                                                 topic offered by the\n"
	  "                                                                                                 studenet (if using,\n"
	  "                                                                                                 otherwise leave blank)\n"
	  "                                                                                             ",
	  nullptr));
#endif// QT_CONFIG(tooltip)
	TopicsLabel_2->setText(QCoreApplication::translate("DataBrowser", "Topics", nullptr));
#if QT_CONFIG(tooltip)
	SuitableCoursesLabel_2->setToolTip(QCoreApplication::translate("DataBrowser",
	  "The least favoured\n"
	  "                                                                                                 topic offered by the\n"
	  "                                                                                                 studenet (if using,\n"
	  "                                                                                                 otherwise leave blank)\n"
	  "                                                                                             ",
	  nullptr));
#endif// QT_CONFIG(tooltip)
	SuitableCoursesLabel_2->setText(QCoreApplication::translate("DataBrowser", "Suitable Courses", nullptr));
	lecturerPreview->setPlaceholderText(QCoreApplication::translate("DataBrowser",
	  "Select columns and a\n"
	  "                                                                                         preview of a lecturer will\n"
	  "                                                                                         appear here for verification.\n"
	  "                                                                                     ",
	  nullptr));
	label_4->setText(QCoreApplication::translate("DataBrowser",
	  "Select matching\n"
	  "                                                                                                 columns...\n"
	  "                                                                                             ",
	  nullptr));
	IDLabel_3->setText(QCoreApplication::translate("DataBrowser", "ID", nullptr));
	PriorityLabel->setText(QCoreApplication::translate("DataBrowser", "Priority", nullptr));
	coursePreview->setPlaceholderText(QCoreApplication::translate("DataBrowser",
	  "Select columns and a\n"
	  "                                                                                         preview of a course will appear\n"
	  "                                                                                         here for verification.\n"
	  "                                                                                     ",
	  nullptr));
	label_5->setText(QCoreApplication::translate("DataBrowser", "Select matching columns...", nullptr));
	IDLabel_4->setText(QCoreApplication::translate("DataBrowser", "ID", nullptr));
	FirstPrefLabel->setText(QCoreApplication::translate("DataBrowser", "First Preference", nullptr));
	LastPrefLabel_2->setText(QCoreApplication::translate("DataBrowser", "Last Preference", nullptr));
#if QT_CONFIG(tooltip)
	FirstTopicPrefLabe->setToolTip(QCoreApplication::translate("DataBrowser",
	  "The least favoured\n"
	  "                                                                                                 topic offered by the\n"
	  "                                                                                                 studenet (if using,\n"
	  "                                                                                                 otherwise leave blank)\n"
	  "                                                                                             ",
	  nullptr));
#endif// QT_CONFIG(tooltip)
	FirstTopicPrefLabe->setText(QCoreApplication::translate("DataBrowser", "First Topic Preference (if present)", nullptr));
#if QT_CONFIG(tooltip)
	LastTopicPrefLabel->setToolTip(QCoreApplication::translate("DataBrowser",
	  "The least favoured\n"
	  "                                                                                                 topic offered by the\n"
	  "                                                                                                 studenet (if using,\n"
	  "                                                                                                 otherwise leave blank)\n"
	  "                                                                                             ",
	  nullptr));
#endif// QT_CONFIG(tooltip)
	LastTopicPrefLabel->setText(QCoreApplication::translate("DataBrowser", "Last Topic Preference (if present)", nullptr));
	label->setText(QCoreApplication::translate("DataBrowser", "Course", nullptr));
	studentPreview->setPlaceholderText(QCoreApplication::translate("DataBrowser",
	  "Select columns and a\n"
	  "                                                                                         preview of a student will\n"
	  "                                                                                         appear here for verification.\n"
	  "                                                                                     ",
	  nullptr));
  }// retranslateUi
};

namespace Ui {
    class DataBrowser: public Ui_DataBrowser {};
} // namespace Ui

QT_END_NAMESPACE

#endif// IMPORTEDDATABROWSERVZLHMV_H
