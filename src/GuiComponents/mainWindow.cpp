//
// Created by tom on 04/10/2020.
//

#include "mainWindow.h"
#include "Windows/DataViewer.h"
#include "../Allocator/Algorithms/DebugAllocator.h"
#include "../Allocator/Algorithms/AbrahamAllocator.h"
#include "../Allocator/Algorithms/AbrahamV2wCourses.h"
#include <fmt/core.h>
#include <iostream>
#include <QMenu>
#include <QEvent>
#include <rapidcsv.h>
#include <QString>
#include <QHostInfo>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <yaml-cpp/yaml.h>
#include <xlnt/xlnt.hpp>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
  setupUi(this);
  connect(actionImport_Data, &QAction::triggered, this, &MainWindow::processImportedData);
  QString name = qgetenv("USER");
  if (name.isEmpty())
	name = qgetenv("USERNAME");
  setWindowTitle(QString::fromStdString(fmt::format("SPrAl - {} @ {}", name.toStdString(), QHostInfo::localHostName().toStdString())));
  connect(runButton, &QPushButton::pressed, this, &MainWindow::runAllocation);
  algoChoiceCombo->addItem("");
  algoChoiceCombo->addItem("Debug Allocator");
  algoChoiceCombo->addItem("Abraham");
  algoChoiceCombo->addItem("AbrahamV2");


  actionEdit->setEnabled(false);
  runButton->setEnabled(false);
  connect(algoChoiceCombo, &QComboBox::currentTextChanged, this, &MainWindow::setupAlgoSettings);
  connect(studentStatisticCombo, &QComboBox::currentTextChanged, this, &MainWindow::PlotStudentStats);
  connect(projectStatisticCombo, &QComboBox::currentTextChanged, this, &MainWindow::PlotProjectStats);
  connect(actionView_All, &QAction::triggered, this, &MainWindow::lauchDataViewer);
  connect(actionResults, &QAction::triggered, this, &MainWindow::SaveResults);
  connect(actionSession, &QAction::triggered, this, &MainWindow::SaveSession);
  connect(actionSession_2, &QAction::triggered, this, &MainWindow::LoadSession);
  connect(allocationResultsChoser, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::UpdateResultsStats);
  connect(actionExit, &QAction::triggered, this, QApplication::exit);
  connect(unassignedStudentsList, &QListWidget::currentTextChanged, this, &MainWindow::ShowUnassignedStudentDetails);
  connect(freeProjectsList, &QListWidget::currentTextChanged, this, &MainWindow::ShowFreeProjectDetails);

  projectStatisticChart->setChart(new QChart());
  projectStatisticChart->chart()->setTheme(QChart::ChartThemeDark);
  projectStatisticChart->setRenderHint(QPainter::Antialiasing);

  studentStatsChart->setChart(new QChart());
  studentStatsChart->chart()->setTheme(QChart::ChartThemeDark);
  studentStatsChart->setRenderHint(QPainter::Antialiasing);

  outputStats->setChart(new QChart());
  outputStats->chart()->setTheme(QChart::ChartTheme::ChartThemeDark);
  outputStats->setRenderHint(QPainter::Antialiasing);

  tabWidget->setCurrentIndex(0);
}

void MainWindow::processImportedData()
{
  QString fileName = QFileDialog::getOpenFileName(this, QObject::tr("Select Data File"), QString(), QObject::tr("*"), new QString(""), QFileDialog::DontUseNativeDialog);
  if (fileName.isEmpty()) {
	return;
  }
  auto dataBrowser = new ImportedDataBrowser(this);
  //  // read csv and populate table
  dataFilenames.insert_or_assign("projects", "/home/tom/Desktop/Projects.csv");
  dataFilenames.insert_or_assign("students", "/home/tom/Desktop/Students.csv");
  dataFilenames.insert_or_assign("courses", "/home/tom/Desktop/Courses.csv");
  dataFilenames.insert_or_assign("lecturers", "/home/tom/Desktop/Lecturers.csv");
  rapidcsv::Document doc(fileName.toStdString(), rapidcsv::LabelParams(0, -1));
  dataBrowser->setCurrentSpreadsheet(&doc);
  dataBrowser->tableWidget->setRowCount(doc.GetRowCount() + 1);
  dataBrowser->tableWidget->setColumnCount(static_cast<int>(doc.GetColumnCount()));
  std::vector<std::string> colHeaderNames = doc.GetColumnNames();
  int columnCounter = 0;
  for (const auto &columnName : colHeaderNames) {
	auto thisItem = new QTableWidgetItem(QString::fromStdString(columnName));
	dataBrowser->tableWidget->setItem(0, columnCounter, thisItem);
	columnCounter++;
  }
  for (int rowIdx = 0; rowIdx < doc.GetRowCount(); rowIdx++) {
	for (int colIdx = 0; colIdx < doc.GetColumnCount(); colIdx++) {
	  auto thisItem = new QTableWidgetItem(QString::fromStdString(doc.GetCell<std::string>(colIdx, rowIdx)));
	  dataBrowser->tableWidget->setItem(rowIdx + 1, colIdx, thisItem);
	}
  }

  if (dataBrowser->exec() != QDialog::Accepted) {
	return;
  }

  std::string completedFile = dataBrowser->stackedWidget->currentWidget()->objectName().toStdString();

  auto optionalColumnName = [&dataBrowser](const std::string &boxName) -> std::optional<std::string> {
	auto boxText = dataBrowser->stackedWidget->findChild<QComboBox *>(QString::fromStdString(boxName))->currentText();
	if (boxText.isEmpty()) {
	  return {};
	} else {
	  return boxText.toStdString();
	}
  };

  auto getColumnIdxFromComboName = [&dataBrowser, &doc](const std::string &comboBoxName) -> ssize_t {
	const auto box = dataBrowser->stackedWidget->findChild<QComboBox *>(QString::fromStdString(comboBoxName));
	if (box)
	  return doc.GetColumnIdx(box->currentText().toStdString());
	else
	  throw std::runtime_error("Couldn't find combobox");
  };

  auto getStringFromTableWidget = [&dataBrowser](const ssize_t rowIndex, const ssize_t columnIdx) -> std::string {
	return dataBrowser->tableWidget->item(rowIndex, columnIdx)->text().toStdString();
  };

  if (completedFile == "student") {
	students.clear();

	// Extract column indicies from ComboBox selections for required columns
	const ssize_t IDColumnIndex = getColumnIdxFromComboName("IDColumnCombo_4");
	const ssize_t Preference1ColumnIndex = getColumnIdxFromComboName("PrefsStartCombo");
	const ssize_t PreferenceNColumnIndex = getColumnIdxFromComboName("PrefsEndCombo");
	const ssize_t CourseColumnIndex = getColumnIdxFromComboName("CourseCombo");

	// Optionals
	const std::optional<std::string> TopicPrefsStartColumnName = optionalColumnName("FirstTopicPrefCombo");
	const std::optional<std::string> TopicPrefsStopColumnName = optionalColumnName("LastTopicPrefCombo_3");
	const size_t numTopicPrefs = (TopicPrefsStartColumnName && TopicPrefsStopColumnName) ? static_cast<const size_t>(doc.GetColumnIdx(TopicPrefsStopColumnName.value()) - doc.GetColumnIdx(TopicPrefsStartColumnName.value())) + 1 : 0;

	const auto numPrefs = (Preference1ColumnIndex >= 0 && PreferenceNColumnIndex >= 0) ? static_cast<const size_t>(PreferenceNColumnIndex - Preference1ColumnIndex) + 1 : 0;
	const auto tableWidgetRowCount = static_cast<size_t>(dataBrowser->tableWidget->rowCount());

	for (size_t rowIndex = 1; rowIndex < tableWidgetRowCount; rowIndex++) {
	  std::vector<std::string> prefs;
	  prefs.reserve(numPrefs);
	  for (size_t prefIdx = 0; prefIdx < numPrefs; prefIdx++) {
		//TODO make this substring extraction customisable/optional
		const auto cellText = dataBrowser->tableWidget->item(rowIndex, static_cast<size_t>(Preference1ColumnIndex) + prefIdx)->text().toStdString();
		if (!cellText.empty()) {
		  prefs.push_back(cellText);
		}
	  }
	  if (numTopicPrefs > 0) {
		const auto topicPrefStartColumnIdx = static_cast<const size_t>(doc.GetColumnIdx(TopicPrefsStartColumnName.value()));
		std::vector<std::string> topicPrefs;
		topicPrefs.reserve(numTopicPrefs);
		for (size_t topicPrefIdx = 0; topicPrefIdx < numTopicPrefs; topicPrefIdx++) {
		  topicPrefs.push_back(dataBrowser->tableWidget->item(rowIndex, topicPrefStartColumnIdx + topicPrefIdx)->text().toStdString());
		}
		students.emplace_back(dataBrowser->tableWidget->item(rowIndex, IDColumnIndex)->text().toStdString(), prefs, dataBrowser->tableWidget->item(rowIndex, CourseColumnIndex)->text().toStdString(), topicPrefs);
	  } else {
		students.emplace_back(dataBrowser->tableWidget->item(rowIndex, IDColumnIndex)->text().toStdString(), prefs, dataBrowser->tableWidget->item(rowIndex, CourseColumnIndex)->text().toStdString());
	  }
	}
	numberStudentPreferences = numPrefs;
	studentsImported = true;
  } else if (completedFile == "project") {
	projects.clear();
	// Import project related stuff
	// NOTE: courses are allowed to be semi-colon sep into their own list, as are topics;
	const ssize_t IDColumnIndex = getColumnIdxFromComboName("IDColumnCombo");
	const ssize_t LecturerColumnIndex = getColumnIdxFromComboName("LecturerCombo");
	const ssize_t CapacityColumnIndex = getColumnIdxFromComboName("CapacityCombo_2");
	const ssize_t SuitableCoursesColumnIndex = getColumnIdxFromComboName("SuitableCoursesCombo");
	const std::optional<std::string> TitleColumnName = optionalColumnName("TitleCombo");
	const std::optional<std::string> TopicsColumnName = optionalColumnName("TopicsCombo");

	const auto tableWidgetRowCount = static_cast<size_t>(dataBrowser->tableWidget->rowCount());

	bool hasTopics{ TopicsColumnName.has_value() };
	bool hasTitle{ TitleColumnName.has_value() };
	std::vector<std::string> projectTopics{};
	std::vector<std::string> projectCourses{};
	std::string projectTitle;
	for (size_t rowIndex = 1; rowIndex < tableWidgetRowCount; rowIndex++) {
	  const std::string projectID = getStringFromTableWidget(rowIndex, IDColumnIndex);
	  const std::string projectLecturer = getStringFromTableWidget(rowIndex, LecturerColumnIndex);
	  const int projectCapacity = std::stoi(getStringFromTableWidget(rowIndex, CapacityColumnIndex));
	  if (hasTopics) {
		const auto titleColumnIndex = doc.GetColumnIdx(TopicsColumnName.value());
		std::stringstream fullTopicsList;
		fullTopicsList << dataBrowser->tableWidget->item(rowIndex, titleColumnIndex)->text().toStdString();
		std::string element;
		while (std::getline(fullTopicsList, element, ';')) {
		  projectTopics.push_back(element);
		}
	  }
	  std::stringstream fullCoursesList;
	  fullCoursesList << dataBrowser->tableWidget->item(rowIndex, SuitableCoursesColumnIndex)->text().toStdString();
	  std::string element;
	  while (std::getline(fullCoursesList, element, ';')) {
		projectCourses.push_back(element);
	  }
	  if (hasTitle) {
		const auto TitleColumnIndex = getColumnIdxFromComboName("TitleCombo");
		projectTitle = getStringFromTableWidget(rowIndex, TitleColumnIndex);
	  }
	  projects.emplace_back(projectID, projectLecturer, projectCapacity, projectCourses, projectTitle, projectTopics);

	  projectTopics.clear();
	  projectCourses.clear();
	  projectStatisticCombo->addItem(QString::fromStdString(projectID));
	}
	projectsImported = true;
  } else if (completedFile == "lecturer") {
	lecturers.clear();
	const ssize_t IDColumnIndex = getColumnIdxFromComboName("IDColumnCombo_2");
	const ssize_t CapacityColumnIndex = getColumnIdxFromComboName("CapacityCombo");
	const std::optional<std::string> NameColumnName = optionalColumnName("NameCombo");
	const std::optional<std::string> TopicsColumnName = optionalColumnName("TopicsCombo_2");
	const std::optional<std::string> CoursesColumnName = optionalColumnName("Courses_Combo");

	const auto tableWidgetRowCount = static_cast<size_t>(dataBrowser->tableWidget->rowCount());
	bool hasName{ NameColumnName.has_value() };
	bool hasTopics{ TopicsColumnName.has_value() };
	bool hasCourses{ CoursesColumnName.has_value() };
	std::vector<std::string> lecturerTopics{};
	std::vector<std::string> lecturerCourses{};
	std::string lecturerName = "";
	for (size_t rowIndex = 1; rowIndex < tableWidgetRowCount; rowIndex++) {
	  if (hasTopics) {
		const auto titleColumnIndex = doc.GetColumnIdx(TopicsColumnName.value());
		std::stringstream fullTopicsList;
		fullTopicsList << dataBrowser->tableWidget->item(rowIndex, titleColumnIndex)->text().toStdString();
		std::string element;
		while (std::getline(fullTopicsList, element, ';')) {
		  lecturerTopics.push_back(element);
		}
	  }
	  if (hasCourses) {
		const auto titleColumnIndex = doc.GetColumnIdx(CoursesColumnName.value());
		std::stringstream fullCourseList;
		fullCourseList << dataBrowser->tableWidget->item(rowIndex, titleColumnIndex)->text().toStdString();
		std::string element;
		while (std::getline(fullCourseList, element, ';')) {
		  lecturerCourses.push_back(element);
		}
	  }
	  if (hasName) {
		lecturerName = getStringFromTableWidget(rowIndex, getColumnIdxFromComboName("NameCombo"));
	  }

	  lecturers.emplace_back(
		getStringFromTableWidget(rowIndex, IDColumnIndex), static_cast<int16_t>(std::stoi(getStringFromTableWidget(rowIndex, CapacityColumnIndex))), lecturerName, lecturerTopics, lecturerCourses);

	  lecturerCourses.clear();
	  lecturerTopics.clear();
	}
	lecturersImported = true;
  } else if (completedFile == "course") {
	courses.clear();
	const ssize_t IDColumnIndex = getColumnIdxFromComboName("IDColumnCombo_3");
	const ssize_t PriorityColumnIndex = getColumnIdxFromComboName("PriorityCombo");
	const auto tableWidgetRowCount = static_cast<size_t>(dataBrowser->tableWidget->rowCount());
	for (size_t rowIndex = 1; rowIndex < tableWidgetRowCount; rowIndex++) {
	  courses.emplace_back(getStringFromTableWidget(rowIndex, IDColumnIndex), std::stoi(getStringFromTableWidget(rowIndex, PriorityColumnIndex)));
	}
	coursesImported = true;
  } else {
	throw std::runtime_error("Invalid data type read in. Email me if you see this, please.");
  }
  if (!actionEdit->isEnabled() && (coursesImported || studentsImported || projectsImported || lecturersImported)) {
	actionEdit->setEnabled(true);
  }
  UpdateImportedDataStats();
  runButton->setEnabled(true);
}

void MainWindow::runAllocation()
{
  outputBox->clear();
  auto errorSourceFromEnum = [](SPRAL::ErrorSource eSource) -> std::string {
	switch (eSource) {
	  case SPRAL::ErrorSource::STUDENTS:
		return "Students";
	  case SPRAL::ErrorSource::PROJECTS:
		return "Projects";
	  case SPRAL::ErrorSource::LECTURERS:
		return "Lecturers";
	  case SPRAL::ErrorSource::COURSES:
		return "Courses";
	  default:
		return "";
	}
  };
  // Final round of data validation before we do something we regret
  if (students.empty() || lecturers.empty() || courses.empty() || projects.empty()) {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Critical);
	msgBox.setText("One of more of the data sets have not been entered correctly");
	msgBox.setInformativeText("Please check all 4 have been properly imported and try again");
	msgBox.setDetailedText("Placeholder text for something more detailed to come at a later date (probably)");
	msgBox.exec();
	return;
  }
  if (currentAllocator) {
	if (currentAllocator->IDsNeedConverting) {
	  currentAllocator->convertIDFormat();
	}
	auto isValidated = currentAllocator->validateData();
	if (!isValidated) {
	  currentAllocator->operator()(outputBox);
	  auto newResults = currentAllocator->getResults();
	  allocationResultsChoser->addItem(QString::fromStdString(fmt::format("{}:{}", newResults.algorithmName, newResults.dateTime)));
	  allocationRuns.push_back(newResults);
	} else {
	  const auto &validationError = isValidated.value();
	  QMessageBox msgBox;
	  msgBox.setIcon(QMessageBox::Critical);
	  msgBox.setText("An error occurred during data validation");
	  msgBox.setInformativeText(QString::fromStdString(validationError.helpMessage));
	  std::string detailedText = fmt::format(
		"Full details of the validation error: \n\t Error Source: {} \n\t Error Type: {}\n\t Error Help: {}", errorSourceFromEnum(validationError.errorSource), "", validationError.helpMessage);
	  msgBox.setDetailedText(QString::fromStdString(detailedText));
	  msgBox.exec();
	}
  } else {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Critical);
	msgBox.setText("A valid allocator has not been selected");
	msgBox.setInformativeText("Please select an allocator from the drop down and try again");
	msgBox.exec();
  }
}
void MainWindow::setupAlgoSettings()
{
  switch (algoChoiceCombo->currentIndex()) {
	case 1: {
	  if (allocators.find("Debug") == allocators.end()) {
		allocators.emplace("Debug", std::make_unique<SPRAL::DebugAllocator>(students, projects, lecturers, courses));
	  }
	  algoSpecificSettings->setCurrentIndex(1);
	  currentAllocator = allocators.at("Debug").get();
	  break;
	}
	case 2: {
	  if (allocators.find("Abraham") == allocators.end()) {
		allocators.emplace("Abraham", std::make_unique<SPRAL::AbrahamAllocator>(students, projects, lecturers, courses));
	  }
	  algoSpecificSettings->setCurrentIndex(2);
	  currentAllocator = allocators.at("Abraham").get();
	  break;
	}
	case 3: {
	  if (allocators.find("AbrahamV2") == allocators.end()) {
		allocators.emplace("AbrahamV2", std::make_unique<SPRAL::AbrahamV2>(students, projects, lecturers, courses));
	  }
	  algoSpecificSettings->setCurrentIndex(3);
	  currentAllocator = allocators.at("AbrahamV2").get();
	  break;
	}
	default:
	  algoSpecificSettings->setCurrentIndex(0);
	  currentAllocator = nullptr;
	  return;
  }
  if (currentAllocator) {
	auto settingsFormLayout = algoSpecificSettings->currentWidget()->findChildren<QFormLayout *>();
	if (!settingsFormLayout.isEmpty()) {
	  for (auto &option : currentAllocator->algorithmOptionDrawOrder) {
		if (!currentAllocator->algorithmOptions[option]->hasBeenDrawn) {
		  currentAllocator->algorithmOptions[option]->draw(settingsFormLayout.at(0));
		}
	  }
	}
  } else {
	return;
  }
}
void MainWindow::PlotStudentStats()
{
  if (studentsImported && coursesImported) {
	for (auto &course : courses) {
	  for (const auto &student : students) {
		if (student.course == course.ID) {
		  course.enrolledStudents++;
		}
	  }
	}

	QPieSeries *series = new QPieSeries();
	for (const auto &course : courses) {
	  series->append(QString::fromStdString(course.ID), course.enrolledStudents);
	}
	QChart *chart = studentStatsChart->chart();
	chart->addSeries(series);
	//  chart->legend()->hide();
	studentStatsChart->setRenderHint(QPainter::Antialiasing);
  }
}
void MainWindow::PlotProjectStats(QString newValue)
{
  static bool axesDrawn{ false };
  if (newValue.isEmpty() || newValue.isNull()) {
	return;
  }
  SPRAL::Project *selectedProject{ nullptr };
  const auto &selectedProjectFindIterator = std::find_if(projects.begin(), projects.end(), [&](const SPRAL::Project &p) { return p.ID == newValue.toStdString(); });
  if (selectedProjectFindIterator != projects.end()) {
	selectedProject = &*selectedProjectFindIterator;
  }
  capacityDisplay->setText(QString::fromStdString(std::to_string(selectedProject->capacity)));
  lecturerDisplay->setText(QString::fromStdString(selectedProject->lecturer));
  if (studentsImported) {
	std::vector<int> projectRatings;
	projectRatings.reserve(numberStudentPreferences);
	for (size_t i = 0; i < numberStudentPreferences; i++) {
	  projectRatings.push_back(0);
	}
	for (const auto &student : students) {
	  const auto &projectFindIterator = std::find(student.projectPreferences.begin(), student.projectPreferences.end(), newValue.toStdString());
	  if (projectFindIterator != student.projectPreferences.end()) {
		projectRatings.at(static_cast<unsigned long>(std::distance(student.projectPreferences.begin(), projectFindIterator)))++;
	  }
	}
	auto *projectPopularity = new QBarSet(newValue);
	for (auto &value : projectRatings) {
	  projectPopularity->append(value);
	}

	auto *series = new QBarSeries();
	series->append(projectPopularity);
	auto *chart = projectStatisticChart->chart();
	chart->removeAllSeries();
	chart->addSeries(series);
	chart->setAnimationOptions(QChart::SeriesAnimations);
	if (!axesDrawn) {
	  QStringList categories;
	  categories << "1st"
				 << "2nd"
				 << "3rd"
				 << "4th"
				 << "5th"
				 << "6th"
				 << "7th";
	  auto *axisX = new QBarCategoryAxis();
	  axisX->setTitleText("Rank Position");
	  axisX->append(categories);
	  chart->addAxis(axisX, Qt::AlignBottom);
	  series->attachAxis(axisX);

	  auto *axisY = new QValueAxis();
	  axisY->setTitleText("Number of ranks");
	  axisY->setTickType(QtCharts::QValueAxis::TicksDynamic);
	  axisY->setTickInterval(1);
	  series->attachAxis(axisY);
	  chart->addAxis(axisY, Qt::AlignLeft);
	  axesDrawn = true;
	}
	auto chartAxes = chart->axes();
	int maxValue = *std::max_element(projectRatings.begin(), projectRatings.end());
	chartAxes.at(1)->setRange(0, maxValue);
  }
}

void MainWindow::findIgnoredProjects()
{
  if (studentsImported && projectsImported) {
	std::vector<SPRAL::Project> ignoredProjects{ projects.begin(), projects.end() };
	for (const auto &student : students) {
	  for (const auto &pref : student.projectPreferences) {
		auto projectInIgnoredList = std::find_if(ignoredProjects.begin(), ignoredProjects.end(), [&pref](const SPRAL::Project &p) { return p.ID == pref; });
		if (projectInIgnoredList != ignoredProjects.end()) {
		  ignoredProjects.erase(projectInIgnoredList);
		}
	  }
	}
	NumberIgnoredProjects->setText(QString::number(ignoredProjects.size()));
  }
}

void MainWindow::lauchDataViewer()
{
  auto dataViewer = new DataViewer(this);
  QList<QTreeWidgetItem *> items;
  if (studentsImported) {
	dataViewer->tabWidget->setTabEnabled(0, true);
	dataViewer->tabWidget->setCurrentIndex(0);
	auto treeWidget = dataViewer->tabWidget->currentWidget()->findChild<QTreeWidget *>("studentTree");
	if (!treeWidget) {
	  return;
	}
	for (const auto &student : students) {
	  auto thisItem = new QTreeWidgetItem(treeWidget, QStringList{ "Name", QString::fromStdString(student.ID) });
	  thisItem->insertChild(0, new QTreeWidgetItem(thisItem, QStringList({ "Course", QString::fromStdString(student.course) })));
	  std::string studentProjectPreferencesString = "[";
	  for (const auto &p : student.projectPreferences) {
		studentProjectPreferencesString += p;
		studentProjectPreferencesString += ",";
	  }
	  if (studentProjectPreferencesString.size() > 1) {
		studentProjectPreferencesString.pop_back();
	  }
	  studentProjectPreferencesString += "]";
	  std::string studentTopicPreferencesString;
	  if (student.topicPreferences) {
		studentTopicPreferencesString = "[";
		for (const auto &t : student.topicPreferences.value()) {
		  studentTopicPreferencesString += t;
		  studentTopicPreferencesString += ",";
		}
		if (studentTopicPreferencesString.size() > 1) {
		  studentTopicPreferencesString.pop_back();
		}
		studentTopicPreferencesString += "]";
	  }
	  thisItem->insertChild(1, new QTreeWidgetItem(thisItem, QStringList({ "Preferences", QString::fromStdString(studentProjectPreferencesString) })));
	  thisItem->insertChild(2, new QTreeWidgetItem(thisItem, QStringList({ "Topic Preferences", QString::fromStdString(studentTopicPreferencesString) })));
	  thisItem->insertChild(3, new QTreeWidgetItem(thisItem, QStringList({ "Is Assigned?", student.isAssigned ? "True" : "False" })));
	  thisItem->insertChild(4, new QTreeWidgetItem(thisItem, QStringList({ "Assigned Project", QString::fromStdString(student.assignedProject) })));
	  items.append(thisItem);
	}
	treeWidget->insertTopLevelItems(0, items);
  }
  if (coursesImported) {
	dataViewer->tabWidget->setTabEnabled(3, true);
	auto treeWidget = dataViewer->tabWidget->findChild<QTreeWidget *>("coursesTree");
	if (!treeWidget) {
	  return;
	}
	for (const auto &course : courses) {
	  auto thisItem = new QTreeWidgetItem(treeWidget, QStringList{ "Name", QString::fromStdString(course.ID) });
	  thisItem->insertChild(0, new QTreeWidgetItem(thisItem, QStringList({ "Priority", QString::number(course.priority) })));
	  thisItem->insertChild(1, new QTreeWidgetItem(thisItem, QStringList({ "No. Enrolled Students", QString::number(course.enrolledStudents) })));
	  items.append(thisItem);
	}
	treeWidget->insertTopLevelItems(0, items);
  }
  if (projectsImported) {
	dataViewer->tabWidget->setTabEnabled(1, true);
	auto treeWidget = dataViewer->tabWidget->findChild<QTreeWidget *>("projectsTree");
	if (!treeWidget) {
	  return;
	}
	for (const auto &project : projects) {
	  std::string topics;
	  if (project.projectTopics) {
		topics = "[";
		for (const auto &t : project.projectTopics.value()) {
		  topics += t;
		  topics += ",";
		}
		if (topics.size() > 1) {
		  topics.pop_back();
		}
		topics += "]";
	  } else {
		topics = "";
	  }
	  std::string projectSuitableProjectsString = "[";
	  for (const auto &c : project.suitableCourses) {
		projectSuitableProjectsString += c;
		projectSuitableProjectsString += ",";
	  }
	  if (projectSuitableProjectsString.size() > 1) {
		projectSuitableProjectsString.pop_back();
	  }
	  projectSuitableProjectsString += "]";
	  std::string studentsAssignedToProject = "[";
	  for (const auto &s : project.assignedStudents) {
		studentsAssignedToProject += s;
		studentsAssignedToProject += ",";
	  }
	  if (studentsAssignedToProject.size() > 1) {
		studentsAssignedToProject.pop_back();
	  }
	  studentsAssignedToProject += "]";
	  auto thisItem = new QTreeWidgetItem(treeWidget, QStringList{ "Name", QString::fromStdString(project.ID) });
	  thisItem->insertChild(0, new QTreeWidgetItem(thisItem, QStringList{ "Name as Student Prefs.", QString::fromStdString(project.IDasInStudentPreferences) }));
	  thisItem->insertChild(1, new QTreeWidgetItem(thisItem, QStringList({ "[Title]", project.title ? QString::fromStdString(project.title.value()) : "" })));
	  thisItem->insertChild(2, new QTreeWidgetItem(thisItem, QStringList({ "Lecturer", QString::fromStdString(project.lecturer) })));
	  thisItem->insertChild(3, new QTreeWidgetItem(thisItem, QStringList({ "Capacity", QString::number(project.capacity) })));
	  thisItem->insertChild(4, new QTreeWidgetItem(thisItem, QStringList({ "[Topics]", QString::fromStdString(topics) })));
	  thisItem->insertChild(5, new QTreeWidgetItem(thisItem, QStringList({ "Suitable Courses", QString::fromStdString(projectSuitableProjectsString) })));
	  thisItem->insertChild(6, new QTreeWidgetItem(thisItem, QStringList({ "Assigned Students", QString::fromStdString(studentsAssignedToProject) })));
	  items.append(thisItem);
	}
	treeWidget->insertTopLevelItems(0, items);
  }
  if (lecturersImported) {
	dataViewer->tabWidget->setTabEnabled(2, true);
	auto treeWidget = dataViewer->tabWidget->findChild<QTreeWidget *>("lecturerTree");
	if (!treeWidget) {
	  return;
	}
	for (const auto &lecturer : lecturers) {
	  std::string lecturerTopicsString;
	  if (lecturer.topics) {
		lecturerTopicsString = "[";
		for (const auto &t : lecturer.topics.value()) {
		  lecturerTopicsString += t;
		  lecturerTopicsString += ",";
		}
		lecturerTopicsString += "]";
	  } else {
		lecturerTopicsString = "";
	  }
	  std::string lecturerAssociatedCoursesString;
	  if (lecturer.associatedCourses) {
		lecturerAssociatedCoursesString = "[";
		for (const auto &t : lecturer.associatedCourses.value()) {
		  lecturerAssociatedCoursesString += t;
		  lecturerAssociatedCoursesString += ",";
		}
		lecturerAssociatedCoursesString += "]";
	  } else {
		lecturerAssociatedCoursesString = "";
	  }
	  std::string studentsAssignedToLecturer = "[";
	  for (const auto &s : lecturer.assignedStudents) {
		studentsAssignedToLecturer += s;
		studentsAssignedToLecturer += ",";
	  }
	  studentsAssignedToLecturer += "]";
	  auto thisItem = new QTreeWidgetItem(treeWidget, QStringList{ "ID", QString::fromStdString(lecturer.ID) });
	  QString lecturerName = "";
	  if (lecturer.name.has_value()) {
		lecturerName = QString::fromStdString(lecturer.name.value());
	  }
	  thisItem->insertChild(0, new QTreeWidgetItem(thisItem, QStringList({ "[Name]", lecturerName })));
	  thisItem->insertChild(1, new QTreeWidgetItem(thisItem, QStringList({ "Capacity", QString::number(lecturer.capacity) })));
	  thisItem->insertChild(2, new QTreeWidgetItem(thisItem, QStringList({ "Occupancy", QString::number(lecturer.assignedStudents.size()) })));
	  thisItem->insertChild(3, new QTreeWidgetItem(thisItem, QStringList({ "[Topics]", QString::fromStdString(lecturerTopicsString) })));
	  thisItem->insertChild(4, new QTreeWidgetItem(thisItem, QStringList({ "[Related Courses]", QString::fromStdString(lecturerAssociatedCoursesString) })));
	  thisItem->insertChild(5, new QTreeWidgetItem(thisItem, QStringList({ "Assigned Students", QString::fromStdString(studentsAssignedToLecturer) })));
	  items.append(thisItem);
	}
	treeWidget->insertTopLevelItems(0, items);
  }

  dataViewer->exec();
}
void MainWindow::SaveResults()
{
  if (allocationRuns.empty()) {
	return;
  }
  auto allocationResults = *(--allocationRuns.end());
  if (allocationResults.allocations.empty()) {
	QMessageBox msgBox;
	msgBox.setText("Allocations list is empty");
	msgBox.setDetailedText("The list of allocations returned from the current allocator was empty. Either the algorithm was not run or an error occured during the run.");
	msgBox.exec();
	return;
  }
  QString filter = "Text CSV (*.csv);;Excel (*.xlsx)";
  QString fileName = QFileDialog::getSaveFileName(this, QObject::tr("Select save location"), QString(), filter, &filter, QFileDialog::DontUseNativeDialog);
  if (fileName.isEmpty()) {
	return;
  }
  std::string fileExtension;
  std::smatch match;
  std::string fileNameStdString = fileName.toStdString();
  if (std::regex_search(fileNameStdString, match, std::regex("(\\.\\w+)$"))) {
	fileExtension = match[0];
  }
  if (fileExtension == ".csv") {
	std::ofstream outputFile;
	outputFile.open(fileName.toStdString().c_str());
	if (!outputFile.is_open()) {
	  QMessageBox msgBox;
	  msgBox.setText("Output file could not be opened");
	  return;
	}
	outputFile << "StudentID,ProjectID,Rank,LecturerCode\n";
	for (auto &allocation : allocationResults.allocations) {
	  outputFile << allocation.student.ID << "," << allocation.project.ID << "," << allocation.student.assignedProjectRank << "," << allocation.project.lecturer << '\n';
	}
	outputFile.close();
  } else if (fileExtension == ".xlsx") {
	xlnt::workbook workbook;
	xlnt::worksheet worksheet = workbook.active_sheet();
	std::map<std::string, std::vector<std::string>> lecturerAssignments;
	auto boldFont = xlnt::font().bold(true).name("Calibri");
	worksheet.title("Student Allocations");
	worksheet.cell("A1").value("Student");
	worksheet.cell("A1").font(boldFont);
	worksheet.cell("B1").value("Project");
	worksheet.cell("B1").font(boldFont);
	worksheet.cell("C1").value("Lecturer");
	worksheet.cell("C1").font(boldFont);

	int counter = 2;
	for (const auto &allocation : allocationResults.allocations) {
	  worksheet.cell(1, counter).value(allocation.student.ID);
	  worksheet.cell(2, counter).value(allocation.project.ID);
	  worksheet.cell(3, counter).value(allocation.project.lecturer);
	  counter++;
	}
	worksheet = workbook.create_sheet();
	worksheet.title("Lecturer Allocations");
	worksheet.cell("A1").value("Lecturer");
	worksheet.cell("A1").font(boldFont);
	worksheet.cell("B1").value("Capacity");
	worksheet.cell("B1").font(boldFont);
	worksheet.cell("C1").value("Occupancy");
	worksheet.cell("C1").font(boldFont);
	worksheet.cell("D1").value("Students");
	worksheet.cell("D1").font(boldFont);

	counter = 2;
	for (const auto &lecturer : lecturers) {
	  lecturerAssignments[lecturer.ID] = {};
	  for (const auto &alloc : allocationResults.allocations) {
		if (alloc.project.lecturer == lecturer.ID && !alloc.project.assignedStudents.empty()) {
		  std::for_each(alloc.project.assignedStudents.begin(), alloc.project.assignedStudents.end(), [&](const std::string s) { lecturerAssignments[lecturer.ID].push_back(s); });
		}
	  }
	  worksheet.cell(1, counter).value(lecturer.ID);
	  worksheet.cell(2, counter).value(lecturer.capacity);
	  int occ = lecturerAssignments.at(lecturer.ID).size();
	  worksheet.cell(3, counter).value(occ);
	  if (occ < lecturer.capacity) {
		worksheet.cell(1, counter).fill(xlnt::fill::solid(xlnt::color::red()));
	  }
	  int colCounter = 4;
	  std::for_each(lecturerAssignments.at(lecturer.ID).begin(), lecturerAssignments.at(lecturer.ID).end(), [&](const std::string &p) {
		worksheet.cell(colCounter, counter).value(p);
		colCounter++;
	  });
	  counter++;
	}

	worksheet = workbook.create_sheet();
	worksheet.title("Unassigned Students");
	worksheet.cell("A1").value("Student ID");
	worksheet.cell("A1").font(boldFont);
	worksheet.cell("B1").value("Course");
	worksheet.cell("B1").font(boldFont);
	worksheet.cell("C1").value("Suggested Lecturer");
	worksheet.cell("C1").font(boldFont);
	counter = 2;
	for (const auto &student : allocationResults.unassignedStudents) {
	  worksheet.cell(1, counter).value(student.ID);
	  worksheet.cell(2, counter).value(student.course);
	  counter++;
	}
	workbook.save(fileName.toStdString());
  }
}

YAML::Emitter &operator<<(YAML::Emitter &output, const SPRAL::Student &student)
{
  output << YAML::BeginMap;// student
  output << YAML::Key << "ID" << YAML::Value << student.ID;
  output << YAML::Key << "ProjectPreferences" << YAML::Value << YAML::Flow << student.projectPreferences;
  output << YAML::Key << "Course" << YAML::Value << student.course;
  output << YAML::Key << "Name" << YAML::Value << (student.name ? student.name.value() : "");
  if (student.topicPreferences) {
	output << YAML::Key << "TopicPreferences" << YAML::Value << YAML::Flow << student.topicPreferences.value();
  } else {
	output << YAML::Key << "TopicPreferences" << YAML::Value << "[]";
  }
  output << YAML::EndMap;// student
  return output;
}
YAML::Emitter &operator<<(YAML::Emitter &output, const SPRAL::Project &project)
{
  output << YAML::BeginMap;
  output << YAML::Key << "ID" << YAML::Value << project.ID;
  output << YAML::Key << "Lecturer" << YAML::Value << project.lecturer;
  output << YAML::Key << "Capacity" << YAML::Value << project.capacity;
  output << YAML::Key << "SuitableCourses" << YAML::Value << YAML::Flow << project.suitableCourses;
  output << YAML::Key << "Title" << YAML::Value << (project.title ? project.title.value() : "");
  if (project.projectTopics) {
	output << YAML::Key << "ProjectTopics" << YAML::Value << YAML::Flow << project.projectTopics.value();
  } else {
	output << YAML::Key << "ProjectTopics" << YAML::Value << "[]";
  }
  output << YAML::EndMap;
  return output;
}

YAML::Emitter &operator<<(YAML::Emitter &output, const SPRAL::Lecturer &lecturer)
{
  output << YAML::BeginMap;
  output << YAML::Key << "ID" << YAML::Value << lecturer.ID;
  output << YAML::Key << "Name" << YAML::Value << (lecturer.name ? lecturer.name.value() : "");
  output << YAML::Key << "Capacity" << YAML::Value << lecturer.capacity;
  if (lecturer.associatedCourses) {
	output << YAML::Key << "AssociatedCourses" << YAML::Value << YAML::Flow << lecturer.associatedCourses.value();
  } else {
	output << YAML::Key << "AssociatedCourses" << YAML::Value << "[]";
  }
  if (lecturer.topics) {
	output << YAML::Key << "AssociatedTopics" << YAML::Value << YAML::Flow << lecturer.topics.value();
  } else {
	output << YAML::Key << "AssociatedTopics" << YAML::Value << "[]";
  }
  output << YAML::EndMap;
  return output;
}

YAML::Emitter &operator<<(YAML::Emitter &output, const SPRAL::Course &course)
{
  output << YAML::BeginMap;
  output << YAML::Key << "ID" << YAML::Value << course.ID;
  output << YAML::Key << "Priority" << YAML::Value << course.priority;
  output << YAML::EndMap;
  return output;
}

void MainWindow::SaveSession()
{
  QString filter = "Sessions (*.spral)";
  QString fileName = QFileDialog::getSaveFileName(this, QObject::tr("Select Save Location"), qgetenv("HOME"), filter, &filter);
  if (fileName.isEmpty()) {
	return;
  }
  YAML::Emitter output;
  output << YAML::BeginMap;
  output << YAML::Key << "Session" << YAML::Value << "Unnamed Session";
  output << YAML::Key << "InputData" << YAML::Value << YAML::BeginMap;
  output << YAML::Key << "Students" << YAML::Value << YAML::BeginSeq;
  for (const auto &student : students) {
	output << student;
  }
  output << YAML::EndSeq;
  output << YAML::Key << "Projects" << YAML::Value << YAML::BeginSeq;
  for (const auto &project : projects) {
	output << project;
  }
  output << YAML::EndSeq;
  output << YAML::Key << "Lecturers" << YAML::Value << YAML::BeginSeq;
  for (const auto &lecturer : lecturers) {
	output << lecturer;
  }
  output << YAML::EndSeq;
  output << YAML::Key << "Courses" << YAML::Value << YAML::BeginSeq;
  for (const auto &course : courses) {
	output << course;
  }
  output << YAML::EndMap;
  output << YAML::Key << "Allocators" << YAML::BeginSeq;
  output << YAML::EndSeq;
  output << YAML::Key << "AllocationResults" << YAML::BeginSeq;
  output << YAML::EndSeq;
  output << YAML::EndMap;

  std::ofstream fOutputStream(fileName.toStdString());
  fOutputStream << output.c_str();
}
void MainWindow::LoadSession()
{
  if (studentsImported || lecturersImported || coursesImported || projectsImported) {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Warning);
	msgBox.setText("A session file is about to be loaded however data has already been manually imported. This data will be overwritten. Do you want to continue?");
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Ok);
	if (msgBox.exec() != QMessageBox::Ok) {
	  return;
	}
  }
  QString fileName = QFileDialog::getOpenFileName(this, QObject::tr("Select Session File"), qgetenv("HOME"), QObject::tr("Sessions (*.spral)"), new QString("Sessions (*.spral)"), QFileDialog::DontUseNativeDialog);
  if (fileName.isEmpty()) {
	return;
  }
  std::ifstream inFileStream(fileName.toStdString());
  std::stringstream strStream;
  strStream << inFileStream.rdbuf();
  YAML::Node inNode = YAML::Load(strStream.str());
  students.clear();
  lecturers.clear();
  courses.clear();
  projects.clear();
  projectStatisticCombo->clear();
  if (!inNode["Session"]) {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Critical);
	msgBox.setText("An invalid session file was loaded. Please check the format and try again.");
	msgBox.setDetailedText(
	  "A session file missing the \"Session\" node was loaded. This node is required for all session files. It is most likely that this file was manually edited"
	  "and in the process has lost the node. Either undo the edits or manually add a \"Session\" node according to the documentation");
	msgBox.exec();
	return;
  }
  if (!inNode["InputData"]) {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Critical);
	msgBox.setText("An invalid session file was loaded. Please check the format and try again.");
	msgBox.setDetailedText(
	  "A session file missing the \"InputData\" node was loaded. This node is required for all session files. It is most likely that this file was manually edited"
	  "and in the process has lost the node. Either undo the edits or manually add a \"InputData\" node according to the documentation");
	msgBox.exec();
	return;
  }
  auto studentsFromYaml = inNode["InputData"]["Students"];
  if (studentsFromYaml) {
	for (auto student : studentsFromYaml) {
	  if (!student["TopicPreferences"].as<std::vector<std::string>>().empty()) {
		students.emplace_back(student["ID"].as<std::string>(), student["ProjectPreferences"].as<std::vector<std::string>>(), student["Course"].as<std::string>(), student["TopicPreferences"].as<std::vector<std::string>>());
	  } else {
		students.emplace_back(student["ID"].as<std::string>(), student["ProjectPreferences"].as<std::vector<std::string>>(), student["Course"].as<std::string>());
	  }
	  if (student["ProjectPreferences"].as<std::vector<std::string>>().size() > numberStudentPreferences) {
		numberStudentPreferences = student["ProjectPreferences"].as<std::vector<std::string>>().size();
	  }
	}
	studentsImported = true;
  }
  auto coursesFromYaml = inNode["InputData"]["Courses"];
  if (coursesFromYaml) {
	for (auto course : coursesFromYaml) {
	  courses.emplace_back(course["ID"].as<std::string>(), course["Priority"].as<int16_t>());
	}
	coursesImported = true;
  }
  auto lecturersFromYaml = inNode["InputData"]["Lecturers"];
  if (lecturersFromYaml) {
	for (auto lecturer : lecturersFromYaml) {
	  auto ID = lecturer["ID"].as<std::string>();
	  auto capacity = lecturer["Capacity"].as<uint16_t>();
	  auto name = lecturer["Name"].as<std::string>();
	  std::vector<std::string> associatedCourses;
	  try {
		associatedCourses = lecturer["AssociatedCourses"].as<std::vector<std::string>>();
	  } catch (YAML::BadConversion &e) {
		associatedCourses = {};
	  }
	  std::vector<std::string> associatedTopics;
	  try {
		associatedTopics = lecturer["AssociatedTopics"].as<std::vector<std::string>>();
	  } catch (YAML::BadConversion &e) {
		associatedTopics = {};
	  }
	  lecturers.emplace_back(ID, capacity, name, associatedTopics, associatedCourses);
	}
	lecturersImported = true;
  }
  auto projectsFromYaml = inNode["InputData"]["Projects"];
  if (projectsFromYaml) {
	for (auto project : projectsFromYaml) {
	  auto ID = project["ID"].as<std::string>();
	  auto lecturer = project["Lecturer"].as<std::string>();
	  auto capacity = project["Capacity"].as<uint8_t>();
	  auto suitableCourses = project["SuitableCourses"].as<std::vector<std::string>>();
	  auto title = project["Title"].as<std::string>();
	  std::vector<std::string> topics;
	  try {
		topics = project["ProjectTopics"].as<std::vector<std::string>>();
	  } catch (YAML::BadConversion &e) {
		topics = {};
	  }
	  projects.emplace_back(ID, lecturer, capacity, suitableCourses, title, topics);
	  projectStatisticCombo->addItem(QString::fromStdString(ID));
	}
	projectsImported = true;
  }
  runButton->setEnabled(true);
  UpdateImportedDataStats();
}
void MainWindow::UpdateImportedDataStats()
{
  if (studentsImported) {
	NumberOfStudents->setText(QString::number(students.size()));
  }
  if (projectsImported) {
	NumberOfProjects->setText(QString::number(projects.size()));
  }
  if (lecturersImported) {
	NumberOfLectuerers->setText(QString::number(lecturers.size()));
  }
  PlotStudentStats();
  findIgnoredProjects();
}
void MainWindow::UpdateResultsStats(int newIndex)
{
  unassignedStudentsList->clear();
  freeProjectsList->clear();
  if (newIndex > allocationRuns.size()) {
	return;
  }
  SPRAL::AllocationResults &newResults = allocationRuns.at(newIndex);
  allocationAlgorithmName->setText(QString::fromStdString(newResults.algorithmName));
  allocationRunDatetime->setText(QString::fromStdString(newResults.dateTime));
  numberUnassignedStudents->setText(QString::number(newResults.unassignedStudents.size()));
  for (const auto &student : newResults.unassignedStudents) {
	unassignedStudentsList->addItem(QString::fromStdString(student.ID));
  }
  numberFreeProjects->setText(QString::number(newResults.freeProjects.size()));
  for (const auto &project : newResults.freeProjects) {
	freeProjectsList->addItem(QString::fromStdString(project.ID));
  }
  // Plot graph
  std::vector<int> allocationRankings;
  allocationRankings.reserve(numberStudentPreferences * 2);
  for (size_t i = 0; i < numberStudentPreferences * 2; i++) {
	allocationRankings.push_back(0);
  }
  for (const auto &allocation : newResults.allocations) {
	allocationRankings.at(allocation.student.assignedProjectRank)++;
  }
  auto *projectPopularity = new QBarSet("rankingPercent");
  for (auto &value : allocationRankings) {
	projectPopularity->append(value);
  }

  auto *series = new QBarSeries();
  series->append(projectPopularity);
  auto *chart = outputStats->chart();
  chart->addSeries(series);
  chart->setAnimationOptions(QChart::SeriesAnimations);
  QStringList categories;
  categories << "1st"
			 << "2nd"
			 << "3rd"
			 << "4th"
			 << "5th"
			 << "6th"
			 << "7th"
			 << "8th"
			 << "9th"
			 << "10th"
			 << "Lecturer"
			 << "Topics";
  auto *axisX = new QBarCategoryAxis();
  axisX->setTitleText("Rank Position");
  axisX->append(categories);
  chart->addAxis(axisX, Qt::AlignBottom);
  series->attachAxis(axisX);

  auto *axisY = new QValueAxis();
  axisY->setTickType(QtCharts::QValueAxis::TicksDynamic);
  axisY->setTickInterval(5);
  axisY->setRange(0, ((int)(*std::max_element(allocationRankings.begin(), allocationRankings.end()) / 5) * 5) + 5);
  axisY->setTitleText("% Of Students Allocated Project At Rank");
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);
  chart->legend()->hide();
}
void MainWindow::ShowUnassignedStudentDetails(const QString &newValue)
{
  unassignedStudentProjectPreferences->clear();
  unassignedStudentTopicPreferences->clear();
  unassignedStudentID->setText(newValue);
  const SPRAL::Student &currentlySelectedStudent = students.at(std::distance(students.begin(), std::find_if(students.begin(), students.end(), [&](const SPRAL::Student &s) { return s.ID == newValue.toStdString(); })));
  unassignedStudentCourse->setText(QString::fromStdString(currentlySelectedStudent.course));
  for (const auto &pPref : currentlySelectedStudent.projectPreferences) {
	unassignedStudentProjectPreferences->append(QString::fromStdString(pPref));
  }
  if (currentlySelectedStudent.topicPreferences) {
	for (const auto &tPref : currentlySelectedStudent.topicPreferences.value()) {
	  unassignedStudentTopicPreferences->append(QString::fromStdString(tPref));
	}
  }
}
void MainWindow::ShowFreeProjectDetails(const QString &newValue)
{
  freeProjectTopics->clear();
  freeProjectCourses->clear();
  freeProjectID->setText(newValue);
  const SPRAL::Project &currentlySelectedProject = projects.at(std::distance(projects.begin(), std::find_if(projects.begin(), projects.end(), [&](const SPRAL::Project &p) { return p.ID == newValue.toStdString(); })));
  if (currentlySelectedProject.title) {
	freeProjectTitle->setText(QString::fromStdString(currentlySelectedProject.title.value()));
  }
  freeProjectLecturer->setText(QString::fromStdString(currentlySelectedProject.lecturer));
  if (currentlySelectedProject.projectTopics) {
	for (const auto &pTopic : currentlySelectedProject.projectTopics.value()) {
	  freeProjectTopics->append(QString::fromStdString(pTopic));
	}
  }
  for (const auto &course : currentlySelectedProject.suitableCourses) {
	freeProjectCourses->append(QString::fromStdString(course));
  }
  freeProjectRemainingCapacity->setText(QString::number(currentlySelectedProject.capacity - currentlySelectedProject.assignedStudents.size()));
  const SPRAL::Lecturer &projectLecturer = lecturers.at(std::distance(lecturers.begin(), std::find_if(lecturers.begin(), lecturers.end(), [&](const SPRAL::Lecturer &l) { return currentlySelectedProject.lecturer == l.ID; })));
  freeProjectRemainingLecturerCapacity->setText(QString::number(projectLecturer.capacity - projectLecturer.assignedStudents.size()));
}
