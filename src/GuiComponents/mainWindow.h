//
// Created by tom on 04/10/2020.
//

#ifndef STUDENTPROJECTALLOCATOR_MAINWINDOW_H
#define STUDENTPROJECTALLOCATOR_MAINWINDOW_H
#include <QtWidgets/QMainWindow>
#include <unordered_map>
#include "Windows/ImportedDataBrowser.h"

#include "../Allocator/Allocator.h"
#include "Windows/ui_MainWindow.h"
class MainWindow : public QMainWindow
  , public Ui_MainWindow
{
  Q_OBJECT
public:
  explicit MainWindow(QWidget *parent = nullptr);

private:
  std::unordered_map<std::string, std::string> dataFilenames;
  std::vector<SPRAL::Student> students;
  std::vector<SPRAL::Project> projects;
  std::vector<SPRAL::Lecturer> lecturers;
  std::vector<SPRAL::Course> courses;
  std::vector<SPRAL::AllocationResults> allocationRuns;
  bool studentsImported{ false };
  bool projectsImported{ false };
  bool lecturersImported{ false };
  bool coursesImported{ false };
  SPRAL::Allocator *currentAllocator{ nullptr };
  std::unordered_map<std::string, std::unique_ptr<SPRAL::Allocator>> allocators;
  size_t numberStudentPreferences{ 0 };
private slots:
  void ShowUnassignedStudentDetails(const QString &newValue);
  void ShowFreeProjectDetails(const QString &newValue);
  void UpdateResultsStats(int newIndex);
  void SaveSession();
  void LoadSession();
  void SaveResults();
  void lauchDataViewer();
  void findIgnoredProjects();
  void processImportedData();
  void runAllocation();
  void setupAlgoSettings();
  void PlotStudentStats();
  void PlotProjectStats(QString newValue);
  void UpdateImportedDataStats();
};

#endif // STUDENTPROJECTALLOCATOR_MAINWINDOW_H
